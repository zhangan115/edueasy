//
//  Tools.swift
//  edetection
//
//  Created by piggybear on 02/05/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import Foundation
import PKHUD
import SwiftyJSON
import Moya
import Result

//MARK: - public method

public func showPKHUD (message: String, completion: ((Bool) -> Void)? = nil) {
    HUD.flash(.label(message), delay: 0.5, completion: completion)
}

public func dateString(millisecond: TimeInterval, dateFormat: String) -> String {
    let date = Date(timeIntervalSince1970: millisecond / 1000)
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat
    return formatter.string(from: date)
}

var getCookie: String {
    let cookieObject = Cookie.unarchiver()
    let name: String = cookieObject?.name ?? ""
    let value: String = cookieObject?.value ?? ""
    let cookie: String = name + "=" + value
    return cookie
}

public func alertController() {
    let alertController = UIAlertController(title: "请登录后查看", message: nil, preferredStyle: .alert)
    let ok = UIAlertAction(title: "确定", style: .destructive) {_ in
        let navi = BaseNavigationController(rootViewController: LoginController())
        UIApplication.shared.keyWindow?.rootViewController?.presentVC(navi)
    }
    let cancel = UIAlertAction(title: "取消", style: .default, handler: nil)
    alertController.addAction(cancel)
    alertController.addAction(ok)
    UIApplication.shared.keyWindow?.rootViewController?.presentVC(alertController)
}



//
//  PGProvider.swift
//  edetection
//
//  Created by piggybear on 01/05/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
import Result
import PKHUD
import RxCocoa
import RxSwift

final class PGProvider: NSObject {
    
    typealias Success = (_ responseJson: JSON) -> Void
    typealias Failure = ((_ error: MoyaError?) -> Void)?
    typealias Relogin = ()->()
    typealias SuccessString = (_ responseJson: String) -> Void
    
    class func networkPlugin(_ offset: Bool = false) -> PluginType {
        let networkPlugin = NetworkActivityPlugin {  change,_  in
            switch(change){
            case .began:
                NSObject().currentViewController.view.showActivityIndicator(offset: offset)
            case .ended:
                NSObject().currentViewController.view.hideActivityIndicator()
            }
        }
        return networkPlugin
    }
    
    public class func successLogic (responseObject: Response, success:  @escaping Success, failure: Failure, relogin: @escaping Relogin) {
        let statusCode = responseObject.statusCode
        if statusCode == 200 {
            let json = try! JSON(data: responseObject.data)
            let errorCode = json["errorCode"].intValue
            if errorCode == 0 {
                success(json)
            }else if errorCode == 1001 { //未登录
                setIsLogin(false)
                let navi = BaseNavigationController(rootViewController: LoginController())
                UIApplication.shared.keyWindow?.rootViewController = navi
            }else if json["errorMessage"].stringValue != "" {
                showPKHUD(message: json["errorMessage"].stringValue)
                if let failure = failure {
                    failure(nil)
                }
            }else {
                showPKHUD(message: "出现了一点小故障，请稍后重试")
                if let failure = failure {
                    failure(nil)
                }
            }
        }else {
            showPKHUD(message: "出现了一点小故障，请稍后重试")
            if let failure = failure {
                failure(nil)
            }
        }
    }
    
    public class func errorLogic(_ error: MoyaError) {
        if case let .underlying(error1, nil) = error {
            let error2 = error1 as NSError
            if error2.code == -999 { //如果是取消当前网络请求的话，不做提示
                return
            }else {
                showPKHUD(message: error.errorDescription!)
            }
        }else {
            showPKHUD(message: error.errorDescription!)
        }
    }
    
    public class func reLoginLogic(success: @escaping ()->(), failure: PGProvider.Failure) {
        let username = UserDefaults.standard.string(forKey: kUsername)
        let password = UserDefaults.standard.string(forKey: kPassword)
        if username == nil || password == nil {
            return
        }
        userProviderNoPlugin.requestResult(.login(username: username!, password: password!), success: { _ in
            success()
        }, failure: failure)
    }
}


//
//  RegisterViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/25.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RegisterViewModel {
    
    let phoneVerifyCodeButton = PublishSubject<Void>()
    var phoneVerifyCodeResult: Driver<Result<ResultModel>>!
    var phoneVerifyCode: String!
    
    let nextButtonTap = PublishSubject<Void>()
    var nextResult: Driver<Result<ResultModel>>!
    
    var suoxuezhuanye:String!
    var type:String!
    var openId:String?
    
    init(password: Observable<String>,
         confirmPassword: Observable<String>,
         realName: Observable<String>,
         userCardId: Observable<String>,
         phoneNumber: String,
         verifyCode: String,
         inviteCode: String?) {
        
        let combineLatest = Observable.combineLatest(password, confirmPassword,realName,userCardId)
        nextResult = nextButtonTap
            .asObserver()
            .withLatestFrom(combineLatest)
            .flatMapLatest {[weak self] (password, confirmPassword,realName,userCardId) -> Single<Result<ResultModel>> in
                if self == nil {
                    return Single.just(.error)
                }
                let this: RegisterViewModel = self!
                switch this.validatePassword(password) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                switch this.validateConfirmPassword(confirmPassword, password: password) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                switch this.validateRealName(password) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                switch this.validateUserCardId(password) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                if self!.suoxuezhuanye.count == 0 {
                    return Single.just(.failed(message: "请选择专业"))
                }
                
                if self!.type.count == 0 {
                    return Single.just(.failed(message: "请选择考试类型"))
                }
                
                return registerProvider
                    .rxRequest(.register(pssword: password,
                                         phone: phoneNumber,
                                         verificationCode: verifyCode,
                                         inviteCode: inviteCode, realName: realName, userCardId: userCardId,
                                         suoxuezhuanye: self!.suoxuezhuanye, type: self!.type, openId: self!.openId))
                    .map{_ in return .ok}
            }.asDriver(onErrorJustReturn: .error)
    }
    
    init(phoneNumber: Observable<String>,
         verifyCode: Observable<String>) {
        
        phoneVerifyCodeResult = phoneVerifyCodeButton
            .asObserver()
            .withLatestFrom(phoneNumber)
            .flatMapLatest {[weak self] number -> Single<Result<ResultModel>> in
                if self == nil {
                    return Single.just(.error)
                }
                let this: RegisterViewModel = self!
                let result = this.validatePhoneNumber(number)
                switch result {
                case .failed:
                    return Single.just(result)
                default: break
                }
                return registerProvider
                    .rxRequest(.verifyPhoneCode(phone: number))
                    .toModel(type: PhoneCodeModel.self)
                    .map{model in
                        let code = model.verificationCode
                        this.phoneVerifyCode = code
                        print("code = ", code ?? "")
                        return .ok
                }
            }.asDriver(onErrorJustReturn: .error)
        
        
        
        let combineLatest = Observable.combineLatest(phoneNumber, verifyCode)
        nextResult = nextButtonTap
            .asObserver()
            .withLatestFrom(combineLatest)
            .flatMapLatest {[weak self] (phoneNumber, verifyCode) -> Single<Result<ResultModel>> in
                if self == nil {
                    return Single.just(.error)
                }
                let this: RegisterViewModel = self!
                switch this.validatePhoneNumber(phoneNumber) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                switch this.validateVerifyCode(verifyCode) {
                case .failed(let message):
                    return Single.just(.failed(message: message))
                default: break
                }
                
                return Single.just(.ok)
                
            }.asDriver(onErrorJustReturn: .error)
    }
    
    func validatePhoneNumber(_ phoneNumber: String) -> Result<ResultModel> {
        if phoneNumber.count == 0 {
            return .failed(message: "手机号码不能为空")
        }
        if phoneNumber.isVaildPhoneNumber {
            return .empty
        }
        return .failed(message: "请输入正确的手机号码")
    }
    
    func validateRealName(_ realName: String) -> Result<ResultModel> {
        if realName.count == 0 {
            return .failed(message: "姓名不能为空")
        }
        return .empty
    }
    
    func validateUserCardId(_ cardId: String) -> Result<ResultModel> {
        if cardId.count == 0 {
            return .failed(message: "身份证不能为空")
        }
        return .empty
    }
    
    func validateUsername(_ username: String) -> Result<ResultModel> {
        if username.count == 0 {
            return .failed(message: "账号不能为空")
        }
        if !username.isEnglishFirst {
            return .failed(message: "账号必须要以字母开头")
        }
        if username.isNumber {
            return .failed(message: "账号必须是数字和字母组成")
        }
        if username.count < 3 || username.count > 12 {
            return .failed(message: "账号应该为3-12位之间")
        }
        return .empty
    }
    
    func validatePassword(_ password: String) -> Result<ResultModel> {
        if password.count == 0 {
            return .failed(message: "密码不能为空")
        }
        if password.isNumber {
            return .failed(message: "密码必须必须是数字和字母组成")
        }
        if password.count < 8 || password.count > 20 {
            return .failed(message: "密码应该为8-20位之间")
        }
        return .empty
    }
    
    func validateConfirmPassword(_ confirmPassword: String, password: String) -> Result<ResultModel> {
        if confirmPassword != password {
            return .failed(message: "两次输入的密码不一致")
        }
        return .empty
    }
    
    func validateVerifyCode(_ verifyCode: String) -> Result<ResultModel> {
        if verifyCode.count == 0 {
            return .failed(message: "验证码不能为空")
        }
        if verifyCode != self.phoneVerifyCode {
            return .failed(message: "验证码不正确")
        }
        return .empty
    }
}

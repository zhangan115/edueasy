//
//  RegisterNextView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class RegisterNextView: UIView {
    
    lazy var passwordTextField: SetPasswordTextField = {
        let textField = self.createTextField()
        textField.setLeftText("密码")
        textField.isSecureTextEntry = true
        textField.placeholder = "8-20 位的字母和数字组成"
        self.addSubview(textField)
        return textField
    }()
    
    lazy var confirmPasswordTextField: SetPasswordTextField = {
        let textField = self.createTextField()
        textField.isSecureTextEntry = true
        textField.setLeftText("确认密码")
        textField.placeholder = "再次输入密码"
        self.addSubview(textField)
        return textField
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let lineViewWidth: CGFloat = 0.5
        let lineView1 = createLineView()
        self.addSubview(lineView1)
        lineView1.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.snp.top)
            make.height.equalTo(lineViewWidth)
        }
        
        passwordTextField.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(lineView1.snp.bottom)
            make.height.equalTo(50)
        }
        
        let lineView2 = createLineView()
        self.addSubview(lineView2)
        lineView2.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.passwordTextField.snp.bottom)
            make.height.equalTo(lineViewWidth)
        }
        
        confirmPasswordTextField.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(lineView2.snp.bottom)
            make.height.equalTo(50)
        }
        
        let lineView3 = createLineView()
        self.addSubview(lineView3)
        lineView3.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.confirmPasswordTextField.snp.bottom)
            make.height.equalTo(lineViewWidth)
        }
    }
    
    func createLineView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        return view
    }
    
    func createTextField() -> SetPasswordTextField {
        let textField = SetPasswordTextField()
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.textAlignment = .left
        textField.textColor = UIColor.black
        return textField
    }
}

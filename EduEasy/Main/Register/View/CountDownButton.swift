//
//  CountDownButton.swift
//  Evpro
//
//  Created by piggybear on 2017/12/12.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

class CountDownButton: UIButton {
    
    private var countdownTimer: Timer?
    private var remainingSeconds: Int = 0 {
        willSet {
            self.setTitle("重新获取\(newValue)秒", for: .normal)
            if newValue <= 0 {
                self.setTitle("获取验证码", for: .normal)
                isCounting = false
            }
        }
    }
    
    var isCounting = false {
        willSet {
            if newValue {
                countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
                remainingSeconds = 30
                self.setTitleColor(UIColor(hexString: "#46AC2B"), for: .normal)
            } else {
                self.setTitle("获取验证码", for: .normal)
                countdownTimer?.invalidate()
                countdownTimer = nil
                self.setTitleColor(UIColor(hexString: "#2772FE"), for: .normal)
            }
            self.isEnabled = !newValue
        }
    }
    
    @objc private func updateTime() {
        remainingSeconds -= 1
    }
    
}


//
//  RegisterNextController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegisterNextController: BeanController {
    
    var phoneNumber: String!//手机号码
    var verifyCode: String!//验证码
    var inviteCode: String!//邀请码
    
    var userCardId:String!//身份证
    var realName:String!//用户真名
    
    var weiXinCode : String? = nil //微信openId
    
    private var zyStrList : [String] = ["高起专（文）", "高起专（理）", "专升本（医学类）", "专升本（理工类）", "专升本（经管类）", "专升本（法学类）"
        , "专升本（教育学类）", "专升本（农学类）", "专升本（艺术类）", "专升本（文史中医类）", "高升本（文）", "高升本（理）"]
    private var lxStrList : [String] = ["成人高考", "学位考试", "统招专升本", "医学护理"]
    
    var viewModel: RegisterViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var confirmPasswordTextField: LoginTextField!
    
    @IBOutlet weak var realNameTextField: LoginTextField!
    @IBOutlet weak var userCardIdTextField: LoginTextField!
    
    @IBOutlet weak var completionButton: UIButton!
    
    @IBOutlet weak var zyButton: UIButton!
    @IBOutlet weak var lxButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "注册"
        passwordTextField.setLeftText("密      码")
        confirmPasswordTextField.setLeftText("确认密码")
        realNameTextField.setLeftText("姓    名")
        userCardIdTextField.setLeftText("身份证")
        completionButton.layer.cornerRadius = 5
        viewModel = RegisterViewModel(password: passwordTextField.rx.text.orEmpty.asObservable(),
                                      confirmPassword: confirmPasswordTextField.rx.text.orEmpty.asObservable(),
                                      realName: realNameTextField.rx.text.orEmpty.asObservable(),
                                      userCardId: userCardIdTextField.rx.text.orEmpty.asObservable(),
                                      phoneNumber: phoneNumber,
                                      verifyCode: verifyCode,
                                      inviteCode: inviteCode)
        
        viewModel.nextResult.asObservable().subscribe(onNext: {[weak self] result in
            switch result {
            case .failed(let message):
                self?.view.showAutoHUD(message)
            case .ok:
                self?.view.showHUD("注册成功", completion: {
                    self?.navigationController?.popToRootViewController(animated: true)
                })
            default: break
            }
        }).disposed(by: disposeBag)
        
        completionButton.rx.tap.asObservable()
            .bind(to: viewModel.nextButtonTap)
            .disposed(by: disposeBag)
    }
    
    @IBAction func userAgreementButtonHandler(_ sender: UIButton) {
        let controller = ContractController()
        self.pushVC(controller)
    }
    
    @IBAction func zyButtonHandler(_ sender: UIButton) {
        let actionSheet = ActionSheet.init(cancelButton: false, buttonList: self.zyStrList)
        self.present(actionSheet, animated: false, completion: nil)
        actionSheet.handler = {[weak self] idx in
            guard let `self` = self else {
                return
            }
            self.zyButton.setTitle(self.zyStrList[idx], for: .normal)
            self.viewModel.suoxuezhuanye = (idx + 1).toString
        }
    }
    
    @IBAction func lxButtonHandler(_ sender: UIButton) {
        let actionSheet = ActionSheet.init(cancelButton: false, buttonList: self.lxStrList)
        self.present(actionSheet, animated: false, completion: nil)
        actionSheet.handler = {[weak self] idx in
            guard let `self` = self else {
                return
            }
            self.lxButton.setTitle(self.lxStrList[idx], for: .normal)
            self.viewModel.type = (idx + 1).toString
        }
    }
}

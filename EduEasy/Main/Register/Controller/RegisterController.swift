//
//  RegisterController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegisterController: BeanController {
    
    @IBOutlet weak var phoneNumberTextField: LoginTextField!
    @IBOutlet weak var verifyTextField: LoginTextField!
    @IBOutlet weak var inviteCodeTextField: LoginTextField!
    @IBOutlet weak var verifyButton: CountDownButton!
    @IBOutlet weak var completionButton: UIButton!
    
    var weiXinCode : String? = nil
    
    var viewModel: RegisterViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel = RegisterViewModel(phoneNumber: phoneNumberTextField.rx.text.orEmpty.asObservable(),
                                      verifyCode: verifyTextField.rx.text.orEmpty.asObservable())
        viewModel.nextResult.asObservable().subscribe(onNext: {[weak self] result in
            guard let `self` = self else {
                return
            }
            switch result {
            case .failed(let message):
                self.view.showAutoHUD(message)
            case .ok:
                let controller = RegisterNextController()
                controller.phoneNumber = self.phoneNumberTextField.text
                controller.verifyCode = self.verifyTextField.text
                controller.inviteCode = self.inviteCodeTextField.text
                self.pushVC(controller)
            default: break
            }
        }).disposed(by: disposeBag)
        
        completionButton.rx.tap.asObservable()
            .bind(to: viewModel.nextButtonTap)
            .disposed(by: disposeBag)
        
        viewModel.phoneVerifyCodeResult.asObservable().subscribe(onNext: {[weak self] result in
            switch result {
            case .failed(let message):
                self?.view.showAutoHUD(message)
            case .ok:
                self?.verifyButton.isCounting = true
            default: break
            }
        }).disposed(by: disposeBag)
        
        verifyButton.rx.tap.asObservable()
            .bind(to: viewModel.phoneVerifyCodeButton)
            .disposed(by: disposeBag)
    }
    
    func setup() {
        verifyButton.layer.borderColor = UIColor(hexString: "#2772FE")?.cgColor
        verifyButton.layer.borderWidth = 0.5
        verifyButton.layer.cornerRadius = 3
        phoneNumberTextField.setLeftText("手机号")
        verifyTextField.setLeftText("验证码")
        inviteCodeTextField.setLeftText("邀请码")
        completionButton.layer.cornerRadius = 5
    }
}

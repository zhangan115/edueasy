//
//	PhoneCodeModel.swift
//
//	Create by piggybear on 25/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PhoneCodeModel: Mappable{

	var verificationCode : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		verificationCode = json["verificationCode"].stringValue
	}

}

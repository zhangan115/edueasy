//
//  StudyController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class StudyController: BeanTableController {

    let cellIdentifier = "StudyCell"
    var index: Int!
    
    let disposeBag = DisposeBag()
    let viewModel = StudyViewModel()
    
    var responseList: [StudyModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableViewRegister()
        
        viewModel.range.value = index
        viewModel.result.asObservable().subscribe(onNext: {[weak self] list in
            self?.tableView.mj_header.endRefreshing()
            self?.responseList = list
            self?.tableView.noRefreshReloadData()
        }).disposed(by: disposeBag)
        
        let header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(headerRefreshHandler))
        tableView.mj_header = header
    }
    
    func tableViewRegister() {
        tableView.register(UINib(nibName: "StudyCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    @objc func headerRefreshHandler() {
        viewModel.range.value = index
    }
    
    func alertController() {
        let controller = UIAlertController(title: "提示", message: "马上充值成为会员，信息浏览将畅通无阻", preferredStyle: .alert)
        let ok = UIAlertAction(title: "马上充值", style: .destructive) {_ in
            let controller = VipController()
            self.pushVC(controller)
        }
        let cancel = UIAlertAction(title: "暂不充值", style: .default) {_ in
            
        }
        controller.addAction(cancel)
        controller.addAction(ok)
        self.present(controller, animated: true, completion: nil)
    }
}

extension StudyController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = responseList {
            return list.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! StudyCell
         let model = responseList[indexPath.row]
        cell.setModel(model)
        return cell
    }
}

extension StudyController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = UserModel.unarchiver()
        let isVip: Bool = user?.isVip ?? false
        guard isVip else {
            alertController()
            return
        }
        let controller = StudyDetailController()
        let model = responseList[indexPath.row]
        controller.setHtmlString(model.detail)
        controller.title = model.title
        self.pushVC(controller)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "header"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}

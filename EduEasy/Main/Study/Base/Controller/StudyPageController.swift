//
//  StudyPageController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class StudyPageController: BeanController {

    var pageTitleView: SGPageTitleView!
    var pageContentView: SGPageContentView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "学习"
        setupPageView()
    }
    
    func setupPageView() {
        var titleViewFrame = view.bounds
        titleViewFrame.size.height = 40
        let configure = pageTitleViewConfigure()
        let titleList = ["成人高考", "学位考试", "统招专升本","护理培训"]
        pageTitleView = SGPageTitleView(frame: titleViewFrame, delegate: self, titleNames: titleList, configure: configure)
        pageTitleView.isShowBottomSeparator = false
        view.addSubview(pageTitleView)
        
        var controllers: [StudyController] = []
        for idx in 0..<4 {
            let controller = StudyController()
            if idx == 3 {
                controller.index = 3
            }else{
                controller.index = 4 + idx
            }
            controllers.append(controller)
        }
        var contentViewFrame = view.bounds
        contentViewFrame.origin.y = pageTitleView.frame.maxY
        contentViewFrame.size.height = contentViewFrame.size.height - pageTitleView.frame.maxY - tabBarHeight - 64
        pageContentView = SGPageContentView(frame: contentViewFrame, parentVC: self, childVCs: controllers)
        pageContentView.delegatePageContentView = self
        view.addSubview(pageContentView)
    }
    
    func pageTitleViewConfigure() -> SGPageTitleViewConfigure {
        let configure = SGPageTitleViewConfigure()
        configure.indicatorScrollStyle = SGIndicatorScrollStyleHalf
        configure.titleFont = UIFont.systemFont(ofSize: 13)
        configure.titleColor = UIColor(hexString: "#333333")
        configure.titleSelectedColor = UIColor(hexString: "#308AFF")
        configure.indicatorColor = UIColor(hexString: "#308AFF")
        return configure
    }
}

extension StudyPageController: SGPageTitleViewDelegate {
    func pageTitleView(_ pageTitleView: SGPageTitleView!, selectedIndex: Int) {
        pageContentView.setPageCententViewCurrentIndex(selectedIndex)
    }
}

extension StudyPageController: SGPageContentViewDelegate {
    func pageContentView(_ pageContentView: SGPageContentView!, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        pageTitleView.setPageTitleViewWithProgress(progress, originalIndex: originalIndex, targetIndex: targetIndex)
    }
}

//
//  StudyCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class StudyCell: UITableViewCell {

    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var browseCountLabel: UILabel!
    
    func setModel(_ model: StudyModel) {
        customImageView.loadNetworkImage(model.firstPic, placeholder: "img_learn1")
        titleLabel.text = model.title
    }
    
}

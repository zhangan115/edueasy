//
//	StudyCreateTime.swift
//
//	Create by piggybear on 6/4/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class StudyCreateTime{

	var afterNow : Bool!
	var beforeNow : Bool!
	var centuryOfEra : Int!
	var chronology : StudyChronology!
	var dayOfMonth : Int!
	var dayOfWeek : Int!
	var dayOfYear : Int!
	var equalNow : Bool!
	var era : Int!
	var hourOfDay : Int!
	var millis : Int!
	var millisOfDay : Int!
	var millisOfSecond : Int!
	var minuteOfDay : Int!
	var minuteOfHour : Int!
	var monthOfYear : Int!
	var secondOfDay : Int!
	var secondOfMinute : Int!
	var weekOfWeekyear : Int!
	var weekyear : Int!
	var year : Int!
	var yearOfCentury : Int!
	var yearOfEra : Int!
	var zone : StudyChronology!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		afterNow = json["afterNow"].boolValue
		beforeNow = json["beforeNow"].boolValue
		centuryOfEra = json["centuryOfEra"].intValue
		let chronologyJson = json["chronology"]
		if !chronologyJson.isEmpty{
			chronology = StudyChronology(fromJson: chronologyJson)
		}
		dayOfMonth = json["dayOfMonth"].intValue
		dayOfWeek = json["dayOfWeek"].intValue
		dayOfYear = json["dayOfYear"].intValue
		equalNow = json["equalNow"].boolValue
		era = json["era"].intValue
		hourOfDay = json["hourOfDay"].intValue
		millis = json["millis"].intValue
		millisOfDay = json["millisOfDay"].intValue
		millisOfSecond = json["millisOfSecond"].intValue
		minuteOfDay = json["minuteOfDay"].intValue
		minuteOfHour = json["minuteOfHour"].intValue
		monthOfYear = json["monthOfYear"].intValue
		secondOfDay = json["secondOfDay"].intValue
		secondOfMinute = json["secondOfMinute"].intValue
		weekOfWeekyear = json["weekOfWeekyear"].intValue
		weekyear = json["weekyear"].intValue
		year = json["year"].intValue
		yearOfCentury = json["yearOfCentury"].intValue
		yearOfEra = json["yearOfEra"].intValue
		let zoneJson = json["zone"]
		if !zoneJson.isEmpty{
			zone = StudyChronology(fromJson: zoneJson)
		}
	}

}
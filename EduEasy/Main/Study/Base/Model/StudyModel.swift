//
//	StudyModel.swift
//
//	Create by piggybear on 6/4/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class StudyModel: Mappable{

	var bAllAccount : Bool!
	var brief : String!
	var createTime : StudyCreateTime!
	var detail : String!
	var firstPic : String!
	var id : Int!
	var msgType : String!
	var newField : Bool!
	var range : String!
	var sender : StudyChronology!
	var title : String!
	var videoUrl : String!


	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		bAllAccount = json["bAllAccount"].boolValue
		brief = json["brief"].stringValue
		let createTimeJson = json["createTime"]
		if !createTimeJson.isEmpty{
			createTime = StudyCreateTime(fromJson: createTimeJson)
		}
		detail = json["detail"].stringValue
		firstPic = json["firstPic"].stringValue
		id = json["id"].intValue
		msgType = json["msgType"].stringValue
		newField = json["new"].boolValue
		range = json["range"].stringValue
		let senderJson = json["sender"]
		if !senderJson.isEmpty{
			sender = StudyChronology(fromJson: senderJson)
		}
		title = json["title"].stringValue
		videoUrl = json["videoUrl"].stringValue
	}

}

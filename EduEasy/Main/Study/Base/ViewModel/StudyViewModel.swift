//
//  StudyViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class StudyViewModel {
    var range = Variable<Int>(0)
    let result: Driver<[StudyModel]>
    
    init() {
        result = range.asObservable().flatMapLatest { (id) -> Single<[StudyModel]> in
            return studyProvider
                .rxRequest(.list(range: id,count:nil))
                .toListModel(type: StudyModel.self)
            }.asDriver(onErrorJustReturn: [StudyModel(fromJson: JSON(""))])
    }
}

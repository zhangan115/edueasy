//
//  StudyDetailController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class StudyDetailController: BeanController {
    
    lazy var webView: UIWebView = {
        let webView = UIWebView(frame: self.view.bounds)
        webView.contentMode = .scaleAspectFit
//        webView.scalesPageToFit = true
//        webView.sizeToFit()
        webView.scrollView.bounces = false
        webView.delegate = self
        self.view.addSubview(webView)
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        webView.frame = self.view.bounds
    }
    
    func setHtmlString(_ string: String) {
        webView.loadHTMLString(styledHTML(string), baseURL: Config.baseURL)
    }
    
    func styledHTML(_ html: String) -> String {
        let style: String = "<head><meta charset=\"UTF-8\"></head>"
        return style + html
    }
    
    func playerWillExitFullscreen(_ sender: Notification) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension StudyDetailController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let string = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '60%'"
        self.webView.stringByEvaluatingJavaScript(from: string)
    }
}

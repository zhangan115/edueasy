//
//  NewsController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/15.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let newsCellReuseIdentifier = "HomeNewsCell"

class NewsController: BeanTableController {
    
    var newsList: [StudyModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "热门信息"
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableViewRegister()
    }
    
    func tableViewRegister() {
        tableView.register(HomeNewsCell.self, forCellReuseIdentifier: newsCellReuseIdentifier)
    }
}

extension NewsController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = newsList {
            return list.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: newsCellReuseIdentifier, for: indexPath) as! HomeNewsCell
        cell.setModel(newsList[indexPath.row])
        return cell
    }
}

extension NewsController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = StudyDetailController()
        let model = newsList[indexPath.row]
        controller.title = model.title
        controller.setHtmlString(model.detail ?? "")
        self.pushVC(controller)
    }
}

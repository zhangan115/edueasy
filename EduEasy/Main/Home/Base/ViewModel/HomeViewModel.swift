//
//  HomeViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/8.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift

class HomeViewModel {

    func bannerRequest() -> Single<[StudyModel]> {
        return request(range: 1,count:nil)
    }
    
    func notifyRequest() -> Single<[StudyModel]> {
        return request(range: 2,count:nil)
    }
    
    func schoolRequest() -> Single<[StudyModel]> {
        return request(range:13,count: 4)
    }
    
    func schoolAllRequest() -> Single<[StudyModel]> {
        return request(range:13,count: 1000)
    }
    
    func request(range: Int,count:Int?) -> Single<[StudyModel]> {
        return studyProvider
            .rxRequest(.list(range: range,count:count))
            .toListModel(type: StudyModel.self)
    }
    
}

//
//  HomeController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import PromiseKit

private let bannerCellReuseIdentifier = "HomeBannerCell"
private let noticeCellReuseIdentifier = "HomeNoticeCell"
private let enrollCellReuseIdentifier = "HomeEnrollCell"
private let examCellReuseIdentifier = "HomeExamCell"
private let newsCellReuseIdentifier = "HomeNewsCell"
private let bottomCellReuseIdentifier = "HomeBottomCell"
private let upgradeCellReuseIdentifier = "HomeUpgradeCell"
private let schoolCellReuseIdentifier = "SchoolTableViewCell"
private let schoolTitleCellReuseIdentifier = "SchoolTitleTableViewCell"
class HomeController: BaseTableController {
    
    let disposeBag = DisposeBag()
    let viewModel = HomeViewModel()
    
    var currentPosition = -1
    var bannerList: [StudyModel]!
    var notifyList: [StudyModel]!
    var schoolList:[StudyModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "首页"
        self.view.backgroundColor = UIColor.white
        self.tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableViewRegister()
        tableView.bounces = false
        request()
        let tf = UserDefaults.standard.bool(forKey: "review")
        if !tf {
            reviewProvider.requestResult(.review, success: { (json) in
                print("json = ", json)
                let tf = json["data"].boolValue
                UserDefaults.standard.set(tf, forKey: "review")
                UserDefaults.standard.synchronize()
            })
        }
    }
    
    func request() {
        firstly {
            bannerRequest()
            }.then {[weak self] (list) -> Promise<[StudyModel]> in
                guard let `self` = self else {
                    throw PGError.exception
                }
                self.bannerList = list
                self.tableView.reloadData()
                return self.notifyRequest()
            }.then {[weak self] (list) -> Promise<[StudyModel]> in
                guard let `self` = self else {
                    throw PGError.exception
                }
                self.notifyList = list
                self.tableView.reloadData()
                return self.schoolRequest()
            }.done {[weak self] list in
                guard let `self` = self else {
                    throw PGError.exception
                }
                self.schoolList = list
                self.tableView.reloadData()
            }.catch { _ in
        }
    }
    
    func bannerRequest() -> Promise<[StudyModel]> {
        return Promise{seal in
            viewModel.bannerRequest().subscribe(onSuccess: { (list) in
                seal.fulfill(list)
            }, onError: { error in
                seal.reject(error)
            }).disposed(by: disposeBag)
        }
    }
    
    func notifyRequest() -> Promise<[StudyModel]> {
        return Promise{seal in
            viewModel.notifyRequest().subscribe(onSuccess: { (list) in
                seal.fulfill(list)
            }, onError: { error in
                seal.reject(error)
            }).disposed(by: disposeBag)
        }
    }
    
    func schoolRequest() -> Promise<[StudyModel]> {
        return Promise{seal in
            viewModel.schoolRequest().subscribe(onSuccess: { (list) in
                seal.fulfill(list)
            }, onError: { error in
                seal.reject(error)
            }).disposed(by: disposeBag)
        }
    }
    
    func tableViewRegister() {
        tableView.register(HomeBannerCell.self, forCellReuseIdentifier: bannerCellReuseIdentifier)
        tableView.register(HomeNoticeCell.self, forCellReuseIdentifier: noticeCellReuseIdentifier)
        tableView.register(HomeEnrollCell.self, forCellReuseIdentifier: enrollCellReuseIdentifier)
        tableView.register(HomeExamCell.self, forCellReuseIdentifier: examCellReuseIdentifier)
        tableView.register(UINib(nibName: "HomeUpgradeCell", bundle: nil),
                           forCellReuseIdentifier: upgradeCellReuseIdentifier)
        tableView.register(UINib(nibName: "SchoolTableViewCell", bundle: nil), forCellReuseIdentifier: schoolCellReuseIdentifier)
        tableView.register(UINib(nibName: "SchoolTitleTableViewCell", bundle: nil), forCellReuseIdentifier: schoolTitleCellReuseIdentifier)
    }
}

extension HomeController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if schoolList != nil && schoolList.count > 0 {
            return 4
        }
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        if section == 3 {
            if schoolList != nil {
                if schoolList.count % 2 == 0 {
                    return schoolList.count / 2 + 1
                }
                return schoolList.count / 2 + 1 + 1
            }
            return 0
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: bannerCellReuseIdentifier, for: indexPath) as! HomeBannerCell
            if bannerList != nil && !bannerList.isEmpty {
                cell.setModel(bannerList)
                cell.currentPositionHandle = { (currentPosition)in
                    self.currentPosition = currentPosition
                }
            }
            return cell
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: noticeCellReuseIdentifier, for: indexPath) as! HomeNoticeCell
            if notifyList != nil && notifyList.count > 0 {
                let title = notifyList.first?.title
                cell.setContent(title ?? "")
            }
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: enrollCellReuseIdentifier, for: indexPath)
            return cell
        }
        
        if indexPath.section == 3  {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: schoolTitleCellReuseIdentifier, for: indexPath) as! SchoolTitleTableViewCell
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: schoolCellReuseIdentifier, for: indexPath) as! SchoolTableViewCell
            var leftModel:StudyModel?
            var rightMdeol:StudyModel?
            let currentPosition = (indexPath.row - 1) * 2
            if currentPosition <= schoolList.count {
                leftModel = schoolList[currentPosition]
            }
            if currentPosition + 1 < schoolList.count{
                rightMdeol = schoolList[currentPosition+1]
            }
            cell.setModel(leftModel: leftModel, rightMdeol: rightMdeol)
            cell.imageClick = { (id) in
                for item in self.schoolList {
                    if item.id == id {
                        let controller = StudyDetailController()
                        controller.title = item.title
                        controller.setHtmlString(item.detail ?? "")
                        self.pushVC(controller)
                        break
                    }
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: examCellReuseIdentifier, for: indexPath)
        return cell
    }
}

extension HomeController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isDidLogin {
            alertController()
            return
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            let controller = StudyDetailController()
            controller.title = "通知"
            let model = notifyList.first
            controller.setHtmlString(model?.detail ?? "")
            self.pushVC(controller)
            return
        }
        
        if indexPath.section == 2 {
            let controller = ApplyTableController()
            self.pushVC(controller)
        }
        if indexPath.section == 3 && indexPath.row == 0 {
            let controller = SchoolListController()
            self.pushVC(controller)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            return 179
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            return 44
        }
        if indexPath.section == 1 {
            return 250
        }
        if indexPath.section == 3 {
            if indexPath.row == 0{
                return 44
            }
            return (self.view.frame.width - 30) / 2 * 15 / 16
        }
        return 78
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        return 10
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        return 10
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let reuseIdentifier = "header"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: reuseIdentifier)
        }
        view?.backgroundColor = UIColor(hexString: "#f4f4f4")
        return view
    }
}

extension HomeController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupClass() ->Bool {
        let controller = self.currentViewController
        if controller.isKind(of: QuestionBankController.self) ||
            controller.isKind(of: StudyPageController.self) ||
            controller.isKind(of: MeController.self) {
            return true
        }
        return false
    }
}

//
//  HomeBottomCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/15.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeBottomCell: UITableViewCell {

    lazy var leftView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "#999999")
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var rightView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "#999999")
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = "我是有底线的"
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = UIColor(hexString: "#616161")
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.contentView.backgroundColor = kBackgroundColor
        
        nameLabel.snp.updateConstraints { (make) in
            make.center.equalTo(self.contentView.snp.center)
        }
        
        leftView.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(10)
            make.right.equalTo(self.nameLabel.snp.left).offset(-10)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.height.equalTo(0.5)
        }
        
        rightView.snp.updateConstraints { (make) in
            make.left.equalTo(self.nameLabel.snp.right).offset(10)
            make.right.equalTo(self.contentView.snp.right).offset(-10)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.height.equalTo(0.5)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
}

//
//  HomeUpgradeCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/28.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeUpgradeCell: UITableViewCell {

    @IBAction func buttonTapHandler(_ sender: UIButton) {
        let controller = StudyController()
        controller.index = 10 + sender.tag
        controller.title = sender.titleLabel?.text
        currentViewController.pushVC(controller)
    }
    
    
}

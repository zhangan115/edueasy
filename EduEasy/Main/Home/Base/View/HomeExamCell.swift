//
//  HomeExamCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeExamCell: UITableViewCell {
    
    lazy var enrollButton:UIImageView = {
        let image = UIImageView()
        self.contentView.addSubview(image)
        image.image = UIImage(named: "ad_img")
        image.clipsToBounds = true
        image.contentMode = UIViewContentMode.scaleToFill
        return image
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        enrollButton.snp.updateConstraints{ (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
        }
    }
}

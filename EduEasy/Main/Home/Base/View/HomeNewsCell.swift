//
//  HomeNewsCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/15.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeNewsCell: UITableViewCell {
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "#616161")
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#ACACAC")
        label.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(label)
        return label
    }()
    
    func setIndex(_ index: Int) {
        if index == 0 {
            nameLabel.text = "热门信息"
            nameLabel.textColor = UIColor.black
            nameLabel.font = UIFont.systemFont(ofSize: 16)
            nameLabel.textAlignment = .center
            timeLabel.text = ""
        }else if index == 4 {
            nameLabel.text = "更多资讯"
            nameLabel.textColor = UIColor(hexString: "#607BCF")
            nameLabel.font = UIFont.systemFont(ofSize: 15)
            nameLabel.textAlignment = .center
            timeLabel.text = ""
        }else {
            nameLabel.textColor = UIColor(hexString: "#616161")
            nameLabel.font = UIFont.systemFont(ofSize: 14)
            nameLabel.textAlignment = .left
        }
    }
    
    func setModel(_ model: StudyModel) {
        nameLabel.text = model.title
        timeLabel.text = "\(model.createTime.year!)-\(model.createTime.monthOfYear!)-\(model.createTime.dayOfMonth!)"
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        nameLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(10)
            make.right.equalTo(self.contentView.snp.right).offset(-5)
            make.top.equalTo(self.contentView.snp.top).offset(15)
        }
        
        timeLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(10)
            make.top.equalTo(self.nameLabel.snp.bottom).offset(10)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

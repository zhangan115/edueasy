//
//  HomeEnrollCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeEnrollCell: UITableViewCell {
    
    lazy var examLabel: UILabel = {
        let label = UILabel()
        self.contentView.addSubview(label)
        label.textColor = UIColor(hexString: "#262626")
        label.font = UIFont.systemFont(ofSize: 16)
        label.text = "成人高考"
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        examLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.top.equalTo(self.contentView.snp.top).offset(15)
        }
        self.contentView.layoutIfNeeded()
        
        let names = ["考试时间", "学费缴纳", "院校专业", "考试时间", "学历介绍", "考试技巧", "视频课堂", "学历查询"]
        let imageNames = ["icon_btn_time", "payment_icon", "icon_btn_college", "icon_btn_examination",
                          "icon_btn_educational", "icon_btn_course", "icon_btn_consult", "icon_btn_query"]
        let marginLeft: CGFloat = 10
        let width: CGFloat = 60
        let space: CGFloat = (self.bounds.size.width - width * 4 - marginLeft * 2) / 3
        for (idx, name) in names.enumerated() {
            let button = createButton()
            button.tag = idx + 1
            button.addTarget(self, action: #selector(buttonTapHandler(_:)), for: .touchUpInside)
            self.contentView.addSubview(button)
            button.setTitle(name, for: .normal)
            button.setImage(UIImage(named: imageNames[idx]), for: .normal)
            button.frame = CGRect(x: marginLeft + (width + space) * CGFloat(Int(idx % 4)),
                                  y: examLabel.frame.maxY + 20 + (width + 35 + 15) * CGFloat(Int(idx / 4)),
                                  width: width,
                                  height: width)
            button.layout(imagePosition: .top, titleSpace: 15)
        }
    }
    
    @objc func buttonTapHandler(_ sender: UIButton) {
        if !isDidLogin {
            alertController()
            return
        }
        if sender.tag == 6 {
            let study = StudyPageController()
            currentViewController.pushVC(study)
            return
        }
        let controller = HomeDetailController()
        if sender.tag == 8 {
            controller.request(urlString: "http://www.chsi.com.cn")
        }else if (sender.tag == 2){
            let controller = PayTableViewController()
            self.currentViewController.pushVC(controller)
            return
        }else {
            controller.setId(sender.tag)
        }
        controller.title = sender.titleLabel?.text
        currentViewController.pushVC(controller)
    }
    
    func enrollButtonHandler() {
        if isDidLogin {
            let controller = ApplyTableController()
            self.currentViewController.pushVC(controller)
        }else {
            alertController()
        }
    }
    // 交钱
    func toOpenPayViewController(){
        if isDidLogin {
            let controller = ApplyTableController()
            self.currentViewController.pushVC(controller)
        }else {
            alertController()
        }
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.setTitleColor(UIColor(hexString: "#262626"), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        return button
    }
}

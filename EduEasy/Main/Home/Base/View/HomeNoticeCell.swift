//
//  HomeNoticeCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class HomeNoticeCell: UITableViewCell {
    
    lazy var leftView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var rightView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.white
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var speakerImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "icon_notice"))
        self.leftView.addSubview(imageView)
        return imageView
    }()
    
    lazy var contentLabel: MarqueeLabelAction = {
        let frame = CGRect(x: 10, y: 15,
                           w: self.contentView.bounds.size.width - 10,
                           h: 20)
        let label = MarqueeLabelAction(frame: frame)
        label.textFont = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor(hexString: "#535353")!
        self.rightView.addSubview(label)
        return label
    }()
    
    let timerSource = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.global())
    
    func setContent(_ content: String) {
        contentLabel.text = content
    }
    
    func setupTimer() {
        timerSource.setEventHandler {[weak self] in
            guard let `self` = self else {
                return
            }
            DispatchQueue.main.async {
                var frame : CGRect = self.contentLabel.frame
                frame.origin.x -= 6
                if frame.maxX <= 0 {
                    frame.origin.x = self.bounds.size.width
                }
                self.contentLabel.frame = frame
            }
        }
        timerSource.scheduleRepeating(deadline: .now(), interval: .milliseconds(100))
        timerSource.resume()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        leftView.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.width.equalTo(50)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        rightView.snp.updateConstraints { (make) in
            make.left.equalTo(self.leftView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
            make.right.equalTo(self.contentView.snp.right)
        }
        
        speakerImageView.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.leftView.snp.centerY)
            make.left.equalTo(self.leftView.snp.left).offset(15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
}

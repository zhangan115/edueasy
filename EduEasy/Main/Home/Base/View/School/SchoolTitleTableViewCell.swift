//
//  SchoolTitleTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/17.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class SchoolTitleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
    
}

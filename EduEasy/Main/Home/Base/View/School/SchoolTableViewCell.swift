//
//  SchoolTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/17.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolLeftImageView: UIImageView!
    
    @IBOutlet weak var schoolRightImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
    func setModel(leftModel:StudyModel?,rightMdeol:StudyModel?) {
        if let left = leftModel {
            schoolLeftImageView.isHidden = false
            schoolLeftImageView.loadNetworkImage(left.firstPic, placeholder: "school_icon")
            schoolLeftImageView.isUserInteractionEnabled = true
            schoolLeftImageView.tag = left.id
            let tagGestureRecord = UITapGestureRecognizer(target: self, action: #selector(showShcoolDetail(_:)))
            schoolLeftImageView.addGestureRecognizer(tagGestureRecord)
            
        }else{
            schoolLeftImageView.isHidden = true
        }
        if let right = rightMdeol {
            schoolRightImageView.isHidden = false
            schoolRightImageView.loadNetworkImage(right.firstPic, placeholder: "school_icon")
            schoolRightImageView.isUserInteractionEnabled = true
            schoolRightImageView.tag = right.id
            let tagGestureRecord = UITapGestureRecognizer(target: self, action: #selector(showShcoolDetail(_:)))
            schoolRightImageView.addGestureRecognizer(tagGestureRecord)
        }else{
            schoolRightImageView.isHidden = true
        }
    }
    
    var imageClick: ((Int)->())?
    
    @objc func showShcoolDetail(_ sender: UIGestureRecognizer)  {
        let tag = sender.view?.tag
        imageClick!(tag ?? -1)
    }
    
}

//
//  HomeBannerCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import FSPagerView
class HomeBannerCell: UITableViewCell ,FSPagerViewDataSource,FSPagerViewDelegate{
    
    var models = [StudyModel]()
    var currentPositionHandle: ((Int)->())?
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return models.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.loadNetworkImage(models[index].firstPic, placeholder: "icon_banner")
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pagerControl.currentPage = targetIndex
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pagerControl.currentPage = index
        if !isDidLogin {
            alertController()
            return
        }
        let controller = StudyDetailController()
        controller.title = models[index].title
        let model = models[index]
        controller.setHtmlString(model.detail ?? "")
        self.currentViewController.pushVC(controller)
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pagerControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pagerControl.currentPage = pagerView.currentIndex
        if currentPositionHandle != nil {
            self.currentPositionHandle!(self.pagerControl.currentPage)
        }
    }
    
    lazy var contentBannerView : UIView = {
        let view = UIView(frame: self.contentView.bounds)
        view.backgroundColor = UIColor.white
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var viewPager : FSPagerView = {
        let viewPager = FSPagerView()
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        viewPager.automaticSlidingInterval = 3
        viewPager.interitemSpacing = 0
        viewPager.isInfinite = true
        return viewPager
    }()
    
    
    lazy var pagerControl:FSPageControl = {
        let pageControl = FSPageControl()
        pageControl.numberOfPages = 1
        pageControl.itemSpacing = 10
        pageControl.contentHorizontalAlignment = .center
        pageControl.setFillColor(.white, for: .selected)
        pageControl.setFillColor(.gray, for: .normal)
        return pageControl
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setHiddenSeparatorView()
        contentBannerView.snp.updateConstraints{(make)in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    func setModel(_ models:[StudyModel]) {
        self.models = models
    
        self.contentBannerView.addSubview(self.viewPager)
        self.viewPager.snp.updateConstraints{(make)in
            make.left.right.top.bottom.equalToSuperview()
        }
        self.contentBannerView.addSubview(self.pagerControl)
        self.pagerControl.snp.updateConstraints{(make)in
            make.bottom.equalToSuperview().offset(-5)
            make.left.right.equalToSuperview()
            make.height.equalTo(8)
        }
        self.pagerControl.numberOfPages = self.models.count
        self.viewPager.reloadData()
    }
}

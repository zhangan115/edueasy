//
//  SchoolListController.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/17.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import PromiseKit

private let schoolCellReuseIdentifier = "SchoolTableViewCell"
class SchoolListController: BeanTableController {
    
    var schoolList: [StudyModel]!
    let disposeBag = DisposeBag()
    let viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "院校招生"
        tableView.estimatedRowHeight = 44
        schoolList = [StudyModel]()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "SchoolTableViewCell", bundle: nil), forCellReuseIdentifier: schoolCellReuseIdentifier)
        viewModel.schoolAllRequest().subscribe(onSuccess: { [weak self](list) in
            if self == nil {
                return
            }
            self?.schoolList.removeAll()
            self?.schoolList = list
            self?.tableView.reloadData()
            }, onError: { error in
                
        }).disposed(by: disposeBag)
    }
    
}


extension SchoolListController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if schoolList != nil {
            if schoolList.count % 2 == 0 {
                return schoolList.count / 2
            }
            return schoolList.count / 2 + 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: schoolCellReuseIdentifier, for: indexPath) as! SchoolTableViewCell
        var leftModel:StudyModel?
        var rightMdeol:StudyModel?
        let currentPosition = (indexPath.row) * 2
        if currentPosition <= schoolList.count {
            leftModel = schoolList[currentPosition]
        }
        if currentPosition + 1 < schoolList.count{
            rightMdeol = schoolList[currentPosition+1]
        }
        cell.setModel(leftModel: leftModel, rightMdeol: rightMdeol)
        cell.imageClick = { (id) in
            for item in self.schoolList {
                if item.id == id {
                    let controller = StudyDetailController()
                    controller.title = item.title
                    controller.setHtmlString(item.detail ?? "")
                    self.pushVC(controller)
                    break
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.frame.width - 30) / 2 * 15 / 16
    }
}

//
//  PayChooseTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class PayChooseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var resultLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setHiddenSeparatorView()
    }
}

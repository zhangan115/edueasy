//
//  UserInfoTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage:UIImageView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var cardNum:UILabel!
    @IBOutlet weak var userVierw:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let user = UserModel.unarchiver()
        if user != nil {
            setModel(user!)
        }
        let layer = userVierw.layer
        layer.masksToBounds=false
        layer.cornerRadius = 4
        userImage.layer.masksToBounds = true
        userImage.layer.cornerRadius = 30
        userImage.isUserInteractionEnabled = true
    }
    
    private func setModel(_ user:UserModel) {
        userImage.loadNetworkImage(user.headImage, placeholder: "img_avatar")
        name.text = "姓名:" + user.realName
        if let num = user.idcard {
            cardNum.text = "身份证:" + num
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setHiddenSeparatorView()
    }
}

//
//  PayTitleTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class PayTitleTableViewCell: UITableViewCell {
    
    lazy var titleView: UILabel = {
        let label = UILabel()
        label.text = "缴纳2018-2019学费"
        label.textColor = UIColor(hexString: "#333333")
        self.contentView.addSubview(label)
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.contentView.backgroundColor = UIColor(hexString: "#f2f2f2")
        titleView.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(10)
            make.centerY.equalToSuperview()
        }
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setHiddenSeparatorView()
    }

}

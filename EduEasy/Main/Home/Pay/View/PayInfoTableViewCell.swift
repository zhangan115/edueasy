//
//  PayInfoTableViewCell.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit

class PayInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stateImage:UIImageView!
    @IBOutlet weak var studyMoney:UILabel!
    @IBOutlet weak var tableMoney:UILabel!
    @IBOutlet weak var allMoney:UILabel!
    @IBOutlet weak var subButton:UIButton!
    
    var callback: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let layer = subButton.layer
        layer.masksToBounds=false
        layer.cornerRadius = 4
    }
    
    func setModel(_ isSelect:Bool,_ studyMoneyStr:String?,_ tableMoneyStr:String? ,_ allMoneyStr:String?) {
        if isSelect {
            stateImage.image = UIImage(named: "icon_btn_radio2")
        }else{
            stateImage.image = UIImage(named: "icon_btn_radio3")
        }
        if let study = studyMoneyStr {
            studyMoney.text = study
        }else{
            studyMoney.text = ""
        }
        if let table = tableMoneyStr {
            tableMoney.text = table
        }else{
            tableMoney.text = ""
        }
        if let all = allMoneyStr {
            allMoney.text = all
        }else{
            allMoney.text = ""
        }
    }
    
    @IBAction func subClick(sender:UIButton){
        callback?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setHiddenSeparatorView()
    }
}

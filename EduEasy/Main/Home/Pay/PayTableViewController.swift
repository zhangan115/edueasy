//
//  PayTableViewController.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import UIKit
import RxSwift
private let userInfoTableViewCell = "UserInfoTableViewCell"
private let payChooseTableViewCell = "PayChooseTableViewCell"
private let payInfoTableViewCell = "PayInfoTableViewCell"
private let payTitleTableViewCell = "PayTitleTableViewCell"

class PayTableViewController: BeanTableController {
    var chooseItemList = ["选择院校","选择类别","选择年级"]
    var resultItemList = ["","",""]
    var gradItemList = ["一年级","二年级","三年级"]
    var studyMoneyStr:String?
    var tableMoneyStr:String?
    var allMoneyStr:String?
    var isSelect:Bool = false
    
    let viewModel = VipViewModel()
    let disposeBag = DisposeBag()
    var weiXinPayModel : WeiXinPayModel?
    var dataList = [PaySchoolModel]()
    
    var schoolId:Int = -1
    var typeId:Int = -1
    var gradeId:Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "学费缴纳"
        self.tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.bounces = false
        tableView.separatorStyle = .none
        self.view.backgroundColor = UIColor(hexString: "#f2f2f2")
        self.tableView.backgroundColor = UIColor(hexString: "#f2f2f2")
        tableView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil)
            , forCellReuseIdentifier: userInfoTableViewCell)
        tableView.register(PayTitleTableViewCell.self, forCellReuseIdentifier: payTitleTableViewCell)
        tableView.register(UINib(nibName: "PayChooseTableViewCell", bundle: nil)
            , forCellReuseIdentifier: payChooseTableViewCell)
        tableView.register(UINib(nibName: "PayInfoTableViewCell", bundle: nil)
            , forCellReuseIdentifier: payInfoTableViewCell)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playSuccessfully"))
            .subscribe(onNext: {[weak self] (sender) in
                let model = sender.object as! AlPayResult
                self?.playSuccessfullyMonitor(model.alipay_trade_app_pay_response.out_trade_no ,model.alipay_trade_app_pay_response.trade_no)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playWeiXinSuccessfully"))
            .subscribe(onNext: {[weak self] (sender) in
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    self?.view.showAutoHUD("支付成功")
                })
                self?.playSuccessfullyMonitor(self?.weiXinPayModel?.outTradeNo,nil)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playWeiXinFail"))
            .subscribe(onNext: {[weak self] (sender) in
                if self == nil{
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    self?.view.showAutoHUD("支付失败")
                })
            }).disposed(by: disposeBag)
        viewModel
            .requestSchoolList()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] (list) in
                self?.dataList = list
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    func request(isAilpay:Bool) {
        var isNot = "1"
        if self.isSelect {
            isNot = "0"
        }
        if isAilpay {
            vipProvider.requestResult(.paySchooMoney(schoolId: String(self.schoolId),majorTypeId:String(self.typeId)
                , tuitionId: String(self.typeId)
                , classGrade: String(self.gradeId)
                ,payType:"1",isnot:isNot), success: {[weak self] (json) in
                    let orderString = json["data"]
                    self?.pay(orderString: orderString.stringValue)
            })
        }else{
            if WXApi.isWXAppInstalled(){
                self.view.showAutoHUD("没有安装微信")
                return
            }
            vipProvider.requestResult(.paySchooMoney(schoolId: String(self.schoolId),majorTypeId:String(self.typeId)
                , tuitionId: String(self.typeId)
                , classGrade: String(self.gradeId)
                ,payType:"0",isnot:isNot), success: {[weak self] (json) in
                    if self != nil {
                        self?.weiXinPayModel = WeiXinPayModel.init(fromJson: json["data"])
                        if  self?.weiXinPayModel != nil {
                            WXApi.send(WeiXinPayUtil.toPay(model:self!.weiXinPayModel!))
                        }
                    }
            })
        }
    }
    
    func pay(orderString: String) {
        let appScheme = "alipaySDK"
        AlipaySDK.defaultService().payOrder(orderString, fromScheme: appScheme) { (resultDic) in
            print("resultDic = ", resultDic ?? [:])
            PaymentUtils.ailpaySuccessfully(with: resultDic)
        }
    }
    
    func playSuccessfullyMonitor(_ outTradeNo:String!,_ trade_no:String?) {
        viewModel.requestPayCallBack(outTradeNo,trade_no).observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] (model) in
                UserModel.archiverUser(model)
                self?.cleanAllData()
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    func cleanAllData() {
        self.resultItemList.removeAll()
        self.resultItemList = ["","",""]
        self.schoolId = -1
        self.typeId = -1
        self.gradeId = -1
        self.studyMoneyStr = nil
        self.tableMoneyStr = nil
        self.allMoneyStr = nil
    }
    
}

extension PayTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return chooseItemList.count + 1
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: userInfoTableViewCell, for: indexPath)
            return cell
        }
        if indexPath.section == 1 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: payTitleTableViewCell, for: indexPath)
            return cell
        }
        if indexPath.section == 1  {
            let cell = tableView.dequeueReusableCell(withIdentifier: payChooseTableViewCell, for: indexPath) as! PayChooseTableViewCell
            let position = indexPath.row - 1
            cell.titleLabel.text = self.chooseItemList[position]
            if position < self.resultItemList.count {
                cell.resultLabel.text = self.resultItemList[position]
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: payInfoTableViewCell, for: indexPath) as! PayInfoTableViewCell
        cell.setModel(self.isSelect, self.studyMoneyStr, self.tableMoneyStr, self.allMoneyStr)
        cell.callback = {[weak self] in
            if self == nil {
                return
            }
            if self?.schoolId == -1 {
                return
            }
            if self?.typeId == -1 {
                return
            }
            if self?.gradeId == -1 {
                return
            }
            if !UserDefaults.standard.bool(forKey: "review") {
                self?.view.showAutoHUD("暂不开放")
                return
            }
            let itemList = ["支付宝","微信"]
            let actionSheet = ActionSheet(cancelButton: true ,buttonList: itemList)
            self?.present(actionSheet, animated: false, completion: nil)
            actionSheet.handler = {[weak self] idx in
                guard let `self` = self else {
                    return
                }
                self.request(isAilpay: idx == 0)
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 160
        }
        
        if indexPath.section == 2 {
            return 166
        }
        return 38
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 1 {
                let schoolList = self.dataList.map{$0.schoolName ?? ""}
                let actionSheet = ActionSheet(buttonList: schoolList)
                self.present(actionSheet, animated: false, completion: nil)
                actionSheet.handler = {[weak self] idx in
                    guard let `self` = self else {
                        return
                    }
                    self.resultItemList.remove(at: 0)
                    self.resultItemList.insert(schoolList[idx], at: 0)
                    self.resultItemList.remove(at: 1)
                    self.resultItemList.insert("", at: 1)
                    self.resultItemList.remove(at: 2)
                    self.resultItemList.insert("", at: 2)
                    self.schoolId = self.dataList[idx].schoolId
                    self.typeId = -1
                    self.gradeId = -1
                    self.studyMoneyStr = nil
                    self.tableMoneyStr = nil
                    self.allMoneyStr = nil
                    self.tableView.reloadData()
                }
            }
            if indexPath.row == 2 {
                if self.schoolId != -1 {
                    var itemList = [String]()
                    var tuitionList = [PaySchoolModel.PayTuitionList]()
                    for data in self.dataList {
                        if data.schoolId == self.schoolId {
                            for item in data.tuitionList {
                                tuitionList.append(item)
                                itemList.append(item.tuitionType)
                            }
                        }
                    }
                    let actionSheet = ActionSheet(buttonList: itemList)
                    self.present(actionSheet, animated: false, completion: nil)
                    actionSheet.handler = {[weak self] idx in
                        guard let `self` = self else {
                            return
                        }
                        self.resultItemList.remove(at: 1)
                        self.resultItemList.insert(itemList[idx], at: 1)
                        self.resultItemList.remove(at: 2)
                        self.resultItemList.insert("", at: 2)
                        self.typeId = tuitionList[idx].tuitionId
                        self.gradeId = -1
                        self.studyMoneyStr = nil
                        self.tableMoneyStr = nil
                        self.allMoneyStr = nil
                        self.gradItemList.removeAll()
                        if tuitionList[idx].grade1Tuition != nil && tuitionList[idx].grade1Tuition.count != 0 {
                            self.gradItemList.append("一年级")
                        }
                        if tuitionList[idx].grade1Tuition != nil && tuitionList[idx].grade2Tuition.count != 0 {
                            self.gradItemList.append("二年级")
                        }
                        if tuitionList[idx].grade1Tuition != nil && tuitionList[idx].grade3Tuition.count != 0 {
                            self.gradItemList.append("三年级")
                        }
                        self.tableView.reloadData()
                    }
                }
            }
            if indexPath.row == 3 {
                if self.typeId != -1 {
                    let actionSheet = ActionSheet(buttonList: gradItemList)
                    self.present(actionSheet, animated: false, completion: nil)
                    actionSheet.handler = {[weak self] idx in
                        guard let `self` = self else {
                            return
                        }
                        self.resultItemList.insert(self.gradItemList[idx], at: 2)
                        self.gradeId = (idx+1)
                        var allMoney:Float? = 0
                        var currentTuition : PaySchoolModel.PayTuitionList?
                        for school in self.dataList {
                            if school.schoolId == self.schoolId {
                                for item in school.tuitionList {
                                    if item.tuitionId == self.typeId {
                                        currentTuition = item
                                        break
                                    }
                                }
                            }
                        }
                        if currentTuition == nil {
                            return
                        }
                        if self.gradeId == 1 {
                            self.isSelect = true
                            allMoney = currentTuition!.grade1Tuition.toFloat()
                            if allMoney != nil{
                                allMoney = allMoney! + currentTuition!.netStudyFee.toFloat()!
                                self.studyMoneyStr = currentTuition!.grade1Tuition
                                self.tableMoneyStr = currentTuition!.netStudyFee + "元/" + currentTuition!.netStudyYear + "年"
                                self.allMoneyStr = String(allMoney!)
                            }
                            
                        }else if self.gradeId == 2 {
                            self.isSelect = false
                            self.studyMoneyStr = currentTuition!.grade2Tuition
                            self.tableMoneyStr = nil
                            self.allMoneyStr = currentTuition!.grade2Tuition
                        }else if self.gradeId == 3 {
                            self.isSelect = false
                            
                            self.studyMoneyStr = currentTuition!.grade3Tuition
                            self.tableMoneyStr = nil
                            self.allMoneyStr = currentTuition!.grade3Tuition
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

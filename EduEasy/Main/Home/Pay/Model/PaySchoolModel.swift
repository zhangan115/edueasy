//
//  PaySchoolModel.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON

class PaySchoolModel: Mappable {
    var schoolName:String!
    var schoolId:Int!
    var tuitionList :[PayTuitionList]!
    
    required init(fromJson json: JSON!) {
        if json == nil {
            return
        }
        self.schoolName = json["schoolName"].stringValue
        self.schoolId = json["schoolId"].intValue
        self.tuitionList = [PayTuitionList]()
        let jsonArray = json["tuitionList"].arrayValue
        for json in jsonArray{
            let value = PayTuitionList(fromJson: json)
            self.tuitionList.append(value)
        }
    }
    
    class PayTuitionList: Mappable {
        var tuitionType:String!
        var desc:String!
        var grade1Tuition:String!
        var grade2Tuition:String!
        var grade3Tuition:String!
        var netStudyFee:String!
        var netStudyYear:String!
        var tuitionId:Int!
        var tuitionStandard:String!
       
        required init(fromJson json: JSON!) {
            if json == nil {
                return
            }
            self.desc = json["desc"].stringValue
            self.grade1Tuition = json["grade1Tuition"].stringValue
            self.grade2Tuition = json["grade2Tuition"].stringValue
            self.grade3Tuition = json["grade3Tuition"].stringValue
            self.netStudyFee = json["netStudyFee"].stringValue
            self.netStudyYear = json["netStudyYear"].stringValue
            self.tuitionId = json["tuitionId"].intValue
            self.tuitionStandard = json["tuitionStandard"].stringValue
            self.tuitionType = json["tuitionType"].stringValue
        }
    }
}

//
//  HomeDetailController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class HomeDetailController: BeanController {
    
    lazy var webView: UIWebView = {
        let webView = UIWebView(frame: self.view.bounds)
        webView.contentMode = .scaleAspectFit
//        webView.scalesPageToFit = true
        webView.delegate = self
        self.view.addSubview(webView)
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        webView.frame = self.view.bounds
    }
    
    func request(urlString: String) {
        webView.loadRequest(URLRequest(url: URL(string: urlString)!))
    }
    
    func setUrlString(_ string: String) {
        let urlString = Config.baseURL.absoluteString + string
        var request = URLRequest(url: URL(string: urlString)!)
        let dic = ["token": kToken]
        let data = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
        request.httpBody = data
        request.httpMethod = "POST"
        webView.loadRequest(request)
    }
    func setId(_ id:Int)  {
        studyProvider.requestResultString(.getWebData(id: id), success: {[weak self] (str) in
            self?.setHtmlString(str)
        })
    }
    
    func setHtmlString(_ string: String) {
        webView.loadHTMLString(styledHTML(string), baseURL: Config.baseURL)
    }
    
    func styledHTML(_ html: String) -> String {
        let style: String = "<head><meta charset=\"UTF-8\"></head>"
        return style + html
    }
}

extension HomeDetailController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let string = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '60%'"
        self.webView.stringByEvaluatingJavaScript(from: string)
    }
}

//
//  ApplyViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift

class ApplyViewModel {
    func request() -> Single<[ApplyModel]> {
        return homeProvider.rxRequest(.schoolList).toListModel(type: ApplyModel.self)
    }
}

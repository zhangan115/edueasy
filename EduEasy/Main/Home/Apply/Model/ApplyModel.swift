//
//	ApplyModel.swift
//
//	Create by piggybear on 7/5/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ApplyModel: Mappable{

	var schoolId : Int!
	var schoolName : String!
	var specialtyCatalogs : [ApplySpecialtyCatalog]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		schoolId = json["schoolId"].intValue
		schoolName = json["schoolName"].stringValue
		specialtyCatalogs = [ApplySpecialtyCatalog]()
		let specialtyCatalogsArray = json["specialtyCatalogs"].arrayValue
		for specialtyCatalogsJson in specialtyCatalogsArray{
			let value = ApplySpecialtyCatalog(fromJson: specialtyCatalogsJson)
			specialtyCatalogs.append(value)
		}
	}

}

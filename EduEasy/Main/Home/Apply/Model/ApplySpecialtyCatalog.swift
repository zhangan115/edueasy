//
//	ApplySpecialtyCatalog.swift
//
//	Create by piggybear on 7/5/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ApplySpecialtyCatalog{

	var catalogCode : String!
	var descriptionField : String!
	var id : Int!
	var newField : Bool!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		catalogCode = json["catalogCode"].stringValue
		descriptionField = json["description"].stringValue
		id = json["id"].intValue
		newField = json["new"].boolValue
		title = json["title"].stringValue
	}

}
//
//  ApplyTableController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift

private let cellReuseIdentifier = "ApplyCell"
private let infoCellReuseIdentifier = "ApplyInfoCell"
private let buttonCellReuseIdentifier = "ApplyButtonCell"
private let textCellReuseIdentifier = "ApplyTextCell"

class ApplyTableController: BeanTableController {
    
    let viewModel = ApplyViewModel()
    let disposeBag = DisposeBag()
    
    var dataList: [ApplyModel]!
    
    struct ApplyDataModel {
        var school: String = ""
        var schoolId: Int!
        var specialtyCatalog: String = ""
        var specialtyCatalogId: Int!
        var specialtyCatalog2: String = ""
        var specialtyCatalog2Id: Int!
        var applyType: String = ""
        var applyTypeId: Int!
        var phoneNumber: String = ""
    }
    
    var dataModel = ApplyDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "我要报名"
        tableViewRegister()
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0)
        tableView.rowHeight = 48
        
        viewModel.request().observeOn(MainScheduler.instance).subscribe(onSuccess: {[weak self] (list) in
            self?.dataList = list
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    func tableViewRegister() {
        tableView.register(ApplyCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tableView.register(ApplyInfoCell.self, forCellReuseIdentifier: infoCellReuseIdentifier)
        tableView.register(ApplyButtonCell.self, forCellReuseIdentifier: buttonCellReuseIdentifier)
        let nib = UINib(nibName: "ApplyTextCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: textCellReuseIdentifier)
    }
    
    func commitButtonHandler() {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ApplyCell
        let nameText: String = cell.textField.text ?? ""
        if nameText.count == 0 {
            self.view.showAutoHUD("请输入您的真实姓名")
            return
        }
        if dataModel.schoolId == nil {
            self.view.showAutoHUD("请选择报考学校")
            return
        }
        if dataModel.specialtyCatalogId == nil {
            self.view.showAutoHUD("请选择报考专业")
            return
        }
        if dataModel.applyTypeId == nil {
            self.view.showAutoHUD("请选择报考类型")
            return
        }
       
        let cellId = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! ApplyCell
        let idcard = cellId.textField.text ?? ""
        if idcard.count == 0 {
            self.view.showAutoHUD("请输入身份证号")
            return
        }
        let cellSex = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! ApplyCell
        var sex = "1"
        if cellSex.checkboxWoman.isSelected {
            sex = "2"
        }
        
        let cell1 = tableView.cellForRow(at: IndexPath(row: 4, section: 1)) as! ApplyTextCell
        let phoneNumber: String = cell1.textField.text ?? ""
        if phoneNumber.count == 0 {
            self.view.showAutoHUD("请输入联系电话")
            return
        }
        let user = UserModel.unarchiver()
        homeProvider.requestResult(.apply(
            accountId: user?.id ?? 0,
            realName: nameText,
            sex: sex,
            schoolId: dataModel.schoolId,
            specialtyCatalogId: dataModel.specialtyCatalogId,
            specialtyCatalogId2: dataModel.specialtyCatalog2Id,
            applyType: dataModel.applyTypeId,
            phone: phoneNumber,
            userName: user?.accountName ?? "",idcard: idcard),
        success: {[weak self] (json) in
            guard let `self` = self else {
                return
            }
            
            let model = UserModel(fromJson: json["data"])
            let user = UserModel.unarchiver()
            user?.sex = model.sex
            user?.realName = model.realName
            user?.phone = model.phone
            UserModel.archiverUser(user!)
            
            self.view.showHUD("提交成功", completion: {
                self.popVC()
            })
        }) {_ in
            
        }
    }
}

extension ApplyTableController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        if section == 1 {
            return 5
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ApplyCell
            cell.setIndex(indexPath.row)
            return cell
        }
        
        if indexPath.section == 1 && indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: textCellReuseIdentifier, for: indexPath) as! ApplyTextCell
            cell.setNameText("联系电话")
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: infoCellReuseIdentifier, for: indexPath) as! ApplyInfoCell
            cell.setIndex(indexPath.row)
            if indexPath.row == 0 {
                cell.setButtonText(dataModel.school)
            }
            if indexPath.row == 1 {
                cell.setButtonText(dataModel.specialtyCatalog)
            }
            if indexPath.row == 2 {
                cell.setButtonText(dataModel.specialtyCatalog2)
            }
            if indexPath.row == 3 {
                cell.setButtonText(dataModel.applyType)
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: buttonCellReuseIdentifier, for: indexPath) as! ApplyButtonCell
        cell.buttonTapCallback = {[weak self] in
            self?.commitButtonHandler()
        }
        return cell
    }
}

extension ApplyTableController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            if dataList == nil {
                return
            }
            let schoolList = dataList.map{$0.schoolName ?? ""}
            let actionSheet = ActionSheet(buttonList: schoolList)
            self.present(actionSheet, animated: false, completion: nil)
            actionSheet.handler = {[weak self] idx in
                guard let `self` = self else {
                    return
                }
                self.dataModel.school = schoolList[idx]
                self.dataModel.schoolId = self.dataList[idx].schoolId
                
                self.dataModel.specialtyCatalog = ""
                self.dataModel.specialtyCatalogId = nil
                self.tableView.reloadData()
            }
        }
        
        if indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2) {
            if self.dataModel.schoolId == nil {
                self.view.showAutoHUD("请先选择学校")
                return
            }
            if dataList == nil {
                return
            }
            let model = dataList.filter { (model) -> Bool in
                return model.schoolId == self.dataModel.schoolId
                }.first
            guard let applyModel = model else {
                return
            }
            let list = applyModel.specialtyCatalogs
            let specialtyList = list?.map{$0.title ?? ""}
            if specialtyList == nil || list == nil {
                return
            }
            if specialtyList!.count == 0 {
                self.view.showAutoHUD("该学校下暂无报考专业")
                return
            }
            let actionSheet = ActionSheet(buttonList: specialtyList)
            self.present(actionSheet, animated: false, completion: nil)
            actionSheet.handler = {[weak self] idx in
                guard let `self` = self else {
                    return
                }
                if indexPath.row == 1 {
                    self.dataModel.specialtyCatalog = specialtyList![idx]
                    self.dataModel.specialtyCatalogId = list![idx].id
                }else {
                    self.dataModel.specialtyCatalog2 = specialtyList![idx]
                    self.dataModel.specialtyCatalog2Id = list![idx].id
                }
                self.tableView.reloadData()
            }
        }
        
        if indexPath.section == 1 && indexPath.row == 3 {
            let titleList = ["高中起点本科", "高中起点专科", "专科起点本科"]
            let actionSheet = ActionSheet(buttonList: titleList)
            self.present(actionSheet, animated: false, completion: nil)
            actionSheet.handler = {[weak self] idx in
                guard let `self` = self else {
                    return
                }
                self.dataModel.applyType = titleList[idx]
                self.dataModel.applyTypeId = idx + 1
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "header"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}

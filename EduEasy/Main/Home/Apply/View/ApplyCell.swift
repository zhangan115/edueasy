//
//  ApplyCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class ApplyCell: UITableViewCell {
    
    lazy var signLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.textColor = UIColor(hexString: "#999999")
        textField.textAlignment = .right
        self.contentView.addSubview(textField)
        return textField
    }()
    
    lazy var checkboxMan: UIButton = {
        let button = self.createButton()
        button.setTitle("男", for: .normal)
        button.isSelected = true
        self.contentView.addSubview(button)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonHandler), for: .touchUpInside)
        return button
    }()
    
    lazy var checkboxWoman: UIButton = {
        let button = self.createButton()
        button.setTitle("女", for: .normal)
        self.contentView.addSubview(button)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
        return button
    }()
    
    @objc func buttonHandler(_ sender: UIButton) {
        sender.isSelected = true
        if sender.tag == 0 {
            checkboxWoman.isSelected = false
        }else {
            checkboxMan.isSelected = false
        }
    }
    
    func setIndex(_ index: Int) {
        var names = ["姓名", "身份证号","性别"]
        if index == 0 {
            signLabel.attributedText = setAttributedString(text: names[index])
        }else {
            signLabel.text = names[index]
        }
        signLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.width.equalTo(names.count * 20)
        }
        if index == 2 {
            textField.isHidden = true
            checkboxWoman.isHidden = false
            checkboxMan.isHidden = false
        }else {
            textField.isHidden = false
            checkboxWoman.isHidden = true
            checkboxMan.isHidden = true
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.selectionStyle = .none
        
        textField.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.signLabel.snp.right).offset(5)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
        checkboxWoman.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
        checkboxMan.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.checkboxWoman.snp.left).offset(-44)
        }
        checkboxMan.layout(imagePosition: .left, titleSpace: 15)
        checkboxWoman.layout(imagePosition: .left, titleSpace: 15)
    }
    
    func setAttributedString(text: String) -> NSMutableAttributedString {
        let string: String = text + " *"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor,
                                      value: UIColor(hexString: "#FB6969")!,
                                      range: NSMakeRange(text.length, 2))
        return attributedString
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "icon_btn_radio1"), for: .normal)
        button.setImage(UIImage(named: "icon_btn_radio2"), for: .selected)
        button.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return button
    }
}

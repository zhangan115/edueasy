//
//  ApplyInfoCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class ApplyInfoCell: UITableViewCell {
    lazy var signLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var button: UIButton = {
        let button = self.createButton()
        self.contentView.addSubview(button)
        return button
    }()
    
    func setIndex(_ index: Int) {
        var names = ["报考学校", "报考专业", "报考专业二", "报考类型", "联系电话"]
        if index == 2 {
            signLabel.text = names[index]
        }else {
            signLabel.attributedText = setAttributedString(text: names[index])
        }
        signLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.width.equalTo(names.count * 20)
        }
    }
    
    func setButtonText(_ text: String) {
        button.setTitle(text, for: .normal)
        button.layout(imagePosition: .right, titleSpace: 10)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.selectionStyle = .none
        button.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
        button.layout(imagePosition: .right, titleSpace: 15)
    }
    
    func setAttributedString(text: String) -> NSMutableAttributedString {
        let string: String = text + " *"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor,
                                      value: UIColor(hexString: "#FB6969")!,
                                      range: NSMakeRange(text.length, 2))
        return attributedString
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.isUserInteractionEnabled = false
        button.setImage(UIImage(named: "icon_btn_arrow_r_g"), for: .normal)
        button.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return button
    }
}

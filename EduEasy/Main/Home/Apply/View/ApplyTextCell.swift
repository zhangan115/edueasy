//
//  ApplyTextCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/8.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class ApplyTextCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setNameText(_ text: String) {
        nameLabel.attributedText = setAttributedString(text: text)
    }
    
    func setAttributedString(text: String) -> NSMutableAttributedString {
        let string: String = text + " *"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor,
                                      value: UIColor(hexString: "#FB6969")!,
                                      range: NSMakeRange(text.length, 2))
        return attributedString
    }
    
}

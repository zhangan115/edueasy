//
//  ApplyButtonCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class ApplyButtonCell: UITableViewCell {

    lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        self.contentView.addSubview(button)
        button.setTitle("提交", for: .normal)
        button.setBackgroundColor(UIColor(hexString: "#308AFF")!, forState: .normal)
        button.addTarget(self, action: #selector(buttonHandler), for: .touchUpInside)
        return button
    }()
    
    var buttonTapCallback: (()->())?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func buttonHandler() {
        buttonTapCallback?()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        button.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 8
        self.setHiddenSeparatorView()
    }
}

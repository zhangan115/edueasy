//
//  ForgetPasswordController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class ForgetPasswordController: BeanController {

    lazy var nextView: ForgetPasswordNextView = {
        let view = ForgetPasswordNextView()
        self.view.addSubview(view)
        return view
    }()
    
    lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setBackgroundColor(UIColor(hexString: "#308AFF")!, forState: .normal)
        button.setTitle("下一步", for: .normal)
        button.addTarget(self, action: #selector(nextButtonHandler), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }()
    
    let disposeBag = DisposeBag()
    var verificationCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "忘记密码"
        nextView.callback = {[weak self] in
            self?.verifyButtonHandler()
        }
    }
    
    func verifyButtonHandler() {
        let phoneNumber = nextView.phoneNumberTextField.text ?? ""
        if phoneNumber.count == 0 {
            self.view.showAutoHUD("手机号码不能为空")
            return
        }
        if !phoneNumber.isVaildPhoneNumber {
            self.view.showAutoHUD("请输入正确的手机号码")
            return
        }
        let phone = nextView.phoneNumberTextField.text ?? ""
        forgetProvider
            .rxRequest(.forget(phone: phone))
            .toModel(type: PhoneCodeModel.self)
            .subscribe(onSuccess: { (model) in
                print("model = ", model.verificationCode)
                self.verificationCode = model.verificationCode
            }).disposed(by: disposeBag)
    }
    
    @objc func nextButtonHandler() {
        let phoneNumber = nextView.phoneNumberTextField.text ?? ""
        let code = nextView.verifyTextField.text ?? ""
        if code.count == 0 {
            self.view.showAutoHUD("验证码不能为空")
            return
        }
        if code != self.verificationCode {
            self.view.showAutoHUD("验证码不正确")
            return
        }
        let controller = SetPasswordController()
        controller.phoneNumber = phoneNumber
        controller.verificationCode = code
        self.pushVC(controller)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        nextView.frame = CGRect(x: 0, y: 15, width: self.view.bounds.size.width, height: 102)
        button.snp.updateConstraints { (make) in
            make.top.equalTo(self.nextView.snp.bottom).offset(15)
            make.left.equalTo(self.view.snp.left).offset(15)
            make.right.equalTo(self.view.snp.right).offset(-15)
            make.height.equalTo(50)
        }
    }
}

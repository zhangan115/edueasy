//
//  SetPasswordController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class SetPasswordController: BeanController {

    @IBOutlet weak var passwordTextField: SetPasswordTextField!
    @IBOutlet weak var confirmPasswordTextField: SetPasswordTextField!
    @IBOutlet weak var completionButton: UIButton!
    
    var phoneNumber: String = ""
    var verificationCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "设置密码"
        passwordTextField.setLeftText("密码")
        confirmPasswordTextField.setLeftText("确认密码")
        completionButton.layer.masksToBounds = true
        completionButton.layer.cornerRadius = 5
    }
    
    @IBAction func completionButtonHandler(_ sender: UIButton) {
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        if password.count == 0 {
            self.view.showAutoHUD("密码不能为空")
            return
        }
        
        if password != confirmPassword {
            self.view.showAutoHUD("两次密码输入不一致")
            return
        }
        
        self.view.showHUD(message: "正在提交...")
        forgetProvider.requestResult(.update(phone: phoneNumber, password: password, verificationCode: verificationCode), success: { _ in
            self.view.showHUD("提交成功", completion: {[weak self] in
                self?.view.hiddenHUD()
                self?.navigationController?.popToRootViewController(animated: true)
            })
        }) {_ in
         self.view.hiddenHUD()
        }
    }
    
}

//
//  ForgetPasswordNextView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class ForgetPasswordNextView: UIView {
    
    lazy var phoneNumberTextField: LoginTextField = {
        let textField = self.createTextField()
        textField.setLeftText("手机号")
        textField.placeholder = "请输入手机号"
        self.addSubview(textField)
        return textField
    }()
    
    lazy var verifyTextField: LoginTextField = {
        let textField = self.createTextField()
        textField.setLeftText("验证码")
        textField.placeholder = "请输入验证码"
        self.addSubview(textField)
        return textField
    }()
    
    lazy var verifyCodeButton: CountDownButton = {
        let button = CountDownButton(type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor(hexString: "#2772FE"), for: .normal)
        button.layer.borderColor = UIColor(hexString: "#2772FE")?.cgColor
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 1
        button.setTitle("获取验证码", for: .normal)
        button.addTarget(self, action: #selector(verifyCodeButtonHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    var callback: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func verifyCodeButtonHandler(_ sender: CountDownButton) {
        sender.isCounting = true
        callback?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let lineViewWidth: CGFloat = 0.5
        let lineView1 = createLineView()
        self.addSubview(lineView1)
        lineView1.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.snp.top)
            make.height.equalTo(lineViewWidth)
        }
        
        phoneNumberTextField.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(lineView1.snp.bottom)
            make.height.equalTo(50)
        }
        
        let lineView3 = createLineView()
        self.addSubview(lineView3)
        lineView3.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.phoneNumberTextField.snp.bottom)
            make.height.equalTo(lineViewWidth)
        }
        
        verifyTextField.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.top.equalTo(lineView3.snp.bottom)
            make.height.equalTo(50)
            make.width.equalTo(250)
        }
        
        verifyCodeButton.snp.updateConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-15)
            make.centerY.equalTo(self.verifyTextField.snp.centerY)
            make.width.equalTo(95)
            make.height.equalTo(35)
        }
        
        let lineView4 = createLineView()
        self.addSubview(lineView4)
        lineView4.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.verifyTextField.snp.bottom)
            make.height.equalTo(lineViewWidth)
        }
    }
    
    func createLineView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        return view
    }
    
    func createTextField() -> LoginTextField {
        let textField = LoginTextField()
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.textAlignment = .left
        textField.textColor = UIColor.black
        textField.keyboardType = .numberPad
        return textField
    }
    
}

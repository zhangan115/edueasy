//
//  OldExamViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/27.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift

class OldExamViewModel {
    func request(type: ExamType, isYear: YearType, title: String?, pagerType: String?, pagerYear: String?, questionCatalogId: String?) -> Single<[PaperModel]> {
        return questionBankProvider.rxRequest(.paperList(type: type, isYear: isYear, title: title, pagerType: pagerType, pagerYear: pagerYear, questionCatalogId: questionCatalogId)).toListModel(type: PaperModel.self)
    }
    
    func questionTypeRequest() -> Single<[QuestionTypeModel]> {
        return questionBankProvider.rxRequest(.questionstype).toListModel(type: QuestionTypeModel.self)
    }
}

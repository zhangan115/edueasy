//
//	PaperZone.swift
//
//	Create by piggybear on 27/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaperZone{

	var fixed : Bool!
	var iD : String!
	var uncachedZone : PaperUncachedZone!
	var ref : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		fixed = json["fixed"].boolValue
		iD = json["iD"].stringValue
		let uncachedZoneJson = json["uncachedZone"]
		if !uncachedZoneJson.isEmpty{
			uncachedZone = PaperUncachedZone(fromJson: uncachedZoneJson)
		}
		ref = json["$ref"].stringValue
	}

}
//
//	PaperModel.swift
//
//	Create by piggybear on 27/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaperModel: Mappable{

	var createTime : PaperCreateTime!
	var id : Int!
	var isYear : String!
	var newField : Bool!
	var pagerType : String!
	var pagerYear : String!
	var questionCatalog : PaperQuestionCatalog!
	var title : String!
	var type : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let createTimeJson = json["createTime"]
		if !createTimeJson.isEmpty{
			createTime = PaperCreateTime(fromJson: createTimeJson)
		}
		id = json["id"].intValue
		isYear = json["isYear"].stringValue
		newField = json["new"].boolValue
		pagerType = json["pagerType"].stringValue
		pagerYear = json["pagerYear"].stringValue
		let questionCatalogJson = json["questionCatalog"]
		if !questionCatalogJson.isEmpty{
			questionCatalog = PaperQuestionCatalog(fromJson: questionCatalogJson)
		}
		title = json["title"].stringValue
		type = json["type"].stringValue
	}

}

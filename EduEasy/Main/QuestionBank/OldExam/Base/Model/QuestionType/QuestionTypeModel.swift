//
//  QuestionTypeModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuestionTypeModel: Mappable{
    
    var id : Int!
    var new : Bool!
    var title : String!
    
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        new = json["new"].boolValue
        title = json["title"].stringValue
    }
    
}

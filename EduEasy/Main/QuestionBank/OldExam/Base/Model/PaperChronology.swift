//
//	PaperChronology.swift
//
//	Create by piggybear on 27/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaperChronology{

	var zone : PaperZone!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let zoneJson = json["zone"]
		if !zoneJson.isEmpty{
			zone = PaperZone(fromJson: zoneJson)
		}
	}

}
//
//  OldExamController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift

private let cellReuseIdentifier = "OldExamCell"

class OldExamController: BaseRequestTableController {
    
    var examType: ExamType!
    var yearType: YearType!
    var paperTitle: String!
    var pagerType: String!
    var pagerYear: String!
    var questionCatalogId: String!
    
    let viewModel = OldExamViewModel()
    let disposeBag = DisposeBag()
    
    var responseList: [PaperModel] = []
    var questionTypeList: [QuestionTypeModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0)
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableViewRegister()
//        rightButtonLogic()
        
        viewModel.questionTypeRequest().subscribe(onSuccess: {[weak self] (list) in
            self?.questionTypeList = list
            }, onError: nil).disposed(by: disposeBag)
    }
    
    func tableViewRegister() {
        tableView.register(OldExamCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    override func request() {
        viewModel.request(type: examType, isYear: yearType, title: paperTitle, pagerType: pagerType, pagerYear: pagerYear, questionCatalogId: questionCatalogId).subscribe(onSuccess: {[weak self] (list) in
            self?.tableView.mj_header.endRefreshing()
            self?.responseList = list
            self?.tableView.safeReloadData()
        }) {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
        }.disposed(by: disposeBag)
    }
    
    func rightButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(rightButtonHandler))
        button.setTitle("筛选", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor.white, for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func rightButtonHandler() {
        let controller = OldExamFilterAnimationController()
        controller.questionTypeList = questionTypeList
        controller.callback = { [weak self] (title, paperType, pagerYear, questionCatalogId) in
            guard let `self` = self else {
                return
            }
            
            self.paperTitle = title.count == 0 ? nil : title
            self.pagerType = paperType.count == 0 ? nil : paperType
            self.pagerYear = pagerYear.count == 0 ? nil : pagerYear
            self.questionCatalogId = questionCatalogId.count == 0 ? nil : questionCatalogId
            self.tableView.mj_header.beginRefreshing()
        }
        self.present(controller, animated: false, completion: nil)
    }
}

extension OldExamController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! OldExamCell
        cell.setModel(responseList[indexPath.row])
        return cell
    }
}

extension OldExamController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = ExamPageController()
        controller.paperId = responseList[indexPath.row].id
        self.pushVC(controller)
    }
}

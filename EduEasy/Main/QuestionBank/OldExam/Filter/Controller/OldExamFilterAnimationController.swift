//
//  OldExamFilterAnimationController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamFilterAnimationController: OldExamFilterController {

    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0)
        self.view.addSubview(view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissLogic))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    let widthSpace: CGFloat = 75
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.modalPresentationStyle = .custom
        view.sendSubview(toBack: overlayView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let width: CGFloat = view.bounds.size.width - widthSpace
        
        var tableViewFrame = tableView.frame
        tableViewFrame.origin.x = view.bounds.size.width
        tableViewFrame.size.width = width
        tableView.frame = tableViewFrame
        
        var buttonViewFrame = buttonView.frame
        buttonViewFrame.origin.x = view.bounds.size.width
        buttonViewFrame.size.width = width
        buttonView.frame = buttonViewFrame
        
        overlayView.frame = view.bounds
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var tableViewFrame = tableView.frame
        tableViewFrame.origin.x = widthSpace
        
        var buttonViewFrame = buttonView.frame
        buttonViewFrame.origin.x = widthSpace
        
        UIView.animate(withDuration: 0.3) {
            self.buttonView.frame = buttonViewFrame
            self.tableView.frame = tableViewFrame
            self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        }
    }
    
    override func dismiss() {
        dismissLogic()
    }

    @objc func dismissLogic() {
        var tableViewFrame = tableView.frame
        tableViewFrame.origin.x = view.bounds.size.width
        
        var buttonViewFrame = buttonView.frame
        buttonViewFrame.origin.x = view.bounds.size.width
        
        UIView.animate(withDuration: 0.3, animations: {
            self.buttonView.frame = buttonViewFrame
            self.tableView.frame = tableViewFrame
            self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0)
        }) { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

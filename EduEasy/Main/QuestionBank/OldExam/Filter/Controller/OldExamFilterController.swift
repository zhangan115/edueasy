//
//  OldExamFilterController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamFilterController: UIViewController {

    let nameCellReuseIdentifier = "OldExamNameCell"
    let styleCellReuseIdentifier = "OldExamPaperStyleCell"
    let headerList = ["试卷名称", "试卷类型", "试卷年份", "试卷科目"]
    
    lazy var buttonView: OldExamButtonView = {
        let view = OldExamButtonView()
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        return view
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        return tableView
    }()
    
    var questionTypeList: [QuestionTypeModel]?
    var callback: ((_ title: String, _ paperType: String, _ pagerYear: String, _ questionCatalogId: String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewRegister()
        
        buttonView.resetButtonClosure = {[weak self] in
            guard let `self` = self else {
                return
            }
            let nameCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OldExamNameCell
            nameCell.textField.text = ""
            
            for idx in 1..<4 {
                let styleCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: idx)) as! OldExamPaperStyleCell
                styleCell.buttonList.setAll({ (_, button) in
                    button.isSelected = false
                })
            }
            self.tableView.reloadData()
        }
        
        buttonView.sureButtonClosure = {[weak self] in
            guard let `self` = self else {
                return
            }
            var  title = "", paperType = "", pagerYear = "", questionCatalogId = ""
            let nameCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OldExamNameCell
            title = nameCell.textField.text!
            
            for idx in 1..<4 {
                let styleCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: idx)) as! OldExamPaperStyleCell
                let idList = styleCell.buttonList.filter{$0.isSelected == true}.map{$0.id ?? ""}
                if idx == 1 {
                    paperType = idList.joined(separator: "、")
                }else if idx == 2 {
                    pagerYear = idList.joined(separator: "、")
                }else if idx == 3 {
                    questionCatalogId = idList.joined(separator: "、")
                }
            }
            self.callback?(title, paperType, pagerYear, questionCatalogId)
            self.dismiss()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setSubviewsFrame()
    }
    
    func setSubviewsFrame() {
        let buttonViewHeight: CGFloat = 60
        buttonView.frame = CGRect(x: 0,
                                  y: self.view.bounds.size.height - buttonViewHeight,
                                  w: self.view.bounds.size.width,
                                  h: buttonViewHeight)
        tableView.frame = CGRect(x: 0, y: 0, w: self.view.bounds.size.width, h: buttonView.frame.minY)
    }
    
    func tableViewRegister() {
        tableView.register(OldExamNameCell.self, forCellReuseIdentifier: nameCellReuseIdentifier)
        tableView.register(OldExamPaperStyleCell.self, forCellReuseIdentifier: styleCellReuseIdentifier)
    }
    
    func dismiss() {
        
    }
}

extension OldExamFilterController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: nameCellReuseIdentifier, for: indexPath) as! OldExamNameCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: styleCellReuseIdentifier, for: indexPath) as! OldExamPaperStyleCell
        cell.set(section: indexPath.section, list: questionTypeList)
        return cell
     }
}

extension OldExamFilterController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "header"
        var view: OldExamHeaderView? = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? OldExamHeaderView
        if view == nil {
            view = OldExamHeaderView(reuseIdentifier: headerIdentifier)
        }
        view?.setText(headerList[section])
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
}

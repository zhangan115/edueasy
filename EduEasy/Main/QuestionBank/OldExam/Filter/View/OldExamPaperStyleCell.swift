//
//  OldExamPaperStyleCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamPaperStyleCell: UITableViewCell {

    var buttonList: [OldExamPaperButton] = [OldExamPaperButton]()
    
    override func draw(_ rect: CGRect) {
        self.selectionStyle = .none
        self.setHiddenSeparatorView()
    }
    
    func set(section: Int, list: [QuestionTypeModel]?) {
        if section == 1 {
            let titleList = ["高中起点本科", "高中起点专科", "专科起点本科"]
            let idList = ["1", "2", "3"]
            set(titleList: titleList, idList: idList, index: 3)
        }else if section == 2 {
            let titleList = [(Date().year - 1).toString, Date().year.toString, (Date().year + 1).toString]
            set(titleList: titleList, idList: titleList, index: 3)
        }else if let list = list {
            let titleList = list.map{$0.title ?? ""}
            let idList = list.map{$0.id.toString}
            set(titleList: titleList, idList: idList, index: 3)
        }
    }
    
    // index 表示一排放几个
    func set(titleList: [String], idList: [String], index: Int) {
        buttonList.setAll { (_, button) in
            button.removeFromSuperview()
        }
        buttonList.removeAll()
        let leftMargin: CGFloat = 15
        let space: CGFloat = 12
        let width = (self.bounds.size.width - leftMargin * 2 - space * (CGFloat(index) - 1)) / CGFloat(index)
        let height: CGFloat = 34
        for (idx, title) in titleList.enumerated() {
            let button = createButton()
            button.id = idList[idx]
            button.setTitle(title, for: .normal)
            button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
            button.frame = CGRect(x: leftMargin + (width + space) * CGFloat(Int(idx % index)),
                                  y: (height + space) * CGFloat(Int(idx / index)),
                                  w: width,
                                  h: height)
            self.contentView.addSubview(button)
            buttonList.append(button)
        }
    }
    
    @objc func buttonHandler(_ sender: UIButton) {
        buttonList.setAll { (_, button) in
            button.isSelected = false
        }
        sender.isSelected = !sender.isSelected
    }
    
    func createButton() -> OldExamPaperButton {
        let button = OldExamPaperButton(type: .custom)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
        button.setTitleColor(UIColor(hexString: "#308AFF"), for: .selected)
        button.setBackgroundColor(UIColor(hexString: "#F4F4F4")!, forState: .normal)
        button.setBackgroundColor(UIColor(hexString: "#BAD8FF")!, forState: .selected)
        return button
    }
}

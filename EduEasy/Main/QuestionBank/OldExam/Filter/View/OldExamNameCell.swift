//
//  OldExamNameCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamNameCell: UITableViewCell {

    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor(hexString: "#333333")
        textField.font = UIFont.systemFont(ofSize: 12)
        textField.borderStyle = .roundedRect
        textField.backgroundColor = UIColor(hexString: "#F4F4F4")
        self.contentView.addSubview(textField)
        return textField
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        textField.snp.updateConstraints { (make) in
            make.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 15, 0, 15))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  OldExamHeaderView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamHeaderView: UITableViewHeaderFooterView {

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#999999")
        label.font = UIFont.systemFont(ofSize: 12)
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let view = UIView()
        view.backgroundColor = UIColor.white
        backgroundView = view
        
        titleLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.centerY.equalTo(self.contentView.snp.centerY)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ text: String) {
        titleLabel.text = text
    }
}

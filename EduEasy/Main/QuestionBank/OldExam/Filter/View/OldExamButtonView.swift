//
//  OldExamButtonView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamButtonView: UIView {
    
    lazy var resetButton: UIButton = {
        let button = self.createButton()
        button.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        button.setBackgroundColor(UIColor(hexString: "#ECECEC")!, forState: .normal)
        button.setTitle("重置", for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonTapHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    lazy var sureButton: UIButton = {
        let button = self.createButton()
        button.setTitleColor(UIColor.white, for: .normal)
        button.setBackgroundColor(UIColor(hexString: "#308AFF")!, forState: .normal)
        button.setTitle("确认", for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonTapHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    var resetButtonClosure: (()->())?
    var sureButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let width: CGFloat = 115
        let height: CGFloat = 40
        let space: CGFloat = 10
        resetButton.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self.snp.centerX).offset(-space)
            make.width.equalTo(width)
            make.height.equalTo(height)
        }
        sureButton.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.centerX).offset(space)
            make.width.equalTo(width)
            make.height.equalTo(height)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func buttonTapHandler(_ sender: UIButton) {
        if sender.tag == 0 {
            resetButtonClosure?()
        }else {
            sureButtonClosure?()
        }
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        return button
    }
}

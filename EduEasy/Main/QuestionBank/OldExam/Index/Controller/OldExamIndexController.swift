//
//  OldExamIndexController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let styleCellReuseIdentifier = "OldExamIndexPaperStyleCell"
private let headerList = ["高起专", "专升本", "高升本"]

class OldExamIndexController: BeanController {
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = UIColor(hexString: "#ECECEC")
        tableView.tableFooterView = UIView()
        let headerView = UIView(frame: CGRect(x: 0, y: 0, w: self.view.bounds.size.width, h: 10))
        tableView.tableHeaderView = headerView
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        return tableView
    }()
    
    var examType: ExamType!
    var yearType: YearType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewRegister()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tableView.frame = self.view.bounds
    }
    
    func tableViewRegister() {
        tableView.register(OldExamIndexPaperStyleCell.self, forCellReuseIdentifier: styleCellReuseIdentifier)
    }
    
    func buttonSelectedHandler(index: Int, questionCatalogId: String) {
        let controller = OldExamController()
        controller.title = self.title
        controller.examType = examType
        controller.yearType = yearType
        controller.questionCatalogId = questionCatalogId
        if index == 0 {
            controller.pagerType = "2"
        }else if index == 1 {
            controller.pagerType = "3"
        }else {
            controller.pagerType = "1"
        }
        self.pushVC(controller)
    }
}

extension OldExamIndexController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: styleCellReuseIdentifier, for: indexPath) as! OldExamIndexPaperStyleCell
        cell.setIndex(indexPath.section)
        cell.callback = {[weak self] id in
            self?.buttonSelectedHandler(index: indexPath.section, questionCatalogId: id)
        }
        return cell
     }
}

extension OldExamIndexController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "header"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? OldExamIndexHeaderView
        if view == nil {
            view = OldExamIndexHeaderView(reuseIdentifier: headerIdentifier)
        }
        view?.setText(headerList[section])
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = "footer"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: footer)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: footer)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        }
        if indexPath.section == 1 {
            return 180
        }
        return 120
    }
}

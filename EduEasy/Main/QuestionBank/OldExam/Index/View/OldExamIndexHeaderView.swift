//
//  OldExamIndexHeaderView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamIndexHeaderView: UITableViewHeaderFooterView {

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15)
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let view = UIView()
        view.backgroundColor = UIColor.white
        backgroundView = view
        
        titleLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.centerY.equalTo(self.contentView.snp.centerY)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ text: String) {
        titleLabel.text = text
    }
}

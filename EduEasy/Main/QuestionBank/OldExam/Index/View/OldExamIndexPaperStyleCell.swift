//
//  OldExamIndexPaperStyleCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/20.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class OldExamIndexPaperStyleCell: UITableViewCell {

    var callback: ((String)->())?
    
    override func draw(_ rect: CGRect) {
        self.selectionStyle = .none
        self.setHiddenSeparatorView()
        self.contentView.backgroundColor = UIColor.white
    }
    
    func setIndex(_ index: Int) {
        let titleList = [["语文", "数学", "英语"],
                    ["英语", "政治", "医学综合", "生态学基础", "教育学基础", "法学基础","大学语文","高数一","高数二"],
                    ["数学", "语文", "外语", "物化(理科)", "文史(文科)"]]
        
        let idList = [["1", "2", "3"],
                      ["3", "4", "6", "7", "8", "9","15","14","13"],
                      ["2", "1", "10", "11", "12"]]
        
        let indexList = [3, 4, 4]
        set(titleList: titleList[index], idList: idList[index], index: indexList[index])
    }
    
    // index 表示一排放几个
    func set(titleList: [String], idList: [String], index: Int) {
        let leftMargin: CGFloat = 15
        let space: CGFloat = 12
        let width: CGFloat = 75
        let height: CGFloat = 50
        for (idx, title) in titleList.enumerated() {
            let button = createButton()
            button.id = idList[idx]
            button.setTitle(title, for: .normal)
            button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
            button.frame = CGRect(x: leftMargin + (width + space) * CGFloat(Int(idx % index)),
                                  y: (height + space) * CGFloat(Int(idx / index)),
                                  w: width,
                                  h: height)
            self.contentView.addSubview(button)
        }
    }
    
    @objc func buttonHandler(_ sender: OldExamPaperButton) {
        callback?(sender.id)
    }
    
    func createButton() -> OldExamPaperButton {
        let button = OldExamPaperButton(type: .custom)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
        button.setTitleColor(UIColor(hexString: "#308AFF"), for: .selected)
        button.setBackgroundColor(UIColor(hexString: "#F4F4F4")!, forState: .normal)
        button.setBackgroundColor(UIColor(hexString: "#BAD8FF")!, forState: .selected)
        return button
    }
}

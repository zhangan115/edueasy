//
//  HeaderTitleTableViewCell.swift
//  EduEasy
//
//  Created by sitech on 2019/4/19.
//  Copyright © 2019 piggybear. All rights reserved.
//

import UIKit

class HeaderTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setModel(){
        self.buttonView.layer.cornerRadius = 8
        self.button1.layout(imagePosition: .top, titleSpace: 20)
        self.button2.layout(imagePosition: .top, titleSpace: 20)
        self.button3.layout(imagePosition: .top, titleSpace: 20)
    }
    
    @IBAction func button1Click(_ sender :UIButton){
        let controller = RecordController()
        controller.title = sender.title(for: .normal)
        controller.requestType = 0
        self.currentViewController.pushVC(controller)
    }
    
    @IBAction func button2Click(_ sender :UIButton){
        let controller = RecordController()
         controller.title = sender.title(for: .normal)
        controller.requestType = 1
        self.currentViewController.pushVC(controller)
    }
    
    @IBAction func button3Click(_ sender :UIButton){
        let controller = FavoriteController()
        self.currentViewController.pushVC(controller)
    }
    
}

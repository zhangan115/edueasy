//
//  QuestionBankCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class QuestionBankCell: UITableViewCell {
    
    lazy var customView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = UIColor.white
        self.contentView.addSubview(view)
        return view
    }()
    
    lazy var titleImageView: UIImageView = {
        let imageView = UIImageView()
        self.customView.addSubview(imageView)
        return imageView
    }()
    
    lazy var oldExamButton: UIButton = {
        let button = self.createButton()
        button.tag = 1
        self.customView.addSubview(button)
        button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var simulationExamButton: UIButton = {
        let button = self.createButton()
        button.tag = 0
        self.customView.addSubview(button)
        button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
        return button
    }()
    
    var _index: Int = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        customView.snp.updateConstraints({ (make) in
            make.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 15, 0, 15))
        })
        
        titleImageView.snp.updateConstraints { (make) in
            make.left.equalTo(self.customView.snp.left)
            make.top.equalTo(self.customView.snp.top).offset(12)
        }
        oldExamButton.snp.updateConstraints { (make) in
            make.left.equalTo(self.customView.snp.left).offset(16)
            make.top.equalTo(self.titleImageView.snp.bottom).offset(12)
            make.bottom.equalTo(self.customView.snp.bottom).offset(-12)
            make.width.equalTo(145)
            make.height.equalTo(94)
        }
        simulationExamButton.snp.updateConstraints { (make) in
            make.right.equalTo(self.customView.snp.right).offset(-16)
            make.centerY.equalTo(self.oldExamButton.snp.centerY)
            make.width.equalTo(self.oldExamButton.snp.width)
            make.height.equalTo(self.oldExamButton.snp.height)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIndex(_ index: Int) {
        _index = index
        let titleImageNames = ["icon_title_college", "icon_title_diploma", "icon_title_strategy"]
        let oldExamImageNames = ["icon_college_img", "icon_english_img", "icon_computer_img"]
        let simulationExamImageNames = ["icon_college_img2", "icon_english_img2", "icon_computer_img2"]
        if index < titleImageNames.count {
            titleImageView.image = UIImage(named: titleImageNames[index])
            oldExamButton.setBackgroundImage(UIImage(named: oldExamImageNames[index]), for: .normal)
            simulationExamButton.setBackgroundImage(UIImage(named: simulationExamImageNames[index]), for: .normal)
        }
    }
    
    @objc func buttonHandler(_ sender: UIButton) {
        let user = UserModel.unarchiver()
        let isVip: Bool = user?.isVip ?? false
        guard isVip else {
            alertController()
            return
        }
        var title = "模拟试题"
        if sender.tag == 1 {
            title = "历年真题"
        }
        let controller = OldExamIndexController()
        controller.title = title
        controller.examType = ExamType(rawValue: _index + 1)
        controller.yearType = YearType(rawValue: sender.tag)
        self.currentViewController.pushVC(controller)
    }
    
    func alertController() {
        let controller = UIAlertController(title: "提示", message: "马上充值成为会员，信息浏览将畅通无阻", preferredStyle: .alert)
        let ok = UIAlertAction(title: "马上充值", style: .destructive) {_ in
            let controller = VipController()
            self.currentViewController.pushVC(controller)
        }
        let cancel = UIAlertAction(title: "暂不充值", style: .default) {_ in
            
        }
        controller.addAction(cancel)
        controller.addAction(ok)
        self.currentViewController.present(controller, animated: true, completion: nil)
    }
    
    override func draw(_ rect: CGRect) {
        self.setHiddenSeparatorView()
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        return button
    }
}

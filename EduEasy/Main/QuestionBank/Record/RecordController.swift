//
//  RecordController.swift
//  EduEasy
//
//  Created by sitech on 2019/4/24.
//  Copyright © 2019 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class RecordController: BeanTableController {
    
    let cellReuseIdentifier = "FavoriteCell"
    
    let disposeBag = DisposeBag()
    let viewModel = FavoriteViewModel()
    
    var requestType:Int! = 0 //记录 或者 错题
    var responseList: [ExamModel]! = []
    var lastId: Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableViewHeader()
        tableViewRegister()
//        setTableViewFooter()
    }
    
    func tableViewRegister() {
        tableView.register(FavoriteCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func setTableViewHeader() {
        let refreshHeader = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_header = refreshHeader
        refreshHeader?.beginRefreshing()
    }
    
    func setTableViewFooter() {
        let footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(loardMoreHandler))
        tableView.mj_footer = footer
        tableView.mj_footer.isHidden = true
    }
    
    @objc func refreshHandler() {
        if self.tableView.mj_header.isRefreshing {
            self.tableView.mj_footer.resetNoMoreData()
            self.lastId = nil
        }
        request()
    }
    
    @objc func loardMoreHandler(){
        request()
    }
    
    @objc func request() {
        self.lastId = nil
        let user: UserModel = UserModel.unarchiver()!
        viewModel.requestRecord(accountId: user.id,bResult: self.requestType.toString, lastId: lastId).subscribe(onSuccess: {[weak self] (list) in
            if (self?.lastId == nil){
                self?.responseList.removeAll()
            }
            self?.requestSuccessMonitor(list)
        }) {[weak self] _ in
            self?.requestSuccessMonitor([])
        }.disposed(by: disposeBag)
    }
    
    
    func requestSuccessMonitor(_ list:[ExamModel]) {
        self.tableView.mj_header.endRefreshing()
//        self.tableView.mj_footer.endRefreshing()
//        if list.count == 0 {
//            self.tableView.mj_footer.endRefreshingWithNoMoreData()
//        }
//        if list.count < 10 {
//            tableView.mj_footer.isHidden = true
//        }else {
//            tableView.mj_footer.isHidden = false
//        }
        self.responseList.append(contentsOf: list)
        self.tableView.safeReloadData()
    }
    
}


extension RecordController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = responseList {
            return list.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FavoriteCell
        let model = responseList[indexPath.row]
        cell.setContentText(model.question)
        return cell
    }
}

extension RecordController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = ExamController()
        controller.customTitle = "题目"
        controller.isUserEnabled = false
        controller.responseList = responseList
        controller.model = responseList[indexPath.row]
        controller.isAnswer = true
        self.pushVC(controller)
    }
}

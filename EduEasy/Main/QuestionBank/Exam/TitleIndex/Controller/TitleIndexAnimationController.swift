//
//  TitleIndexAnimationController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/24.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class TitleIndexAnimationController: TitleIndexController {

    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0)
        self.view.addSubview(view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissLogic))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    let offsetY: CGFloat = 200
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.modalPresentationStyle = .custom
        view.sendSubview(toBack: overlayView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        overlayView.frame = self.view.bounds
        var collectionViewFrame = self.collectionView.bounds
        collectionViewFrame.origin.y = self.view.bounds.size.height
        collectionViewFrame.size.height -= offsetY
        self.collectionView.frame = collectionViewFrame
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
        var containViewFrame = collectionView.frame
        containViewFrame.origin.y = offsetY
        UIView.animate(withDuration: 0.3) {
            self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            self.collectionView.frame = containViewFrame
        }
    }
    
    @objc func dismissLogic() {
        var containViewFrame = collectionView.frame
        containViewFrame.origin.y = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionView.frame = containViewFrame
            self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0)
        }) { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

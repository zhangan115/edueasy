//
//  TitleIndexController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/24.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let reuseIdentifier = "TitleIndexCell"
private let headerViewReuseIdentifier = "TitleIndexHeaderView"

class TitleIndexController: BaseViewController {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 50, height: 50)
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10)
        layout.footerReferenceSize = CGSize(width: self.view.bounds.size.width, height: 20)
        let view = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        view.backgroundColor = UIColor.white
        view.dataSource = self
        view.delegate = self
        view.showsVerticalScrollIndicator = false
        self.view.addSubview(view)
        return view
    }()
    
    var _responseList: [ExamModel]!
    var list: [[ExamModel]] = []
    var lastSection: Int = 0
    var didSelectItemCallback: ((Int)->())?
    var isAnswer: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonLogic()
        self.view.backgroundColor = UIColor.clear
        
        collectionView.register(TitleIndexCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.register(TitleIndexHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerViewReuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.frame = self.view.bounds
    }
    
    func setResponseList(_ responseList: [ExamModel]) {
        _responseList = responseList
        let filteredList = responseList.filterDuplicates{$0.paperSectionId}
        let sectionList = filteredList.map{$0.paperSectionId ?? 0}
        for item in sectionList {
            list.append(titleList(from: item))
        }
    }
    
    func titleList(from paperSectionId: Int) -> [ExamModel] {
        return _responseList.filter{model in
            return model.paperSectionId == paperSectionId
        }
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        let model = self.list[indexPath.section][indexPath.row]
        let index = _responseList.index{$0 == model}
        didSelectItemCallback?(index!)
    }
    
    func backButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(pop))
        button.setImage(UIImage(named: "icon_btn_arrow_l_r"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func pop() {
        if isAnswer {
            let controller = self.navigationController?.viewControllers[1]
            self.navigationController?.popToViewController(controller!, animated: true)
        }else {
         self.navigationController?.popViewController(animated: true)
        }
    }
}

extension TitleIndexController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TitleIndexCell
        lastSection = indexPath.section
        cell.setIndex(indexPath.row + 1)
        let model = list[indexPath.section][indexPath.item]
        cell.setButton(answerType: model.answerType)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerViewReuseIdentifier, for: indexPath) as! TitleIndexHeaderView
        headerView.setModel(list[indexPath.section].first)
        return headerView
    }
}

extension TitleIndexController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: false) {[weak self] in
                self?.didSelectItem(at: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let model = list[section].first {
            let lines = model.paperSectionTitle.numberOfLines(font: UIFont.systemFont(ofSize: 14),
                                                  width: self.view.bounds.size.width)
            if lines == 1 {
                return CGSize(width: self.view.bounds.size.width, height: 40)
            }
            return CGSize(width: self.view.bounds.size.width, height: 30 * lines)
        }
        return CGSize(width: self.view.bounds.size.width, height: 40)
    }
}

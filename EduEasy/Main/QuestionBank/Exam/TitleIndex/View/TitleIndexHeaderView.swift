//
//  TitleIndexHeaderView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/24.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class TitleIndexHeaderView: UICollectionReusableView {
    
    lazy var signLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        self.addSubview(label)
        return label
    }()
    
    let bgColor = UIColor(hexString: "#ECECEC")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = bgColor
        signLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(15)
            make.right.equalTo(self.snp.right).offset(-5)
            make.top.equalTo(self.snp.top).offset(10)
            make.bottom.equalTo(self.snp.bottom).offset(-15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setModel(_ model: ExamModel?) {
        signLabel.text = model?.paperSectionTitle
    }
}

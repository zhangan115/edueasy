//
//  TitleIndexCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/24.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let buttonWidth: CGFloat = 49

class TitleIndexCell: UICollectionViewCell {    
    lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.layer.cornerRadius = buttonWidth / 2
        button.layer.borderWidth = 1
        button.clipsToBounds = true
        button.isUserInteractionEnabled = false
        self.contentView.addSubview(button)
        return button
    }()
    
    var buttonTapClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        button.frame = CGRect(x: (self.bounds.size.width - buttonWidth) / 2,
                               y: (self.bounds.size.height - buttonWidth) / 2 + 10,
                               w: buttonWidth,
                               h: buttonWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIndex(_ index: Int) {
        button.setTitle("\(index)", for: .normal)
    }
    
    func setButton(answerType: AnswerType) {
        switch answerType {
        case .empty:
            button.setBackgroundColor(UIColor.white, forState: .normal)
            button.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
            button.layer.borderColor = UIColor(hexString: "#9F9F9F")?.cgColor
        case .right:
            button.setBackgroundColor(UIColor(hexString: "#BAD7FF")!, forState: .normal)
            button.setTitleColor(UIColor(hexString: "#3179D6"), for: .normal)
            button.layer.borderColor = UIColor(hexString: "#3179D6")?.cgColor
        case .error:
            button.setBackgroundColor(UIColor(hexString: "#FDD5D4")!, forState: .normal)
            button.setTitleColor(UIColor(hexString: "#FA5B5B"), for: .normal)
            button.layer.borderColor = UIColor(hexString: "#FA5B5B")?.cgColor
        }
    }
}

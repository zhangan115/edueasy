//
//  AnswerStatisticsPageController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let space: CGFloat = 10
private let bottomViewHeight: CGFloat = 50

class AnswerStatisticsPageController: BeanController {
    lazy var bottomView: ExamBottomView = {
        let view = ExamBottomView()
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(bottomViewTapHandler))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    var responseList: [ExamModel]!
    var pageContentView: SGPageContentView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "答案统计"
    }
    
    func set(list: [ExamModel], index: Int) {
        responseList = list
        setup()
        pageContentView.setPageCententViewCurrentIndex(index)
    }
    
    func setup() {
        if responseList.count < 0 {
            return
        }
        var controllers: [ExamController] = []
        var index = 0
        var lastItem: ExamModel = responseList.first!
        for value in responseList {
            if value.flag != lastItem.flag {
                index = 0
            }
            index += 1
            
            let controller = ExamController()
            controller.isAnswer = true
            controller.responseList = responseList
            controller.model = value
            controller.index = index
            controllers.append(controller)
            
            lastItem = value
        }
        let frame = CGRect(x: 0,
                           y: 0,
                           w: self.view.bounds.size.width,
                           h: self.view.bounds.size.height - space - bottomViewHeight)
        pageContentView = SGPageContentView(frame: frame, parentVC: self, childVCs: controllers)
        pageContentView.backgroundColor = UIColor(hexString: "#24247F")
        view.addSubview(pageContentView)
        setupSubviewsFrame()
    }
    
    @objc func bottomViewTapHandler() {
        let controller = TitleIndexAnimationController()
        controller.setResponseList(responseList)
        self.present(controller, animated: false, completion: nil)
        controller.didSelectItemCallback = {[weak self] index in
            self?.pageContentView.setPageCententViewCurrentIndex(index)
        }
    }
    
    func setupSubviewsFrame() {
        bottomView.frame = CGRect(x: 0,
                                  y: pageContentView.frame.maxY + space,
                                  w: self.view.bounds.size.width,
                                  h: bottomViewHeight)
    }

}

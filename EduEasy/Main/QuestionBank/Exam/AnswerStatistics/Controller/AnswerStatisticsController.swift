//
//  AnswerStatisticsController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/25.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class AnswerStatisticsController: TitleIndexController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "答案统计"
        
        isAnswer = true
        
        let list = _responseList
        self.didSelectItemCallback = {[weak self] index in
            let controller = AnswerStatisticsPageController()
            controller.set(list: list!, index: index)
             self?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       self.didSelectItem(at: indexPath)
    }
}

//
//	ExamModel.swift
//
//	Create by piggybear on 27/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ExamModel : NSObject, Mappable{
    
    //自己作答的答案
    var ownAnswers: [String] {
        var list: [String] = []
        let type = TitleType(rawValue: flag)!
        for item in options {
            if item.isSelected {
                list.append(item.optVal)
            }else if type == .blank && item.answer != nil && item.answer.count != 0 {
                list.append(item.answer)
            }
        }
        return list
    }
    /*
     正确答案
     1、如果是选择题或判断题，则存放的是A、B、C等选项而不是正确答案的内容
     2、如果是填空题，则存放的是正确答案的内容
     */
    var rightAnswers: [String]!
    var answers : [ExamAnswer]!
    var flag : Int!
    var id : Int!
    var options : [ExamOptions]!
    var pagerId : Int!
    var paperSectionId : Int!
    var paperSectionTitle : String!
    var question : String!
    var questionCode : String!
    var score : Int!
    var isCollect: Bool!
    var answerType: AnswerType {
        if isEmpty {
            return .empty
        }
        if hasError {
            return .error
        }
        return .right
    }
    
    /*
     是否未作答
     只要答了一个就不算未作答
     */
    private var isEmpty: Bool {
        let type = TitleType(rawValue: flag)!
        for item in options {
            if type == .blank {
                if item.answer != nil && item.answer.count != 0 { //只要答了一个就不算空
                    return false
                }
            }
            if item.isSelected {//只要答了一个就不算空
                return false
            }
        }
        return true
    }
    
    /*
     1、首先判断是否全部作答，如果有一个答案没有作答也算错误
     2、如果答案中有一个回答错误也算答错
     */
    private var hasError: Bool {
        let type = TitleType(rawValue: flag)!
        var answers: [String] = []
        for (idx, item) in options.enumerated() {
            if type == .blank {
                if item.answer != nil && item.answer.count != 0 { //已作答
                    answers.append(item.answer)
                }
            }else if item.isSelected {
                answers.append(idx.toString)
            }
        }
        if answers.count != rightAnswers.count { //答案没有全部作答也算错误
            return true
        }
        
        for (idx, right) in rightAnswers.enumerated() {
            let answer = answers[idx]
            if right != answer { //只要有一个答案回答错就算错
                return true
            }
        }
        
        return false
    }
    
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let answer = json["answer"].stringValue
        let answerJson = try! JSON(data: answer.data(using: .utf8)!)
        var answerList: [ExamAnswer] = []
        for item in answerJson.arrayValue {
            let answer = ExamAnswer(fromJson: item)
            answerList.append(answer)
        }
        answers = answerList
        
        flag = json["flag"].intValue
        id = json["id"].intValue
        let string = json["options"].stringValue
        let opsJson = try! JSON(data: string.data(using: .utf8)!)
        var list: [ExamOptions] = []
        var rightAnswerList: [String] = []
        let type = TitleType(rawValue: flag) ?? .blank
        for (idx, item) in opsJson.arrayValue.enumerated() {
            let ops = ExamOptions(fromJson: item)
            if ops.optSta {
                var answer: String = idx.toString
                if type == .blank {
                    answer = ops.optVal
                }
                rightAnswerList.append(answer)
            }
            rightAnswers = rightAnswerList
            list.append(ops)
        }
        
        options = list
        pagerId = json["pagerId"].intValue
        paperSectionId = json["paperSectionId"].intValue
        paperSectionTitle = json["paperSectionTitle"].stringValue
        question = json["question"].stringValue
        questionCode = json["questionCode"].stringValue
        score = json["score"].intValue
        isCollect = json["isCollect"].boolValue
    }
}

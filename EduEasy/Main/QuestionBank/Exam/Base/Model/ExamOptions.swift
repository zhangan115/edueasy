//
//  ExamOptions.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/1.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import SwiftyJSON

class ExamOptions {
    
    var optSta : Bool!
    var optVal : String!
    var isSelected: Bool = false
    //回答的答案
    var answer: String!
    
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        optSta = json["optSta"].boolValue
        optVal = json["optVal"].stringValue
    }
}

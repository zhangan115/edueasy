//
//  ExamAnswer.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import SwiftyJSON

class ExamAnswer {
    
    var optSta : Bool!
    var optVal : String!
    
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        optSta = json["optSta"].boolValue
        optVal = json["optVal"].stringValue
    }
}

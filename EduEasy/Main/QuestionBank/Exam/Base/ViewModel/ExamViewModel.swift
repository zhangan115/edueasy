//
//  ExamViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/27.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class ExamViewModel {
    
    var paperId = Variable<Int>(0)
    var result: Driver<[ExamModel]>!
    var responseJson: JSON!
    
    init() {
        result = paperId.asObservable().flatMapLatest { (id) -> Single<[ExamModel]> in
            return questionBankProvider
                .rxRequest(.paperQuestion(id: id))
                .map({ (json) -> JSON in
                    self.responseJson = json["data"]
                    return json
                })
                .toListModel(type: ExamModel.self)
            }.asDriver(onErrorJustReturn: [ExamModel(fromJson: JSON(""))])
    }
}

//
//  ExamContentView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/27.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SwiftyJSON

private let titleCellReuseIdentifier = "ExamTitleCell"
private let contentCellReuseIdentifier = "ExamContentCell"
private let answerCellReuseIdentifier = "ExamAnswerCell"

class ExamContentView: UIView {

    lazy var tableView: ExamTableView = {
        let tableView = ExamTableView(frame: self.bounds, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        self.addSubview(tableView)
        return tableView
    }()
    
    lazy var bottomView: ExamBottomView = {
        let view = ExamBottomView()
        view.backgroundColor = UIColor.white
        self.addSubview(view)
        return view
    }()
    
    lazy var alertView: ExamAlertView = {
        let view = ExamAlertView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 8
        view.isHidden = true
        self.addSubview(view)
        return view
    }()
    
    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.isHidden = true
        self.addSubview(view)
        return view
    }()
    
    let responseList: [ExamModel]
    let model: ExamModel!
    let index: Int
    
    init(frame: CGRect, model: ExamModel, list: [ExamModel], index: Int) {
        self.index = index
        self.model = model
        self.responseList = list
         super.init(frame: frame)
        setupSubviewsFrame()
        tableViewRegister()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableViewRegister() {
        tableView.register(ExamTitleCell.self, forCellReuseIdentifier: titleCellReuseIdentifier)
        tableView.register(ExamContentCell.self, forCellReuseIdentifier: contentCellReuseIdentifier)
        tableView.register(ExamAnswerCell.self, forCellReuseIdentifier: answerCellReuseIdentifier)
    }
    
    func setupSubviewsFrame() {
        let space: CGFloat = 10
        let bottomViewHeight: CGFloat = 50
        tableView.frame = CGRect(x: 0,
                                 y: 0,
                                 w: self.bounds.size.width,
                                 h: self.bounds.size.height - space - bottomViewHeight)
        bottomView.frame = CGRect(x: 0,
                                  y: tableView.frame.maxY + space,
                                  w: self.bounds.size.width,
                                  h: bottomViewHeight)
        
        overlayView.frame = self.bounds
        alertView.snp.updateConstraints({ (make) in
            make.center.equalTo(self.snp.center)
            make.width.equalTo(290)
            make.height.equalTo(215)
        })
    }
    
    func titleList(from titleType: TitleType) -> [ExamModel] {
        return self.responseList.filter{model in
            let type = TitleType(rawValue: model.flag)
            return type == titleType
        }
    }
}

extension ExamContentView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 1 {
            return
        }
        let type = TitleType(rawValue: model.flag)!
        if type == .singleChoice || type == .judgment {
            model.options.setAll { (_, option) in
                option.isSelected = false
            }
        }
        let option = model.options[indexPath.row]
        option.isSelected = !option.isSelected
        tableView.reloadData()
    }
}

extension ExamContentView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if model == nil {
            return 0
        }
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return model.options.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: titleCellReuseIdentifier, for: indexPath) as! ExamTitleCell
            let type = TitleType(rawValue: model.flag)!
            cell.set(model: model, index: index, allCount: titleList(from: type).count)
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: contentCellReuseIdentifier, for: indexPath) as! ExamContentCell
            let type = TitleType(rawValue: model.flag)!
            cell.set(option: model.options[indexPath.row], index: indexPath.row, titleType: type)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: answerCellReuseIdentifier, for: indexPath)
        return cell
    }
}

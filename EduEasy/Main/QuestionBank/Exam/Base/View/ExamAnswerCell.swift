//
//  ExamAnswerCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/22.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit
import EZSwiftExtensions

class ExamAnswerCell: UITableViewCell {

    lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        contentLabel.snp.updateConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(15)
            make.bottom.greaterThanOrEqualTo(self.contentView.snp.bottom).offset(-15)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.right.greaterThanOrEqualTo(self.contentView.snp.right).offset(-15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setModel(_ model: ExamModel) {
        let type = TitleType(rawValue: model.flag)!
        var text = ""
        if type == .blank {
            text = model.rightAnswers.joined(separator: "、")
        }else {
            let indexs: [String] = ["A", "B", "C", "D", "E", "F", "G"]
            if let list = model.rightAnswers {
                let answers = list.map{value -> String in
                    let answer: Int = value.toInt()!
                    return indexs[answer]
                }
                text = answers.joined(separator: "、")
            }
        }
        setContentLabelText(text)
    }
    
    func setContentLabelText(_ text: String) {
        contentLabel.text = "正确答案：" + text
    }
}

//
//  ExamAlertView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/25.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class ExamAlertView: UIView {

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        self.addSubview(imageView)
        return imageView
    }()
    
    lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(label)
        return label
    }()
    
    lazy var cancelButton: UIButton = {
        let button = self.createButton()
        button.setBackgroundColor(UIColor(hexString: "#ECECEC")!, forState: .normal)
        button.setTitle("取消", for: .normal)
        button.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    lazy var completionButton: UIButton = {
        let button = self.createButton()
        button.setBackgroundColor(UIColor(hexString: "#308AFF")!, forState: .normal)
        button.setTitle("完成", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    var cancelButtonClosure: (()->())?
    var completionButtonClosure: (()->())?
    
    var isDidComoletion: Bool = false {
        willSet(newValue){
            if newValue {
                imageView.image = UIImage(named: "img_complete")
                contentLabel.text = "考题全部已作答，点击完成结束考试"
            }else {
                imageView.image = UIImage(named: "img_unfinished")
                contentLabel.text = "您有未作答的试题，详情请点击“题序”查看，若强行完成将结束考试。"
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviewsFrame()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 5
        return button
    }
    
    @objc func buttonHandler(_ sender: UIButton) {
        if sender.tag == 0 {
            cancelButtonClosure?()
        }else {
            completionButtonClosure?()
        }
    }
    
    func setSubviewsFrame() {
        imageView.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(-25)
        }
        contentLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(17)
            make.right.equalTo(self.snp.right).offset(-17)
        }
        cancelButton.snp.updateConstraints { (make) in
            make.top.equalTo(self.contentLabel.snp.bottom).offset(39)
            make.left.equalTo(self.contentLabel.snp.left)
            make.height.equalTo(40)
            make.width.equalTo(114)
        }
        completionButton.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.cancelButton.snp.centerY)
            make.left.equalTo(self.cancelButton.snp.right).offset(16)
            make.width.equalTo(self.cancelButton.snp.width)
            make.height.equalTo(self.cancelButton.snp.height)
        }
    }
}

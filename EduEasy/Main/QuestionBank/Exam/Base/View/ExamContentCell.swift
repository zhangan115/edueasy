//
//  ExamContentCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/22.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class ExamContentCell: UITableViewCell {

    lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor(hexString: "#333333")
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        self.contentView.addSubview(textField)
        return textField
    }()
    
    lazy var titleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isUserInteractionEnabled = false
        button.setBackgroundImage(UIImage(named: "icon_btn_radio3"), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor(hexString: "#666666"), for: .normal)
        self.contentView.addSubview(button)
        return button
    }()
    
    let indexs: [String] = ["A", "B", "C", "D", "E", "F", "G"]
    var option: ExamOptions!
    
    func set(option: ExamOptions, index: Int, titleType: TitleType?) {
        self.option = option
        if titleType == .blank {
            textField.isHidden = false
            contentLabel.isHidden = true
            titleButton.setTitle((index + 1).toString, for: .normal)
        }else {
            textField.isHidden = true
            contentLabel.isHidden = false
            if index < indexs.count {
                titleButton.setTitle(indexs[index], for: .normal)
            }
        }
        contentLabel.text = option.optVal
        selected(option.isSelected)
    }
    
    func selected(_ isSelected: Bool) {
        if isSelected {
             titleButton.setTitle("", for: .normal)
             titleButton.setBackgroundImage(UIImage(named: "icon_btn_choice"), for: .normal)
            contentLabel.textColor = UIColor(hexString: "#308AFF")
        }else {
             titleButton.setBackgroundImage(UIImage(named: "icon_btn_radio3"), for: .normal)
            contentLabel.textColor = UIColor(hexString: "#333333")
        }
    }
    
    @objc func editingChanged(_ sender: UITextField) {
        option.answer = sender.text
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        titleButton.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.top.equalTo(self.contentView.snp.top).offset(10)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        contentLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.titleButton.snp.right).offset(15)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
            make.top.equalTo(self.titleButton.snp.top)
            make.bottom.greaterThanOrEqualTo(self.contentView.snp.bottom).offset(-15)
        }
        textField.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.titleButton.snp.centerY)
            make.left.equalTo(self.titleButton.snp.right).offset(10)
            make.right.equalTo(self.contentView.snp.right).offset(-10)
            make.height.equalTo(35)
            make.bottom.greaterThanOrEqualTo(self.contentView.snp.bottom).offset(-10)
        }
        
        textField.layoutIfNeeded()
        textField.setBorder(.bottom, height: 1, color: UIColor(hexString: "#c6d8e6")!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

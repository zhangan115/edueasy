//
//  ExamBottomView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/21.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class ExamBottomView: UIView {

    lazy var favoriteButton: UIButton = {
        let button = self.createButton()
        button.setTitle("收藏", for: .normal)
        button.setTitle("已收藏", for: .selected)
        button.setImage(UIImage(named: "icon_btn_favorite"), for: .normal)
        button.setImage(UIImage(named: "icon_btn_already_favorite"), for: .selected)
        button.addTarget(self, action: #selector(favoriteButtonHandler(_:)), for: .touchUpInside)
        self.addSubview(button)
        return button
    }()
    
    lazy var titleIndexButton: UIButton = {
        let button = self.createButton()
        button.isUserInteractionEnabled = false
        button.setImage(UIImage(named: "icon_btn_all"), for: .normal)
        self.addSubview(button)
        return button
    }()
    
    var model: ExamModel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        favoriteButton.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(15)
        }
        titleIndexButton.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self.snp.right).offset(-15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func favoriteButtonHandler(_ sender: UIButton) {
        if model.isCollect {
            return
        }
        let user = UserModel.unarchiver()!
        questionBankProvider.requestResult(.favorite(paperQuestionId: model.id, accountId: user.id)) { (json) in
        }
        model.isCollect = true
        sender.isSelected = true
        favoriteButton.isUserInteractionEnabled = false
    }
    
    func createButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor(hexString: "#333333"), for: .normal)
        return button
    }
    
    func setIndexTitle(_ title: String) {
        titleIndexButton.setTitle(title, for: .normal)
        favoriteButton.isSelected = model.isCollect
        if model.isCollect {
            favoriteButton.isUserInteractionEnabled = false
        }else {
            favoriteButton.isUserInteractionEnabled = true
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        favoriteButton.layout(imagePosition: .left, titleSpace: 8)
        titleIndexButton.layout(imagePosition: .left, titleSpace: 8)
    }
}

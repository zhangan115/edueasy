//
//  ExamTitleCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/22.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class ExamTitleCell: UITableViewCell {
    
    lazy var signLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#999999")
        label.font = UIFont.systemFont(ofSize: 12)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var titleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isUserInteractionEnabled = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitleColor(UIColor(hexString: "#308AFF"), for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(hexString: "#308AFF")?.cgColor
        self.contentView.addSubview(button)
        return button
    }()
    
    lazy var titleLabel: UILabel = {
        let label = self.createLabel()
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var webView: UIWebView = {
        let view = UIWebView()
        view.delegate = self
        view.scalesPageToFit = true
        self.contentView.addSubview(view)
        return view
    }()
    
    var webViewHeightConstraint: Constraint? = nil
    var isFinishLoad: Bool = false

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        signLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.top.equalTo(self.contentView.snp.top).offset(9)
            make.height.equalTo(20)
        }
        
        titleButton.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.top.equalTo(self.signLabel.snp.bottom).offset(22)
            make.width.equalTo(45)
            make.height.equalTo(18)
        }
        
        titleLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.titleButton.snp.right).offset(5)
            make.top.equalTo(self.titleButton.snp.top)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
//            self.webViewHeightConstraint = make.height.equalTo(30).constraint
             make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(model: ExamModel, index: Int!, allCount: Int) {
        let score: Int =  model.score
        let allScore: Int = score * allCount
        if let index = index {
            setSignLabelText("\(index) / \(allCount)  每小题\(score)分，共\(allScore)分")
        }
        let type = TitleType(rawValue: model.flag)
        titleButton.setTitle(type?.toText(), for: .normal)
        titleLabel.attributedText = attributedText(text: styledHTML(model.question))
//        webView.loadHTMLString(styledHTML(model.question), baseURL: nil)
        print("=======", styledHTML(model.question))
    }
    
    func attributedText(text: String) -> NSAttributedString {
        let data = text.data(using: String.Encoding.unicode, allowLossyConversion: true)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attrString = try! NSAttributedString(data: data!, options: options, documentAttributes: nil)
        return attrString
    }
    
    func setSignLabelText(_ text: String) {
        let attributedText =  NSMutableAttributedString(string: text)
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor: UIColor(hexString: "#308AFF")!], range: NSMakeRange(0, 1))
        signLabel.attributedText = attributedText
    }
    
    func styledHTML(_ html: String) -> String {
//        let style: String = "<head><meta charset=\"UTF-8\"><style> body {font-size:16px; color: #333333;}</style></head>"
        let newHtml = html.replacingOccurrences(of: "font-size", with: "")
         let style: String = "<head><meta charset=\"UTF-8\"><style> span{ font-size:16px} body {font-size:16px; color: #333333;} img{margin: 0 auto; max-width: \(UIScreen.main.bounds.size.width - 80)px;align=\"middle\";}</style></head>"
        return style + newHtml
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        return label
    }
}

extension ExamTitleCell: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
//        if isFinishLoad {
//            return
//        }
//        isFinishLoad = true
//        let string = "document.body.style.fontSize=15;document.body.style.color=#333333"
//        webView.stringByEvaluatingJavaScript(from: string)
        let height = webView.stringByEvaluatingJavaScript(from: "document.body.offsetHeight")?.toFloat()
        self.webViewHeightConstraint?.update(offset: height ?? 0)
    }
}

//
//  ExamTableView.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/27.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class ExamTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.estimatedRowHeight = 44
        self.rowHeight = UITableViewAutomaticDimension
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, w: 0, h: 0.1))
        self.backgroundColor = UIColor.white
        self.showsVerticalScrollIndicator = false
        self.separatorStyle = .none
        self.delegate = self
        self.keyboardDismissMode = .onDrag
    }
}

extension ExamTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "Header"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = backgroundColor
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerIdentifier = "Footer"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

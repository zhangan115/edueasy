//
//  ExamScrollController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/31.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let titleCellReuseIdentifier = "ExamTitleCell"
private let contentCellReuseIdentifier = "ExamContentCell"
private let answerCellReuseIdentifier = "ExamAnswerCell"

class ExamController: BeanController {
    
    lazy var tableView: ExamTableView = {
        let tableView = ExamTableView(frame: self.view.bounds, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 44
        self.view.addSubview(tableView)
        return tableView
    }()
        
    var responseList: [ExamModel]!
    var model: ExamModel!
    var index: Int!
    var isAnswer: Bool = false
    var customTitle: String!
    var isUserEnabled: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let title = customTitle {
            self.title = title
        }else {
            self.title = "考试"
        }
        tableViewRegister()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tableView.frame = self.view.bounds
//        tableView.isUserInteractionEnabled = isUserEnabled
    }
    
    func tableViewRegister() {
        tableView.register(ExamTitleCell.self, forCellReuseIdentifier: titleCellReuseIdentifier)
        tableView.register(ExamContentCell.self, forCellReuseIdentifier: contentCellReuseIdentifier)
        tableView.register(ExamAnswerCell.self, forCellReuseIdentifier: answerCellReuseIdentifier)
    }
    
    func titleList(from titleType: TitleType?) -> [ExamModel] {
        return self.responseList.filter{model in
            let type = TitleType(rawValue: model.flag)
            return type == titleType
        }
    }
}

extension ExamController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard isUserEnabled else {
            return
        }
        if indexPath.section != 1 {
            return
        }
        let type = TitleType(rawValue: model.flag)
        if type == .singleChoice || type == .judgment {
            model.options.setAll { (_, option) in
                option.isSelected = false
            }
        }
        let option = model.options[indexPath.row]
        option.isSelected = !option.isSelected
        tableView.reloadData()
    }
}

extension ExamController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if model == nil {
            return 0
        }
        if isAnswer {
            return 3
        }
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return model.options.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: titleCellReuseIdentifier, for: indexPath) as! ExamTitleCell
            let type = TitleType(rawValue: model.flag)
            cell.set(model: model, index: index, allCount: titleList(from: type).count)
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: contentCellReuseIdentifier, for: indexPath) as! ExamContentCell
            let type = TitleType(rawValue: model.flag)
            cell.set(option: model.options[indexPath.row], index: indexPath.row, titleType: type)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: answerCellReuseIdentifier, for: indexPath) as! ExamAnswerCell
        cell.setModel(model)
        return cell
    }
}

//
//  ExamController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/21.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import SwiftyJSON

class ExamPageController: BeanController {
    lazy var alertView: ExamAlertView = {
        let view = ExamAlertView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 8
        view.isHidden = true
        self.view.addSubview(view)
        return view
    }()
    
    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.isHidden = true
        self.view.addSubview(view)
        return view
    }()
    
    lazy var bottomView: ExamBottomView = {
        let view = ExamBottomView()
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(bottomViewTapHandler))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    var rightButton: UIButton!
    var pageContentView: SGPageContentView!
    var paperId: Int = 0
    var responseList: [ExamModel]!
    
    let viewModel = ExamViewModel()
    let disposeBag = DisposeBag()
    
    let space: CGFloat = 10
    let bottomViewHeight: CGFloat = 50
    
    //是否作答了题目，如果作答了一道题也算作答
    var isAnswered: Bool {
        for item in responseList {
            if item.answerType != .empty {
                return true
            }
        }
        return false
    }
    
    //是否把题全部作答了，如果有一道题没有作答则也不算全部作答
    var isAllAnswered: Bool {
        for item in responseList {
            if item.answerType == .empty {
                return false
            }
        }
        return true
    }
    
    var responseJson: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "考试"
        
        viewModel.paperId.value = paperId
        viewModel.result.asObservable().subscribe(onNext: {[weak self] list in
            guard let `self` = self else {
                return
            }
            self.responseList = list
            self.responseJson = self.viewModel.responseJson
            self.setup()
        }).disposed(by: disposeBag)
        
        rightButtonLogic()
    }
    
    func setup() {
        if responseList.count <= 0 {
            rightButton.isHidden = true
            return
        }
        var controllers: [ExamController] = []
        var index = 0
        var lastItem: ExamModel = responseList.first!
        for value in responseList {
            if value.flag != lastItem.flag {
                index = 0
            }
            index += 1
            
            let controller = ExamController()
            controller.responseList = responseList
            controller.model = value
            controller.index = index
            controllers.append(controller)
            
            lastItem = value
        }
        let frame = CGRect(x: 0,
                           y: 0,
                           w: self.view.bounds.size.width,
                           h: self.view.bounds.size.height - space - bottomViewHeight)
        pageContentView = SGPageContentView(frame: frame, parentVC: self, childVCs: controllers)
        pageContentView.delegatePageContentView = self
        pageContentView.backgroundColor = UIColor(hexString: "#24247F")
        view.addSubview(pageContentView)
        setupSubviewsFrame()
        self.setIndex(1)
    }

    func rightButtonLogic() {
        rightButton = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(rightButtonHandler))
        rightButton.setTitle("交卷", for: .normal)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        rightButton.setTitleColor(UIColor.white, for: .normal)
        rightButton.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }

    @objc func rightButtonHandler() {
        setHiddenAlertView(false)
        alertView.isDidComoletion = isAllAnswered
        alertView.cancelButtonClosure = {[weak self] in
            self?.setHiddenAlertView(true)
        }
        alertView.completionButtonClosure = {[weak self] in
            self?.completionButtonHandler()
        }
    }
    
    func completionButtonHandler() {
        setHiddenAlertView(true)
        if isAnswered {
            upload()
        }else {
            pushToAnswerStatisticsController()
        }
    }
    
    func pushToAnswerStatisticsController() {
        let controller = AnswerStatisticsController()
        controller.setResponseList(responseList)
        self.pushVC(controller)
    }
    
    func upload() {
        var list: [[String: Any]] = []
        for (idx, item) in responseList.enumerated() {
            if item.answerType == .empty {
                continue
            }
            var json = responseJson.arrayValue[idx]
            var answers: [[String: String]] = []
            for answer in item.ownAnswers {
                let dic = ["optSta": "true", "optVal": answer]
                answers.append(dic)
            }
            let stdAnswer = json["answer"]
            json["stdAnswer"] = stdAnswer
            json["answer"].arrayObject = answers
            if item.answerType == .right {
                json["bResult"] = true
            }else {
                json["bResult"] = false
            }
            list.append(json.dictionaryObject!)
        }
        let accountId = UserModel.unarchiver()?.id
        self.view.showHUD(message: "正在提交...")
        questionBankProvider.requestResult(.upload(accountId: accountId!, data: list.toJson()), success: {[weak self] (json) in
            self?.view.showHUD("提交成功", completion: {
                self?.view.hiddenHUD()
                self?.pushToAnswerStatisticsController()
            })
        }) {[weak self] _ in
            self?.view.hiddenHUD()
        }
    }

    func setHiddenAlertView(_ isHidden: Bool) {
        overlayView.isHidden = isHidden
        alertView.isHidden = isHidden
    }
    
    @objc func bottomViewTapHandler() {
        let controller = TitleIndexAnimationController()
        controller.setResponseList(responseList)
        self.present(controller, animated: false, completion: nil)
        controller.didSelectItemCallback = {[weak self] index in
            self?.pageContentView.setPageCententViewCurrentIndex(index)
            self?.setIndex(index + 1)
        }
    }
    
    func setupSubviewsFrame() {
        bottomView.frame = CGRect(x: 0,
                                  y: pageContentView.frame.maxY + space,
                                  w: self.view.bounds.size.width,
                                  h: bottomViewHeight)
        
        overlayView.frame = self.view.bounds
        alertView.snp.updateConstraints({ (make) in
            make.center.equalTo(self.view.snp.center)
            make.width.equalTo(290)
            make.height.equalTo(215)
        })
    }
    
    func setIndex(_ index: Int) {
        bottomView.model = responseList[index - 1]
        bottomView.setIndexTitle("\(index)/\(responseList.count)题序")
    }
}

extension ExamPageController: SGPageContentViewDelegate {
    func pageContentView(_ pageContentView: SGPageContentView!, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        setIndex(targetIndex + 1)
    }
}



//
//  LoginTextField.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class LoginTextField: UITextField {
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var leftViewRect = bounds
        leftViewRect.origin.x = 15
        leftViewRect.size.width = 70
        return leftViewRect
    }
    
    func setLeftText(_ text: String) {
        let label = createLabel()
        label.text = text
        self.leftView = label
        self.leftViewMode = .always
    }
    
    func createLabel() ->UILabel {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }
}

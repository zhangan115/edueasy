//
//  LoginController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var usernameTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var checkboxButton: UIButton!
    
    @IBOutlet weak var weiXinButton: UIButton!
    @IBOutlet weak var qqButton: UIButton!
    
    var viewModel: LoginViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel = LoginViewModel(account: usernameTextField.rx.text.orEmpty.asObservable(),
                                   password: passwordTextField.rx.text.orEmpty.asObservable())
        loginButton.rx.tap.asObservable()
            .bind(to: viewModel.loginButton)
            .disposed(by: disposeBag)
         NotificationCenter.default.addObserver(self,selector: #selector(WXLoginSuccess(notification:)),name: NSNotification.Name(rawValue: "WeiXinLogin"),object: nil)
        
        viewModel.result.asObservable().subscribe(onNext: {[weak self] result in
            switch result {
            case .failed(let message):
                self?.view.showAutoHUD(message)
            case .success(let model):
                self?.successLogic(model: model)
            default: break
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func WXLoginSuccess(notification:Notification){
        let code = notification.object as? String
        if let code = code {
            print("code " + code)
            viewModel.weiXinGetCode(code: code).subscribe(onSuccess: {[weak self] (model) in
                self?.weiXinLoginSuccess(model: model)
            }).disposed(by: disposeBag)
        }
    }
    
    func weiXinLoginSuccess(model:WeiXinLogin){
        if model.user != nil {
            UserModel.archiverUser(model.user)
        }else{
            let controller = RegisterController()
            controller.weiXinCode = model.openId
            controller.title = "完善信息"
            self.pushVC(controller)
        }
    }
    
    func successLogic(model: UserModel) {
        UserModel.archiverUser(model)
        setIsLogin(true)
        let controller = PGTabBarController()
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    func setupUI() {
        iconButton.setTitle(BundleConfig.appDisplayName, for: .normal)
        iconButton.layout(imagePosition: .top, titleSpace: 10)
        loginButton.layer.cornerRadius = 5
        contentView.layer.cornerRadius = 5
        usernameTextField.setLeftText("账号")
        passwordTextField.setLeftText("密码")
        self.navBar?.isHidden = true
//        self.weiXinButton.isHidden = !WXApi.isWXAppInstalled()
        
    }
    
    @IBAction func forgetPasswordHandler(_ sender: UIButton) {
        let controller = ForgetPasswordController()
        self.pushVC(controller)
    }
    
    @IBAction func registerHandler(_ sender: UIButton) {
        let controller = RegisterController()
        controller.title = "注册"
        self.pushVC(controller)
    }
    
    @IBAction func contractButtonHandle(_ sender: UIButton) {
        let controller = ContractController()
        self.pushVC(controller)
    }
    
    @IBAction func checkboxHandle(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        viewModel.checkbox.value = sender.isSelected
    }
    
    @IBAction func weiXinLogin(_ sender: UIButton) {
        let req = SendAuthReq()
        req.scope = "snsapi_userinfo,snsapi_friend,snsapi_message,snsapi_contact"
        req.state = "none"
        WXApi.send(req)
    }
    
    @IBAction func qqLogin(_ sender: UIButton) {
        
    }
}

extension LoginController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupClass() ->Bool {
        let controller = self.currentViewController
        if controller.isKind(of: RegisterController.self) ||
            controller.isKind(of: ForgetPasswordController.self) {
            return true
        }
        return false
    }
}

//
//	UserModel.swift
//
//	Create by piggybear on 26/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserModel : NSObject, NSCoding, Mappable{

	var accountName : String!
	var bIntegrated : Bool!
	var headImage : String!
	var id : Int!
	var locked : Bool!
	var newField : Bool!
	var phone : String!
	var pssword : String!
	var regTime : UserRegTime!
	var token : String!
    var integral: String!
    var inviteCode: String!
    var myCardEndTime1: String!
    var myCardEndTime2: String!
    var myCardEndTime3: String!
    var sex : String!
    var userName : String!
    var realName : String!
    var idcard : String!
    
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        idcard = json["idcard"].stringValue
        realName = json["realName"].stringValue
        sex = json["sex"].stringValue
        userName = json["userName"].stringValue
        myCardEndTime1 = json["myCardEndTime1"].stringValue
        myCardEndTime2 = json["myCardEndTime2"].stringValue
        myCardEndTime3 = json["myCardEndTime3"].stringValue
        inviteCode = json["inviteCode"].stringValue
        integral = json["integral"].stringValue
		accountName = json["accountName"].stringValue
		bIntegrated = json["bIntegrated"].boolValue
		headImage = json["headImage"].stringValue
		id = json["id"].intValue
		locked = json["locked"].boolValue
		newField = json["new"].boolValue
		phone = json["phone"].stringValue
		pssword = json["pssword"].stringValue
		let regTimeJson = json["regTime"]
		if !regTimeJson.isEmpty{
			regTime = UserRegTime(fromJson: regTimeJson)
		}
		token = json["token"].stringValue
	}
    
    class func path() -> String {
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        return documentPath! + "/user"
    }
    
    class func archiverUser(_ user: UserModel) {
        NSKeyedArchiver.archiveRootObject(user, toFile: path())
    }
    
    class func unarchiver() ->UserModel? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: path()) as? UserModel
    }
    
    var isVip: Bool {
        return true
        let user = UserModel.unarchiver()
        let card1: String = user?.myCardEndTime1 ?? ""
        let card2: String = user?.myCardEndTime2 ?? ""
        let card3: String = user?.myCardEndTime3 ?? ""
        if card1.count == 0 && card2.count == 0 && card3.count == 0 {
            return false
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date1: TimeInterval = formatter.date(from: card1)?.timeIntervalSince1970 ?? 0
        let date2: TimeInterval = formatter.date(from: card2)?.timeIntervalSince1970 ?? 0
        let date3: TimeInterval = formatter.date(from: card3)?.timeIntervalSince1970 ?? 0
        let currentTime = formatter.string(from: Date())
        let currentDate = formatter.date(from: currentTime)?.timeIntervalSince1970 ?? 0
        if date1 > currentDate || date2 > currentDate || date3 > currentDate {
            return true
        }
        return false
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        idcard = aDecoder.decodeObject(forKey: "idcard") as? String
        realName = aDecoder.decodeObject(forKey: "realName") as? String
        sex = aDecoder.decodeObject(forKey: "sex") as? String
        userName = aDecoder.decodeObject(forKey: "userName") as? String
         accountName = aDecoder.decodeObject(forKey: "accountName") as? String
         bIntegrated = aDecoder.decodeObject(forKey: "bIntegrated") as? Bool
         headImage = aDecoder.decodeObject(forKey: "headImage") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         locked = aDecoder.decodeObject(forKey: "locked") as? Bool
         newField = aDecoder.decodeObject(forKey: "new") as? Bool
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         pssword = aDecoder.decodeObject(forKey: "pssword") as? String
         regTime = aDecoder.decodeObject(forKey: "regTime") as? UserRegTime
         token = aDecoder.decodeObject(forKey: "token") as? String
        integral = aDecoder.decodeObject(forKey: "integral") as? String
        inviteCode = aDecoder.decodeObject(forKey: "inviteCode") as? String
        myCardEndTime1 = aDecoder.decodeObject(forKey: "myCardEndTime1") as? String
        myCardEndTime2 = aDecoder.decodeObject(forKey: "myCardEndTime2") as? String
        myCardEndTime3 = aDecoder.decodeObject(forKey: "myCardEndTime3") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
        if idcard != nil{
            aCoder.encode(idcard, forKey: "idcard")
        }
        if realName != nil{
            aCoder.encode(realName, forKey: "realName")
        }
        if sex != nil{
            aCoder.encode(sex, forKey: "sex")
        }
        if userName != nil{
            aCoder.encode(userName, forKey: "userName")
        }
		if accountName != nil{
			aCoder.encode(accountName, forKey: "accountName")
		}
		if bIntegrated != nil{
			aCoder.encode(bIntegrated, forKey: "bIntegrated")
		}
		if headImage != nil{
			aCoder.encode(headImage, forKey: "headImage")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if locked != nil{
			aCoder.encode(locked, forKey: "locked")
		}
		if newField != nil{
			aCoder.encode(newField, forKey: "new")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if pssword != nil{
			aCoder.encode(pssword, forKey: "pssword")
		}
		if regTime != nil{
			aCoder.encode(regTime, forKey: "regTime")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
        if integral != nil{
            aCoder.encode(integral, forKey: "integral")
        }
        if inviteCode != nil{
            aCoder.encode(inviteCode, forKey: "inviteCode")
        }
        if myCardEndTime1 != nil{
            aCoder.encode(myCardEndTime1, forKey: "myCardEndTime1")
        }
        if myCardEndTime2 != nil{
            aCoder.encode(myCardEndTime2, forKey: "myCardEndTime2")
        }
        if myCardEndTime3 != nil{
            aCoder.encode(myCardEndTime3, forKey: "myCardEndTime3")
        }
	}
}

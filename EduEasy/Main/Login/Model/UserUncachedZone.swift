//
//	UserUncachedZone.swift
//
//	Create by piggybear on 26/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserUncachedZone : NSObject, NSCoding{

	var cachable : Bool!
	var fixed : Bool!
	var iD : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cachable = json["cachable"].boolValue
		fixed = json["fixed"].boolValue
		iD = json["iD"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cachable != nil{
			dictionary["cachable"] = cachable
		}
		if fixed != nil{
			dictionary["fixed"] = fixed
		}
		if iD != nil{
			dictionary["iD"] = iD
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cachable = aDecoder.decodeObject(forKey: "cachable") as? Bool
         fixed = aDecoder.decodeObject(forKey: "fixed") as? Bool
         iD = aDecoder.decodeObject(forKey: "iD") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if cachable != nil{
			aCoder.encode(cachable, forKey: "cachable")
		}
		if fixed != nil{
			aCoder.encode(fixed, forKey: "fixed")
		}
		if iD != nil{
			aCoder.encode(iD, forKey: "iD")
		}

	}

}

//
//	UserRegTime.swift
//
//	Create by piggybear on 26/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserRegTime : NSObject, NSCoding{

	var afterNow : Bool!
	var beforeNow : Bool!
	var centuryOfEra : Int!
	var chronology : UserChronology!
	var dayOfMonth : Int!
	var dayOfWeek : Int!
	var dayOfYear : Int!
	var equalNow : Bool!
	var era : Int!
	var hourOfDay : Int!
	var millis : Int!
	var millisOfDay : Int!
	var millisOfSecond : Int!
	var minuteOfDay : Int!
	var minuteOfHour : Int!
	var monthOfYear : Int!
	var secondOfDay : Int!
	var secondOfMinute : Int!
	var weekOfWeekyear : Int!
	var weekyear : Int!
	var year : Int!
	var yearOfCentury : Int!
	var yearOfEra : Int!
	var customZone : UserZone!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		afterNow = json["afterNow"].boolValue
		beforeNow = json["beforeNow"].boolValue
		centuryOfEra = json["centuryOfEra"].intValue
		let chronologyJson = json["chronology"]
		if !chronologyJson.isEmpty{
			chronology = UserChronology(fromJson: chronologyJson)
		}
		dayOfMonth = json["dayOfMonth"].intValue
		dayOfWeek = json["dayOfWeek"].intValue
		dayOfYear = json["dayOfYear"].intValue
		equalNow = json["equalNow"].boolValue
		era = json["era"].intValue
		hourOfDay = json["hourOfDay"].intValue
		millis = json["millis"].intValue
		millisOfDay = json["millisOfDay"].intValue
		millisOfSecond = json["millisOfSecond"].intValue
		minuteOfDay = json["minuteOfDay"].intValue
		minuteOfHour = json["minuteOfHour"].intValue
		monthOfYear = json["monthOfYear"].intValue
		secondOfDay = json["secondOfDay"].intValue
		secondOfMinute = json["secondOfMinute"].intValue
		weekOfWeekyear = json["weekOfWeekyear"].intValue
		weekyear = json["weekyear"].intValue
		year = json["year"].intValue
		yearOfCentury = json["yearOfCentury"].intValue
		yearOfEra = json["yearOfEra"].intValue
		let zoneJson = json["zone"]
		if !zoneJson.isEmpty{
			customZone = UserZone(fromJson: zoneJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if afterNow != nil{
			dictionary["afterNow"] = afterNow
		}
		if beforeNow != nil{
			dictionary["beforeNow"] = beforeNow
		}
		if centuryOfEra != nil{
			dictionary["centuryOfEra"] = centuryOfEra
		}
		if chronology != nil{
			dictionary["chronology"] = chronology.toDictionary()
		}
		if dayOfMonth != nil{
			dictionary["dayOfMonth"] = dayOfMonth
		}
		if dayOfWeek != nil{
			dictionary["dayOfWeek"] = dayOfWeek
		}
		if dayOfYear != nil{
			dictionary["dayOfYear"] = dayOfYear
		}
		if equalNow != nil{
			dictionary["equalNow"] = equalNow
		}
		if era != nil{
			dictionary["era"] = era
		}
		if hourOfDay != nil{
			dictionary["hourOfDay"] = hourOfDay
		}
		if millis != nil{
			dictionary["millis"] = millis
		}
		if millisOfDay != nil{
			dictionary["millisOfDay"] = millisOfDay
		}
		if millisOfSecond != nil{
			dictionary["millisOfSecond"] = millisOfSecond
		}
		if minuteOfDay != nil{
			dictionary["minuteOfDay"] = minuteOfDay
		}
		if minuteOfHour != nil{
			dictionary["minuteOfHour"] = minuteOfHour
		}
		if monthOfYear != nil{
			dictionary["monthOfYear"] = monthOfYear
		}
		if secondOfDay != nil{
			dictionary["secondOfDay"] = secondOfDay
		}
		if secondOfMinute != nil{
			dictionary["secondOfMinute"] = secondOfMinute
		}
		if weekOfWeekyear != nil{
			dictionary["weekOfWeekyear"] = weekOfWeekyear
		}
		if weekyear != nil{
			dictionary["weekyear"] = weekyear
		}
		if year != nil{
			dictionary["year"] = year
		}
		if yearOfCentury != nil{
			dictionary["yearOfCentury"] = yearOfCentury
		}
		if yearOfEra != nil{
			dictionary["yearOfEra"] = yearOfEra
		}
		if customZone != nil{
			dictionary["zone"] = customZone.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         afterNow = aDecoder.decodeObject(forKey: "afterNow") as? Bool
         beforeNow = aDecoder.decodeObject(forKey: "beforeNow") as? Bool
         centuryOfEra = aDecoder.decodeObject(forKey: "centuryOfEra") as? Int
         chronology = aDecoder.decodeObject(forKey: "chronology") as? UserChronology
         dayOfMonth = aDecoder.decodeObject(forKey: "dayOfMonth") as? Int
         dayOfWeek = aDecoder.decodeObject(forKey: "dayOfWeek") as? Int
         dayOfYear = aDecoder.decodeObject(forKey: "dayOfYear") as? Int
         equalNow = aDecoder.decodeObject(forKey: "equalNow") as? Bool
         era = aDecoder.decodeObject(forKey: "era") as? Int
         hourOfDay = aDecoder.decodeObject(forKey: "hourOfDay") as? Int
         millis = aDecoder.decodeObject(forKey: "millis") as? Int
         millisOfDay = aDecoder.decodeObject(forKey: "millisOfDay") as? Int
         millisOfSecond = aDecoder.decodeObject(forKey: "millisOfSecond") as? Int
         minuteOfDay = aDecoder.decodeObject(forKey: "minuteOfDay") as? Int
         minuteOfHour = aDecoder.decodeObject(forKey: "minuteOfHour") as? Int
         monthOfYear = aDecoder.decodeObject(forKey: "monthOfYear") as? Int
         secondOfDay = aDecoder.decodeObject(forKey: "secondOfDay") as? Int
         secondOfMinute = aDecoder.decodeObject(forKey: "secondOfMinute") as? Int
         weekOfWeekyear = aDecoder.decodeObject(forKey: "weekOfWeekyear") as? Int
         weekyear = aDecoder.decodeObject(forKey: "weekyear") as? Int
         year = aDecoder.decodeObject(forKey: "year") as? Int
         yearOfCentury = aDecoder.decodeObject(forKey: "yearOfCentury") as? Int
         yearOfEra = aDecoder.decodeObject(forKey: "yearOfEra") as? Int
         customZone = aDecoder.decodeObject(forKey: "zone") as? UserZone

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if afterNow != nil{
			aCoder.encode(afterNow, forKey: "afterNow")
		}
		if beforeNow != nil{
			aCoder.encode(beforeNow, forKey: "beforeNow")
		}
		if centuryOfEra != nil{
			aCoder.encode(centuryOfEra, forKey: "centuryOfEra")
		}
		if chronology != nil{
			aCoder.encode(chronology, forKey: "chronology")
		}
		if dayOfMonth != nil{
			aCoder.encode(dayOfMonth, forKey: "dayOfMonth")
		}
		if dayOfWeek != nil{
			aCoder.encode(dayOfWeek, forKey: "dayOfWeek")
		}
		if dayOfYear != nil{
			aCoder.encode(dayOfYear, forKey: "dayOfYear")
		}
		if equalNow != nil{
			aCoder.encode(equalNow, forKey: "equalNow")
		}
		if era != nil{
			aCoder.encode(era, forKey: "era")
		}
		if hourOfDay != nil{
			aCoder.encode(hourOfDay, forKey: "hourOfDay")
		}
		if millis != nil{
			aCoder.encode(millis, forKey: "millis")
		}
		if millisOfDay != nil{
			aCoder.encode(millisOfDay, forKey: "millisOfDay")
		}
		if millisOfSecond != nil{
			aCoder.encode(millisOfSecond, forKey: "millisOfSecond")
		}
		if minuteOfDay != nil{
			aCoder.encode(minuteOfDay, forKey: "minuteOfDay")
		}
		if minuteOfHour != nil{
			aCoder.encode(minuteOfHour, forKey: "minuteOfHour")
		}
		if monthOfYear != nil{
			aCoder.encode(monthOfYear, forKey: "monthOfYear")
		}
		if secondOfDay != nil{
			aCoder.encode(secondOfDay, forKey: "secondOfDay")
		}
		if secondOfMinute != nil{
			aCoder.encode(secondOfMinute, forKey: "secondOfMinute")
		}
		if weekOfWeekyear != nil{
			aCoder.encode(weekOfWeekyear, forKey: "weekOfWeekyear")
		}
		if weekyear != nil{
			aCoder.encode(weekyear, forKey: "weekyear")
		}
		if year != nil{
			aCoder.encode(year, forKey: "year")
		}
		if yearOfCentury != nil{
			aCoder.encode(yearOfCentury, forKey: "yearOfCentury")
		}
		if yearOfEra != nil{
			aCoder.encode(yearOfEra, forKey: "yearOfEra")
		}
		if customZone != nil{
			aCoder.encode(customZone, forKey: "zone")
		}

	}

}

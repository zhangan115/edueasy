//
//	UserZone.swift
//
//	Create by piggybear on 26/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserZone : NSObject, NSCoding{

	var fixed : Bool!
	var iD : String!
	var uncachedZone : UserUncachedZone!
	var ref : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		fixed = json["fixed"].boolValue
		iD = json["iD"].stringValue
		let uncachedZoneJson = json["uncachedZone"]
		if !uncachedZoneJson.isEmpty{
			uncachedZone = UserUncachedZone(fromJson: uncachedZoneJson)
		}
		ref = json["$ref"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if fixed != nil{
			dictionary["fixed"] = fixed
		}
		if iD != nil{
			dictionary["iD"] = iD
		}
		if uncachedZone != nil{
			dictionary["uncachedZone"] = uncachedZone.toDictionary()
		}
		if ref != nil{
			dictionary["$ref"] = ref
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         fixed = aDecoder.decodeObject(forKey: "fixed") as? Bool
         iD = aDecoder.decodeObject(forKey: "iD") as? String
         uncachedZone = aDecoder.decodeObject(forKey: "uncachedZone") as? UserUncachedZone
         ref = aDecoder.decodeObject(forKey: "$ref") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if fixed != nil{
			aCoder.encode(fixed, forKey: "fixed")
		}
		if iD != nil{
			aCoder.encode(iD, forKey: "iD")
		}
		if uncachedZone != nil{
			aCoder.encode(uncachedZone, forKey: "uncachedZone")
		}
		if ref != nil{
			aCoder.encode(ref, forKey: "$ref")
		}

	}

}

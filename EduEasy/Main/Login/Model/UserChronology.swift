//
//	UserChronology.swift
//
//	Create by piggybear on 26/3/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserChronology : NSObject, NSCoding{

	var customZone : UserZone!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		let zoneJson = json["zone"]
		if !zoneJson.isEmpty{
			customZone = UserZone(fromJson: zoneJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if customZone != nil{
			dictionary["zone"] = customZone.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         customZone = aDecoder.decodeObject(forKey: "zone") as? UserZone

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if customZone != nil{
			aCoder.encode(customZone, forKey: "zone")
		}

	}

}

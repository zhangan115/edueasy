//
//  WeiXinLogin.swift
//  EduEasy
//
//  Created by sitech on 2019/4/17.
//  Copyright © 2019 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeiXinLogin: Mappable {
    
    var accessToken:String!
    var openId:String!
    var user:UserModel!
    
    required init(fromJson json: JSON!) {
        if json.isEmpty{
            return
        }
        self.accessToken = json["accessToken"].stringValue
        self.openId = json["openId"].stringValue
        let userJson = json["user"]
        if !userJson.isEmpty{
            self.user = UserModel(fromJson: userJson)
        }
    }
    
}

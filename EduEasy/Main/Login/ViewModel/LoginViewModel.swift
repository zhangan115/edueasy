//
//  LoginViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/26.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class LoginViewModel {
    
    let loginButton = PublishSubject<Void>()
    var checkbox: Variable<Bool> = Variable(true)
    var result: Driver<Result<UserModel>>!
    
    init(account: Observable<String>, password: Observable<String>) {
        let combineLatest = Observable.combineLatest(account, password)
        result = loginButton.withLatestFrom(combineLatest).flatMapLatest { (arg) -> Single<Result<UserModel>> in
            let (account, password) = arg
            let accountResult = self.validateAccount(account)
            switch accountResult {
            case .failed:
                return Single.just(accountResult)
            default: break
            }
            
            let passwordResult = self.validatePassword(password)
            switch passwordResult {
            case .failed:
                return Single.just(passwordResult)
                default: break
            }
            
            if !self.checkbox.value {
                return Single.just(.failed(message: "同意使用条款才可以使用"))
            }
            return userProvider
                .rxRequest(.login(username: account, password: password))
                .toModel(type: UserModel.self)
                .map{ return .success(model: $0)}
        }.asDriver(onErrorJustReturn: .empty)
    }
    
    func validateAccount(_ account: String) -> Result<UserModel> {
        if account.count == 0 {
            return .failed(message: "账号不能为空")
        }
        return .empty
    }
    
    func validatePassword(_ password: String) -> Result<UserModel> {
        if password.count == 0 {
            return .failed(message: "密码不能为空")
        }
        return .empty
    }
    
    init() {
        
    }

    func weiXinGetCode(code:String)-> Single<WeiXinLogin> {
        return userProvider.rxRequest(.weiXinLogin(code: code)).toModel(type: WeiXinLogin.self)
    }
    
}

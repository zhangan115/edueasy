//
//  SpreadController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/11.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class SpreadController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var inviteCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "推广大使"
        backButtonLogic()
        rightButtonLogic()
        qrCodeView.layer.cornerRadius = 8

        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.barTintColor = UIColor.white
        navigationBar?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        
        let user = UserModel.unarchiver()
        nameLabel.text = "我是 \(user?.accountName ?? "")"
        inviteCodeLabel.text = "邀请码：\(user?.inviteCode ?? "")"
        let image = createQRForString(qrString: "https://itunes.apple.com/cn/app/%E5%AD%A6%E5%8E%86%E6%97%A0%E5%BF%A7/id1372874627?mt=8", qrImageName: "icon_application")
        qrCodeImageView.image = image
    }

    func createQRForString(qrString: String?, qrImageName: String?) -> UIImage?{
        if let sureQRString = qrString{
            let stringData = sureQRString.data(using: String.Encoding.utf8, allowLossyConversion: false)
            //创建一个二维码的滤镜
            let qrFilter = CIFilter(name: "CIQRCodeGenerator")
            qrFilter?.setValue(stringData, forKey: "inputMessage")
            qrFilter?.setValue("H", forKey: "inputCorrectionLevel")
            let qrCIImage = qrFilter?.outputImage
            
            // 创建一个颜色滤镜,黑白色
            let colorFilter = CIFilter(name: "CIFalseColor")!
            colorFilter.setDefaults()
            colorFilter.setValue(qrCIImage, forKey: "inputImage")
            colorFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor0")
            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1")
            // 返回二维码image
            let codeImage = UIImage(ciImage: (colorFilter.outputImage!.transformed(by: CGAffineTransform(scaleX: 5, y: 5))))
            
            // 中间一般放logo
            if let iconImage = UIImage(named: qrImageName!) {
                let rect = CGRect(x: 0, y: 0, width: codeImage.size.width, height: codeImage.size.height)
                
                UIGraphicsBeginImageContext(rect.size)
                codeImage.draw(in: rect)
                let avatarSize = CGSize(width: rect.size.width*0.3, height: rect.size.height*0.3)
                
                let x = (rect.width - avatarSize.width) * 0.5
                let y = (rect.height - avatarSize.height) * 0.5
                iconImage.draw(in: CGRect(x: x, y: y, width: avatarSize.width, height: avatarSize.height))
                
                let resultImage = UIGraphicsGetImageFromCurrentImageContext()
                
                UIGraphicsEndImageContext()
                return resultImage
            }
            return codeImage
        }
        return nil
    }
    
    func backButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(pop))
        button.setImage(UIImage(named: "icon_btn_arrow_l_b"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func rightButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(shared))
        button.setTitle("分享", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.setTitleColor(UIColor(hexString: "#4294FC"), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func shared() {
        let controller = UIActivityViewController(activityItems: [qrCodeImageView.image!], applicationActivities: nil)
        self.presentVC(controller)
    }
}

extension SpreadController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
}

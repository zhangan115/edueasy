//
//  MyProfileHeader.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class MyProfileHeader: UITableViewHeaderFooterView {

    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(hexString: "#333333")
        self.contentView.addSubview(label)
        return label
    }()

    override func draw(_ rect: CGRect) {
        label.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.contentView.left).offset(15)
        }
    }
    
    func setText(_ text: String) {
        label.text = text
    }
}

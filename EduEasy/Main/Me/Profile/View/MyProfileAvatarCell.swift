//
//  MyProfileAvatarCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/18.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class MyProfileAvatarCell: UITableViewCell {

    lazy var avatarLabel: UILabel = {
        let label = UILabel()
        label.text = "头像"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(hexString: "#333333")
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 30
        self.contentView.addSubview(imageView)
        return imageView
    }()
    
    lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "icon_btn_arrow_r_g"))
        self.contentView.addSubview(imageView)
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        let user = UserModel.unarchiver()
        
        avatarImageView.loadNetworkImage(user?.headImage ?? "", placeholder: "img_avatar")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        avatarLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.contentView.left).offset(15)
        }
        
        arrowImageView.snp.updateConstraints { (make) in
             make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
            make.width.equalTo(18)
            make.height.equalTo(18)
        }
        
        avatarImageView.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.arrowImageView.snp.left).offset(-16)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
}

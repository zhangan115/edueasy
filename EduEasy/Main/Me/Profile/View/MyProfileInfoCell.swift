//
//  MyProfileInfoCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class MyProfileInfoCell: UITableViewCell {

    lazy var signLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#333333")
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor(hexString: "#999999")
        textField.textAlignment = .right
        textField.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(textField)
        return textField
    }()
    
    let user = UserModel.unarchiver()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIndexPath(_ indexPath: IndexPath) {
        if indexPath.section == 1 {
            signLabel.text = "昵称"
            return
        }
        
        let textList = ["真实姓名", "身份证号", "出生年月", "性别"]
        if indexPath.row < textList.count {
            signLabel.text = textList[indexPath.row]
        }
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        signLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.width.equalTo(90)
            make.left.equalTo(self.contentView.left).offset(15)
        }
        
        textField.snp.updateConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(self.contentView.snp.right).offset(-15)
            make.left.equalTo(self.signLabel.snp.right).offset(5)
        }
    }

}

//
//  MyProfileController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/18.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

private let avatarCellReuseIdentifier = "MyProfileAvatarCell"
private let infoCellReuseIdentifier = "MyProfileInfoCell"
private let buttonCellReuseIdentifier = "ApplyButtonCell"

class MyProfileController: NavigationBaseTableController {

    struct ApplyDataModel {
        var userName: String!
        var realName: String!
        var card: String!
    }
    var dataModel = ApplyDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0)
        setCustomTitle("个人信息")
        tableViewRegister()
        hiddenNaviBarLine()
        
        let user = UserModel.unarchiver()
        dataModel.realName = user?.realName
        dataModel.userName = user?.userName
        dataModel.card = user?.idcard
        
    }
    func tableViewRegister() {
        tableView.register(MyProfileAvatarCell.self, forCellReuseIdentifier: avatarCellReuseIdentifier)
        tableView.register(MyProfileInfoCell.self, forCellReuseIdentifier: infoCellReuseIdentifier)
         tableView.register(ApplyButtonCell.self, forCellReuseIdentifier: buttonCellReuseIdentifier)
    }
    
    func commitHandler() {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! MyProfileInfoCell
        let userName = cell.textField.text ?? ""
        if userName.count == 0 {
            self.view.showAutoHUD("请输入昵称")
            return
        }
        
        let cell1 = tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! MyProfileInfoCell
        let realName = cell1.textField.text ?? ""
        if realName.count == 0 {
            self.view.showAutoHUD("请输入您的真实姓名")
            return
        }
        
        let cell2 = tableView.cellForRow(at: IndexPath(row: 1, section: 2)) as! MyProfileInfoCell
        let card = cell2.textField.text ?? ""
        if card.count == 0 {
            self.view.showAutoHUD("请输入身份证号码")
            return
        }
        self.view.showHUD(message: "正在提交...")
        let user = UserModel.unarchiver()
        homeProvider.requestResult(.info(accountId: user?.id ?? 0,
                                         realName: realName,
                                         idcard: card,
                                         userName: userName),
                                   success: {[weak self] (json) in
                                    guard let `self` = self else {
                                        return
                                    }
                                    let model = UserModel(fromJson: json["data"])
                                    let user = UserModel.unarchiver()
                                    user?.idcard = model.idcard
                                    user?.realName = model.realName
                                    user?.userName = model.userName
                                    UserModel.archiverUser(user!)
                                    self.view.showHUD("提交成功", completion: {
                                        self.view.hiddenHUD()
                                        self.popVC()
                                    })
        }) {[weak self] _ in
            self?.view.hiddenHUD()
        }
    }
}

extension MyProfileController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return 2
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: avatarCellReuseIdentifier, for: indexPath) as! MyProfileAvatarCell
            return cell
        }
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: buttonCellReuseIdentifier, for: indexPath) as! ApplyButtonCell
            cell.buttonTapCallback = {[weak self] in
                self?.commitHandler()
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: infoCellReuseIdentifier, for: indexPath) as! MyProfileInfoCell
        cell.setIndexPath(indexPath)
        if indexPath.section == 1 {
            cell.textField.text = dataModel.userName
        }else {
            if indexPath.row == 0 {
                cell.textField.text = dataModel.realName
            }
            if indexPath.row == 1 {
                cell.textField.text = dataModel.card
            }
        }
        return cell
    }
}

extension MyProfileController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "Header"
        var view: MyProfileHeader? = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? MyProfileHeader
        if view == nil {
            view = MyProfileHeader(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = kBackgroundColor
            view?.backgroundView = backgroundView
        }
        if section == 2 {
            view?.setText("完善资料")
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 35
        }
        return 10
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 88
        }
        return 48
    }
}


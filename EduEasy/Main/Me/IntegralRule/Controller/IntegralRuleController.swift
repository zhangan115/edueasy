//
//  IntegralRuleController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class IntegralRuleController: MeBaseController {
    
    let cellReuseIdentifier = "IntegralRuleCell"
    let titleList = ["积分开通规则", "积分获得", "积分消耗"]
    let contentList = ["只要注册学历无优会员即可参与积分回馈活动;", "使用推广二维码或未来支持的其他推荐方式成功推荐其他用户入会或续费的,奖励积分600;用用户使用他人推广维码或未来支持的其他推荐方式入会或续费的,奖奖励积分60;", "兑换为会员资格,即积分数达6000个可获得学位英语和计算机的免费使用权限"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCustomTitle("积分规则")
        tableViewRegister()
    }
    
    func tableViewRegister() {
        tableView.register(IntegralRuleCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
}

extension IntegralRuleController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! IntegralRuleCell
        cell.set(title: titleList[indexPath.row], content: contentList[indexPath.row])
        return cell
    }
}

extension IntegralRuleController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

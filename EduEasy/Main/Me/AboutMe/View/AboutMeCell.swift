//
//  AboutMeCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import SnapKit

class AboutMeCell: UITableViewCell {

    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "icon_application"))
        self.contentView.addSubview(imageView)
        return imageView
    }()
    
    lazy var appDisplayNameLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#333333")
        label.text = BundleConfig.appDisplayName
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var versionLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#999999")
        label.text = "当前版本号 V\(BundleConfig.appVersion)"
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var contentLabel: UILabel = {
        let label = self.createLabel()
        label.textColor = UIColor(hexString: "#999999")
        label.textAlignment = .left
        label.numberOfLines = 0
        let text = "学历无优是阳明教育为成人高等函授教育,学位考试,高职统招本科考试专门开发的一款软件。该软件集合了近四年的考试真题,通过大数据分类集合原理使各位考生能够更顺利通过考试。"
        label.set(text: text, lineSpace: 10)
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        iconImageView.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.top.equalTo(self.contentView.snp.top).offset(25)
        }
        appDisplayNameLabel.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.top.equalTo(self.iconImageView.snp.bottom).offset(10)
        }
        versionLabel.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.top.equalTo(self.appDisplayNameLabel.snp.bottom).offset(17.5)
        }
        contentLabel.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.top.equalTo(self.versionLabel.snp.bottom).offset(30)
            make.left.equalTo(self.contentView.snp.left).offset(20)
            make.right.equalTo(self.contentView.snp.right).offset(-20)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-25)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }
    
}

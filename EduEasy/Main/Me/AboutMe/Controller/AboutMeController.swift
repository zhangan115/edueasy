//
//  AboutMeController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class AboutMeController: MeBaseController {

    let cellReuseIdentifier = "AboutMeCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setCustomTitle("关于我们")
        tableViewRegister()
    }
    
    func tableViewRegister() {
        tableView.register(AboutMeCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
}

extension AboutMeController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AboutMeCell
        return cell
    }
}

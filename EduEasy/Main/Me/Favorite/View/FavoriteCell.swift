//
//  FavoriteCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class FavoriteCell: UITableViewCell {

    lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 3
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentLabel.snp.updateConstraints { (make) in
            make.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(15, 15, 15, 15))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContentText(_ text: String) {
        contentLabel.attributedText = attributedText(text: styledHTML(text))
    }
    
    func attributedText(text: String) -> NSAttributedString {
        let data = text.data(using: String.Encoding.unicode, allowLossyConversion: true)
        let options = [NSAttributedString.DocumentReadingOptionKey .documentType: NSAttributedString.DocumentType.html]
        let attrString = try! NSAttributedString(data: data!, options: options, documentAttributes: nil)
        return attrString
    }
    
    func styledHTML(_ html: String) -> String {
        let style: String = "<head><meta charset=\"UTF-8\"><style> body {font-size:16px; color: #333333;}</style></head>"
        return style + html
    }
}

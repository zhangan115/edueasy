//
//  FavoriteViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift

class FavoriteViewModel {
    func request(accountId: Int, lastId: Int?) -> Single<[ExamModel]> {
        return meProvider.rxRequest(.favorite(accountId: accountId, lastId: lastId)).toListModel(type: ExamModel.self)
    }
    func requestRecord(accountId: Int,bResult: String, lastId: Int?) -> Single<[ExamModel]> {
        return meProvider.rxRequest(.getRecordList(accountId:accountId,bResult: bResult, lastId: lastId)).toListModel(type: ExamModel.self)
    }
}

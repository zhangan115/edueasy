//
//  FavoriteController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class FavoriteController: MeBaseController {

    let cellReuseIdentifier = "FavoriteCell"
    
    let disposeBag = DisposeBag()
    let viewModel = FavoriteViewModel()
    
    var responseList: [ExamModel]!
    var lastId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCustomTitle("我的收藏")
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0)
        setTableViewHeader()
//        setTableViewFooter()
        tableViewRegister()
    }
    
    func tableViewRegister() {
        tableView.register(FavoriteCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func setTableViewHeader() {
        let refreshHeader = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_header = refreshHeader
        refreshHeader?.beginRefreshing()
    }
    
    func setTableViewFooter() {
        let footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_footer = footer
        tableView.mj_footer.isHidden = true
    }
    
    @objc func request() {
        self.lastId = nil
        let user: UserModel = UserModel.unarchiver()!
        viewModel.request(accountId: user.id, lastId: lastId).subscribe(onSuccess: {[weak self] (list) in
            guard let `self` = self else {
                return
            }
            self.tableView.mj_header.endRefreshing()
            self.responseList = list
            self.lastId = list.last?.id
            self.tableView.reloadData()
        }) {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
        }.disposed(by: disposeBag)
    }
}

extension FavoriteController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = responseList {
            return list.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FavoriteCell
        let model = responseList[indexPath.row]
        cell.setContentText(model.question)
        return cell
    }
}

extension FavoriteController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = ExamController()
        controller.customTitle = "题目"
        controller.isUserEnabled = false
        controller.responseList = responseList
        controller.model = responseList[indexPath.row]
        controller.isAnswer = true
        self.pushVC(controller)
    }
}

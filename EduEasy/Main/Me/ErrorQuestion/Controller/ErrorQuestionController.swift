//
//  ErrorQuestion.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import MJRefresh

class ErrorQuestionController: MeBaseController {
    
    let cellReuseIdentifier = "ErrorQuestion"
    
    var examType: ExamType!
    var yearType: YearType!
    var paperTitle: String!
    var pagerType: String!
    var pagerYear: String!
    var questionCatalogId: String!
    
    let disposeBag = DisposeBag()
    let viewModel = ErrorQuestionViewModel()
    var responseList: [ErrorQuestionModel]!
    var questionTypeList: [QuestionTypeModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCustomTitle("过往答错")
        tableViewRegister()
//        rightButtonLogic()
        setTableViewHeader()
        
        viewModel.questionTypeRequest().subscribe(onSuccess: {[weak self] (list) in
            self?.questionTypeList = list
            }, onError: nil).disposed(by: disposeBag)
    }
    
    func setTableViewHeader() {
        let refreshHeader = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_header = refreshHeader
        refreshHeader?.beginRefreshing()
    }
    
    func tableViewRegister() {
        tableView.register(ErrorQuestion.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func rightButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(rightButtonHandler))
        button.setTitle("筛选", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.setTitleColor(UIColor(hexString: "#308AFF"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func rightButtonHandler() {
        let controller = OldExamFilterAnimationController()
        controller.questionTypeList = questionTypeList
        controller.callback = { [weak self] (title, paperType, pagerYear, questionCatalogId) in
            guard self != nil else {
                return
            }
        }
        self.present(controller, animated: false, completion: nil)
    }
    
    @objc func request() {
        viewModel.request().subscribe(onSuccess: {[weak self] (list) in
            guard let `self` = self else {
                return
            }
            self.tableView.mj_header.endRefreshing()
            self.responseList = list
            self.tableView.reloadData()
            
        }) {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
        }.disposed(by: disposeBag)
    }
}

extension ErrorQuestionController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = responseList {
            return list.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ErrorQuestion
        cell.setContentText(responseList[indexPath.row].title)
        return cell
    }
}

extension ErrorQuestionController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = ErrorQuestionPageController()
        controller.examedPaperId = responseList[indexPath.row].examedPaperId
        self.pushVC(controller)
    }
}

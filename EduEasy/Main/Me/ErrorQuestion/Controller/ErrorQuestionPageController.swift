//
//  ErrorQuestionPageController.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

import SnapKit
import RxSwift
import SwiftyJSON

class ErrorQuestionPageController: BeanController {
    lazy var bottomView: ExamBottomView = {
        let view = ExamBottomView()
        view.backgroundColor = UIColor.white
        self.view.addSubview(view)
        let tap = UITapGestureRecognizer(target: self, action: #selector(bottomViewTapHandler))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    var pageContentView: SGPageContentView!
    var examedPaperId: Int = 0
    var responseList: [ExamModel]!
    
    let viewModel = ErrorQuestionViewModel()
    let disposeBag = DisposeBag()
    
    let space: CGFloat = 10
    let bottomViewHeight: CGFloat = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "答案"
        
        viewModel.examedPaperId.value = examedPaperId
        viewModel.result.asObservable().subscribe(onNext: {[weak self] list in
            if self == nil {
                return
            }
            let this = self!
            this.responseList = list
            this.setup()
        }).disposed(by: disposeBag)
    }
    
    func setup() {
        var controllers: [ExamController] = []
        var index = 0
        var lastItem: ExamModel = responseList.first!
        for value in responseList {
            if value.flag != lastItem.flag {
                index = 0
            }
            index += 1
            let controller = ExamController()
            controller.isAnswer = true
            controller.isUserEnabled = false
            controller.responseList = responseList
            controller.model = value
            controllers.append(controller)
            
            lastItem = value
        }
        let frame = CGRect(x: 0,
                           y: 0,
                           w: self.view.bounds.size.width,
                           h: self.view.bounds.size.height - space - bottomViewHeight)
        pageContentView = SGPageContentView(frame: frame, parentVC: self, childVCs: controllers)
        pageContentView.delegatePageContentView = self
        pageContentView.backgroundColor = UIColor(hexString: "#24247F")
        view.addSubview(pageContentView)
        setupSubviewsFrame()
        self.setIndex(1)
    }
    
    @objc func bottomViewTapHandler() {
        let controller = TitleIndexAnimationController()
        controller.setResponseList(responseList)
        self.present(controller, animated: false, completion: nil)
        controller.didSelectItemCallback = {[weak self] index in
            self?.pageContentView.setPageCententViewCurrentIndex(index)
             self?.setIndex(index + 1)
        }
    }
    
    func setupSubviewsFrame() {
        bottomView.frame = CGRect(x: 0,
                                  y: pageContentView.frame.maxY + space,
                                  w: self.view.bounds.size.width,
                                  h: bottomViewHeight)
        
    }
    
    func setIndex(_ index: Int) {
        bottomView.model = responseList[index - 1]
        bottomView.setIndexTitle("\(index)/\(responseList.count)题序")
    }
}

extension ErrorQuestionPageController: SGPageContentViewDelegate {
    func pageContentView(_ pageContentView: SGPageContentView!, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        setIndex(targetIndex + 1)
    }
}


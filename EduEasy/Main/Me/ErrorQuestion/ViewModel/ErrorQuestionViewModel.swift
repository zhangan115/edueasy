//
//  ErrorQuestionViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class ErrorQuestionViewModel {
    
    var examedPaperId = Variable<Int>(0)
    let result: Driver<[ExamModel]>!
    
    init() {
        result = examedPaperId.asObservable().flatMapLatest { (id) -> Single<[ExamModel]> in
            return meProvider
                  .rxRequest(.errorTitleDetail(examedPaperId: id))
                  .toListModel(type: ExamModel.self)
            }.asDriver(onErrorJustReturn: [ExamModel(fromJson: JSON(""))])
    }
    
    func request() -> Single<[ErrorQuestionModel]> {
        let user = UserModel.unarchiver()!
        return meProvider.rxRequest(.errorTitleList(accountId: user.id)).toListModel(type: ErrorQuestionModel.self)
    }
    
    func questionTypeRequest() -> Single<[QuestionTypeModel]> {
        return questionBankProvider.rxRequest(.questionstype).toListModel(type: QuestionTypeModel.self)
    }
}

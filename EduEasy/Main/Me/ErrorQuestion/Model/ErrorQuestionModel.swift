//
//	ErrorQuestionModel.swift
//
//	Create by piggybear on 7/4/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ErrorQuestionModel : Mappable{

	var examedPaperId : Int!
	var id : Int!
	var isYear : String!
	var newField : Bool!
	var pagerType : String!
	var pagerYear : String!
	var questionCatalog : ErrorQuestionQuestionCatalog!
	var title : String!
	var type : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		examedPaperId = json["examedPaperId"].intValue
		id = json["id"].intValue
		isYear = json["isYear"].stringValue
		newField = json["new"].boolValue
		pagerType = json["pagerType"].stringValue
		pagerYear = json["pagerYear"].stringValue
		let questionCatalogJson = json["questionCatalog"]
		if !questionCatalogJson.isEmpty{
			questionCatalog = ErrorQuestionQuestionCatalog(fromJson: questionCatalogJson)
		}
		title = json["title"].stringValue
		type = json["type"].stringValue
	}
}

//
//	ErrorQuestionQuestionCatalog.swift
//
//	Create by piggybear on 7/4/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ErrorQuestionQuestionCatalog {

	var id : Int!
	var newField : Bool!
	var title : String!

	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		id = json["id"].intValue
		newField = json["new"].boolValue
		title = json["title"].stringValue
	}
}

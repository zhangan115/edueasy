//
//  VipViewModel.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import RxSwift

class VipViewModel {
    func request() -> Single<[VipModel]> {
        return vipProvider
            .rxRequest(.cardList)
            .toListModel(type: VipModel.self)
    }
    
    func requestPayCallBack(_ outTradeNo:String!,_ trade_no:String?) -> Single<UserModel> {
        return vipProvider
            .rxRequest(.payCallBack(outTradeNo: outTradeNo,trade_no: trade_no))
            .toModel(type: UserModel.self)
    }
    
    func requestSchoolList() -> Single<[PaySchoolModel]> {
        return vipProvider
            .rxRequest(.getSchoolList)
            .toListModel(type: PaySchoolModel.self)
    }
}

//
//  WeiXinPayModel.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeiXinPayModel: Mappable{
    
    var sign :String!
    var package :String!
    var partnerid :String!
    var noncestr :String!
    var appid :String!
    var outTradeNo :String!
    var timestamp :String!
    var prepayid :String!
    
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        self.sign = json["sign"].stringValue
        self.package = json["package"].stringValue
        self.partnerid = json["partnerid"].stringValue
        self.noncestr = json["noncestr"].stringValue
        self.appid = json["appid"].stringValue
        self.outTradeNo = json["outTradeNo"].stringValue
        self.timestamp = json["timestamp"].stringValue
        self.prepayid = json["prepayid"].stringValue
    }
    
}

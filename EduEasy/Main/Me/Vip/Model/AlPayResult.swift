//
//  AlPayResult.swift
//  EduEasy
//
//  Created by zhang an on 2018/12/22.
//  Copyright © 2018 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON

class AlPayResult: Mappable {
    
    var  alipay_trade_app_pay_response:AlPayResponse!
    var  sign:String!
    var  sign_type:String!
    
    required init(fromJson json: JSON!) {
        self.sign = json["sign"].stringValue
        self.sign_type = json["sign_type"].stringValue
        self.alipay_trade_app_pay_response = AlPayResponse(fromJson: JSON(json["alipay_trade_app_pay_response"].stringValue))
    }
    
    class AlPayResponse: Mappable {
        
        var  code:String!
        var  msg:String!
        var  app_id:String!
        var  auth_app_id:String!
        var  charset:String!
        var  timestamp:String!
        var  out_trade_no:String!
        var  total_amount:String!
        var  trade_no:String!
        var  seller_id:String!
        
        required init(fromJson json: JSON!) {
            self.code = json["code"].stringValue
            self.msg = json["msg"].stringValue
            self.app_id = json["app_id"].stringValue
            self.auth_app_id = json["auth_app_id"].stringValue
            self.charset = json["charset"].stringValue
            self.timestamp = json["timestamp"].stringValue
            self.out_trade_no = json["out_trade_no"].stringValue
            self.total_amount = json["total_amount"].stringValue
            self.trade_no = json["trade_no"].stringValue
            self.seller_id = json["seller_id"].stringValue
        }
    }
}

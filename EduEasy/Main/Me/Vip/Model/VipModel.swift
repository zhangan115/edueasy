//
//	VipModel.swift
//
//	Create by piggybear on 6/5/2018
//	Copyright © 2018 小猪熊集团. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class VipModel: Mappable{

	var cardName : String!
	var id : Int!
	var month : String!
	var price : String!

	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cardName = json["cardName"].stringValue
		id = json["id"].intValue
		month = json["month"].stringValue
		price = json["price"].stringValue
	}

}

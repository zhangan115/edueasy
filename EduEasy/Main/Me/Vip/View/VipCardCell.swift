//
//  VipCardCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class VipCardCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var vipImageView: UIImageView!
    @IBOutlet weak var vipLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let width: CGFloat = 275
    let height: CGFloat = 140
    let space: CGFloat = 15
    var currentPage: CGFloat = 0
    var callback: ((Int)->())?
    var cardList: [VipCardView] = []
    
    var isVip: Bool = false {
        willSet(value){
            if value {
                vipImageView.image = UIImage(named: "tag_vip")
                vipLabel.text = "你已是会员"
            }else {
                vipImageView.image = UIImage(named: "tag_vip2")
                vipLabel.text = "你还不是会员"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCardList()
        let user = UserModel.unarchiver()
        nameLabel.text = user?.realName
        avatarImageView.loadNetworkImage(user?.headImage ?? "", placeholder: "img_avatar")
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = 30
    }
    
    func setupCardList() {
        let marginLeft: CGFloat = vipLabel.frame.x
        let count = 3
        let images = ["card_f", "card_s", "card_t"]
        let prices = ["118", "280", "380"]
        for idx in 0..<count {
            let view = VipCardView()
            view.tag = idx + 1
            view.setText(prices[idx])
            view.setImage(images[idx])
            scrollView.addSubview(view)
            view.frame = CGRect(x: CGFloat(idx) * (width + space), y: 0, width: width, height: height)
//            let tap = UITapGestureRecognizer(target: self, action: #selector(cardViewTapHandle(_:)))
//            view.addGestureRecognizer(tap)
//            cardList.append(view)
        }
        scrollView.contentSize = CGSize(width: marginLeft + (width + space) * CGFloat(count), height: 0)
    }
    
    func cardViewTapHandle(_ sender: UIGestureRecognizer) {
        let view = sender.view as! VipCardView
        callback?(view.tag)
    }
    
    func setList(_ list: [VipModel]) {
        for item in list {
            for view in cardList where item.id == view.tag {
                view.setText(item.price)
            }
        }
    }
}

extension VipCardCell: UIScrollViewDelegate {
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / (width + space)
        callback?(Int(page) + 1)
    }
}

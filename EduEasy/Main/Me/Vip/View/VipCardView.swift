//
//  VipCardView.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class VipCardView: UIView {

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        self.addSubview(imageView)
        return imageView
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.white
        self.addSubview(label)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.snp.updateConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        label.snp.updateConstraints { (make) in
            make.left.equalTo(self).offset(162)
            make.top.equalTo(self).offset(71)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ text: String) {
        label.text = "\(text)元 / 年"
    }
    
    func setImage(_ imageNamed: String) {
        imageView.image = UIImage(named: imageNamed)
    }
}

//
//  VipPayCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class VipPayCell: UITableViewCell {

    @IBOutlet weak var openVipButton: UIButton!
    @IBOutlet weak var aliPayButton: UIButton!
    @IBOutlet weak var wechatPayButton: UIButton!
    
    @IBOutlet weak var wetchatView: UIView!
    @IBOutlet weak var alipayView: UIView!
    
    var callback: ((Bool)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        openVipButton.layer.masksToBounds = true
        openVipButton.layer.cornerRadius = 8
        
        let wetchatViewTap = UITapGestureRecognizer(target: self, action: #selector(paymentHandle(_:)))
        let alipayViewTap = UITapGestureRecognizer(target: self, action: #selector(paymentHandle(_:)))
        wetchatView.addGestureRecognizer(wetchatViewTap)
        alipayView.addGestureRecognizer(alipayViewTap)
    }

    @objc func paymentHandle(_ sender: UITapGestureRecognizer) {
        aliPayButton.isSelected = false
        wechatPayButton.isSelected = false
        
        let view = sender.view
        if view?.tag == 0 {
            aliPayButton.isSelected = true
        }else {
            wechatPayButton.isSelected = true
        }
    }
    
    @IBAction func openVipHandle(_ sender: UIButton) {
        var aliPay: Bool = true
        if wechatPayButton.isSelected {
            aliPay = false
        }
        callback?(aliPay)
    }
    
}

//
//  VipController.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift

private let cardCellReuseIdentifier = "VipCardCell"
private let interestCellReuseIdentifier = "VipInterestCell"
private let payCellReuseIdentifier = "VipPayCell"

class VipController: MeBaseController {
    
    let viewModel = VipViewModel()
    let disposeBag = DisposeBag()
    
    var dataList: [VipModel]!
    var carId: Int = 1
    
    var weiXinPayModel : WeiXinPayModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCustomTitle("会员充值")
        
        tableView.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.white
        
        tableView.separatorStyle = .none
        let cardNib = UINib(nibName: "VipCardCell", bundle: nil)
        tableView.register(cardNib, forCellReuseIdentifier: cardCellReuseIdentifier)
        
        let interestNib = UINib(nibName: "VipInterestCell", bundle: nil)
        tableView.register(interestNib, forCellReuseIdentifier: interestCellReuseIdentifier)
        
        let payNib = UINib(nibName: "VipPayCell", bundle: nil)
        tableView.register(payNib, forCellReuseIdentifier: payCellReuseIdentifier)
        
        viewModel
            .request()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] (list) in
                self?.dataList = list
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playSuccessfully"))
            .subscribe(onNext: {[weak self] (sender) in
                let model = sender.object as! AlPayResult
                self?.playSuccessfullyMonitor(model.alipay_trade_app_pay_response.out_trade_no ,model.alipay_trade_app_pay_response.trade_no)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playWeiXinSuccessfully"))
            .subscribe(onNext: {[weak self] (sender) in
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    self?.view.showAutoHUD("支付成功")
                })
                self?.playSuccessfullyMonitor(self?.weiXinPayModel?.outTradeNo,nil)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "playWeiXinFail"))
            .subscribe(onNext: {[weak self] (sender) in
                if self == nil{
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    self?.view.showAutoHUD("支付失败")
                })
            }).disposed(by: disposeBag)
        
    }
    
    func request(isAilpay:Bool) {
        if isAilpay {
            vipProvider.requestResult(.orderString(carId: carId,payType:"1"), success: {[weak self] (json) in
                let orderString = json["data"]
                self?.pay(orderString: orderString.stringValue)
            })
        }else{
            if WXApi.isWXAppInstalled(){
                self.view.showAutoHUD("没有安装微信")
                return
            }
            vipProvider.requestResult(.orderString(carId: carId,payType:"0"), success: {[weak self] (json) in
                if self != nil {
                    self?.weiXinPayModel = WeiXinPayModel.init(fromJson: json["data"])
                    if  self?.weiXinPayModel != nil {
                        WXApi.send(WeiXinPayUtil.toPay(model:self!.weiXinPayModel!))
                    }
                }
            })
        }
    }
    
    func pay(orderString: String) {
        let appScheme = "alipaySDK"
        AlipaySDK.defaultService().payOrder(orderString, fromScheme: appScheme) { (resultDic) in
            print("resultDic = ", resultDic ?? [:])
            PaymentUtils.ailpaySuccessfully(with: resultDic)
        }
    }
    
    func playSuccessfullyMonitor(_ outTradeNo:String!,_ trade_no:String?) {
        viewModel.requestPayCallBack(outTradeNo,trade_no).observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] (model) in
                UserModel.archiverUser(model)
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
}

extension VipController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cardCellReuseIdentifier, for: indexPath) as! VipCardCell
            let user = UserModel.unarchiver()
            cell.isVip = user?.isVip ?? false
            if let list = dataList {
                cell.setList(list)
            }
            cell.callback = {[weak self] idx in
                self?.carId = idx
            }
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: interestCellReuseIdentifier, for: indexPath)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: payCellReuseIdentifier, for: indexPath) as! VipPayCell
        cell.callback = {[weak self] isAlipay in
            self?.request(isAilpay: isAlipay)
        }
        return cell
    }
}

extension VipController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

//
//  MeController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import PGActionSheet

class MeController: BaseTableController {

    let infoCellIdentifier = "MeInfoCell"
    let cellIdentifier = "MeCell"
    let logoutCellIdentifier = "LogoutCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "个人中心"
        tableView.separatorInset = UIEdgeInsetsMake(0, 50, 0, 0)
        tableView.bounces = false
        tableViewRegister()
    }
    
    func tableViewRegister() {
        tableView.register(MeInfoCell.self, forCellReuseIdentifier: infoCellIdentifier)
        tableView.register(MeCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(LogoutCell.self, forCellReuseIdentifier: logoutCellIdentifier)
    }
}

extension MeController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if UserDefaults.standard.bool(forKey: "review") {
                return 4
            }
            return 3
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: infoCellIdentifier, for: indexPath) as! MeInfoCell
            cell.avatarTapClosure = {[weak self] in
                let controller = MyProfileController()
                self?.pushVC(controller)
            }
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MeCell
            cell.setInex(indexPath.row)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: logoutCellIdentifier, for: indexPath) as! LogoutCell
        return cell
    }
}

extension MeController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            if UserDefaults.standard.bool(forKey: "review") {
                switch indexPath.row {
                case 0: //积分规则
                    let controller = IntegralRuleController()
                    self.pushVC(controller)
                    return
                case 1: //会员充值
                    let controller = VipController()
                    self.pushVC(controller)
                    break
                case 2: //推广大使
                    let controller = SpreadController()
                    self.pushVC(controller)
                    break
                case 3: //关于我们
                    let controller = AboutMeController()
                    self.pushVC(controller)
                    return
                default: break
                }
            }else{
                switch indexPath.row {
                case 0: //积分规则
                    let controller = IntegralRuleController()
                    self.pushVC(controller)
                    return
                case 1: //推广大使
                    let controller = SpreadController()
                    self.pushVC(controller)
                    break
                case 2: //关于我们
                    let controller = AboutMeController()
                    self.pushVC(controller)
                    return
                default: break
                }
            }
        
        }
        
        if indexPath.section == 2 {
            let actionSheet = PGActionSheet(cancelButton: true, buttonList: ["确认退出"])
            actionSheet.actionSheetTranslucent = false
            actionSheet.handler = {_ in
                setIsLogin(false)
                let navi = BaseNavigationController(rootViewController: LoginController())
                self.present(navi, animated: false, completion: nil)
            }
            self.present(actionSheet, animated: false, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerIdentifier = "footer"
        var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier)
        if view == nil {
            view = UITableViewHeaderFooterView(reuseIdentifier: headerIdentifier)
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            view?.backgroundView = backgroundView
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 190
        }
        return 50
    }
}

extension MeController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if setupClass() {
            self.navigationController?.navigationBar.isHidden = true
        }else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupClass() ->Bool {
        let controller = self.currentViewController
        if controller.isKind(of: QuestionBankController.self) ||
            controller.isKind(of: StudyPageController.self) ||
            controller.isKind(of: HomeController.self) {
            return true
        }
        return false
    }
}

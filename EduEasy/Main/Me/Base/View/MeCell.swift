//
//  MeCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/18.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class MeCell: UITableViewCell {
    
    lazy var signImageView: UIImageView = {
        let imageView = UIImageView()
        self.contentView.addSubview(imageView)
        return imageView
    }()
    
    lazy var signLabel: UILabel = {
        let label = UILabel()
        self.contentView.addSubview(label)
        label.textColor = UIColor(hexString: "#333333")
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var button: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "icon_btn_arrow_r_g"), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        btn.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        self.contentView.addSubview(btn)
        return btn
    }()
    
    func setInex(_ index: Int) {
        var names :[String] = []
        var imageNames :[String] = []
        if UserDefaults.standard.bool(forKey: "review") {
            names = ["积分规则", "会员充值", "推广大使", "关于我们"]
            imageNames = ["icon_list_integral","icon_list_vip", "icon_list_spread", "icon_list_about"]
        }else{
            names = ["积分规则", "推广大使", "关于我们"]
            imageNames = ["icon_list_integral", "icon_list_spread", "icon_list_about"]
        }
        
        if names.count > index {
            signLabel.text = names[index]
            signImageView.image = UIImage(named: imageNames[index])
        }
        if names.count - 1 == index {
            let version: String = "当前版本V\(BundleConfig.appVersion)"
            button.setTitle(version, for: .normal)
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        signImageView.snp.updateConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.centerY.equalTo(self.contentView.snp.centerY)
        }
        
        signLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.signImageView.snp.right).offset(15)
            make.centerY.equalTo(self.contentView.snp.centerY)
        }
        
        button.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
        button.layout(imagePosition: .right, titleSpace: 15)
    }
}

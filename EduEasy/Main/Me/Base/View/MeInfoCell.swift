//
//  MeInfoCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/17.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class MeInfoCell: UITableViewCell {
    let disposeBag = DisposeBag()
    var avatarTapClosure: (()->())?
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "个人中心"
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 17)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var avatar: UIImageView = {
        let imageView = UIImageView()
        self.contentView.addSubview(imageView)
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 30
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(label)
        return label
    }()
    
    lazy var integralLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        self.contentView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        let user = UserModel.unarchiver()
        integralLabel.text = "积分 \(user?.integral ?? "")"
        
        let tap = UITapGestureRecognizer()
        tap.rx.event.subscribe(onNext: { [weak self] _ in
            self?.avatarTapClosure?()
        }).disposed(by: disposeBag)
        avatar.addGestureRecognizer(tap)
        
        let backgroundView = UIImageView(image: UIImage(named: "img_minebg"))
        self.backgroundView = backgroundView
        
        nameLabel.text = user?.realName
        avatar.loadNetworkImage(user?.headImage ?? "", placeholder: "img_avatar_default")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        titleLabel.snp.updateConstraints { (make) in
            make.centerX.equalTo(self.contentView.centerX)
            make.top.equalTo(self.contentView.snp.top).offset(40)
        }
        
        avatar.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        
        nameLabel.snp.updateConstraints { (make) in
            make.top.equalTo(self.avatar.snp.top).offset(10)
            make.left.equalTo(self.avatar.snp.right).offset(25)
        }
        
        integralLabel.snp.updateConstraints { (make) in
            make.top.equalTo(self.nameLabel.snp.bottom).offset(10)
            make.left.equalTo(self.nameLabel.snp.left)
        }
    }
}

//
//  LogoutCell.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/18.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class LogoutCell: UITableViewCell {

    lazy var logoutLabel: UILabel = {
        let label = UILabel()
        self.contentView.addSubview(label)
        label.text = "退出账号"
        label.textColor = UIColor(hexString: "#FC6666")
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        logoutLabel.snp.updateConstraints { (make) in
            make.center.equalTo(self.contentView.center)
        }
    }

}

//
//  paymentUtils.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import SwiftyJSON
class PaymentUtils {
    
    class func ailpaySuccessfully(with resultDic: [AnyHashable: Any]?) {
        if let result = resultDic {
//            let resultStatus = result.filter()
//            let resultStatus = result.filter({(key, value) -> Bool in
//                let string = key as! String
//                return string == "resultStatus"
//            })
//
//            let memoStr = result.filter({ (key, value) -> Bool in
//                let string = key as! String
//                return string == "memo"
//            })
//
//            let resultStr = result.filter({ (key, value) -> Bool in
//                let string = key as! String
//                return string == "result"
//            })
            let status = result["resultStatus"] as! String
            let memo: String = result["memo"] as! String
            let resultJson = result["result"] as! String
            let model = AlPayResult.init(fromJson: JSON(resultJson))
            let view = UIApplication.shared.keyWindow?.rootViewController?.view
            if status == "9000"  {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "playSuccessfully"), object: model)
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    view?.showAutoHUD("支付成功")
                })
            }else {
                DispatchQueue.main.asyncAfter(deadline: 0.5, execute: {
                    view?.showAutoHUD(memo)
                })
            }
        }
    }
}

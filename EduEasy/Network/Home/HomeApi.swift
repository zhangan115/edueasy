//
//  HomeApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let homeProvider = MoyaProvider<HomeApi>(manager: PGAlamofireManager.sharedManager)

enum HomeApi {
    case list(id: Int)
    case schoolList
    case apply(
        accountId: Int,
        realName: String,
        sex: String,
        schoolId: Int,
        specialtyCatalogId: Int,
        specialtyCatalogId2: Int,
        applyType: Int,
        phone: String,
        userName: String,
        idcard: String
    )
    case info(
        accountId: Int,
        realName: String,
        idcard: String,
        userName: String
    )
}

extension HomeApi: TargetType {
    var baseURL: URL {
        var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
        switch self {
        case .list:
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
        case .schoolList: break
        case .apply, .info:
            return Config.baseURL
        }
        return (urlComponents?.url)!
    }
    
    var urlPath: String {
        switch self {
        case .list:
            return "api/sysmsg/details"
        case .schoolList:
            return "api/school/list"
        default: break
        }
        return ""
    }
    
    var path: String {
        switch self {
        case .apply, .info:
            return "api/maccount/signup"
        default: break
        }
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .list, .schoolList, .apply, .info:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .list(let id):
            return ["id": id]
        case .schoolList:
            return nil
        case let .apply(accountId,
                    realName,
                    sex,
                    schoolId,
                    specialtyCatalogId,
                    specialtyCatalogId2,
                    applyType,
                    phone,
                    userName,
                    idcard):
            return ["accountId": accountId,
                    "realName": realName,
                    "sex": sex,
                    "schoolId": schoolId,
                    "specialtyCatalogId": specialtyCatalogId,
                    "specialtyCatalogId2": specialtyCatalogId2,
                    "applyType": applyType,
                    "phone": phone,
                    "userName": userName,
                    "idcard":idcard]
            
        case let .info(accountId, realName, idcard, userName):
            return ["accountId": accountId,
                    "realName": realName,
                    "idcard": idcard,
                    "userName": userName]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .list, .schoolList:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        case .apply, .info:
            var param = parameters
            param?["token"] = kToken
            return .requestParameters(parameters: param!, encoding: parameterEncoding)
        }
    }
}

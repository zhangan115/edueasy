//
//  ReviewApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/18.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let reviewProvider = MoyaProvider<ReviewApi>(manager: PGAlamofireManager.sharedManager)

enum ReviewApi {
    case review
}

extension ReviewApi: TargetType {
    var baseURL: URL {
        return Config.baseURL
    }
    
    var path: String {
        return "api/data/getData"
    }
    
    var method: Moya.Method {
        switch self {
        case .review:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        return nil
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .review:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        }
    }
}

//
//  StudyApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/6.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let studyProvider = MoyaProvider<StudyApi>(manager: PGAlamofireManager.sharedManager)

enum StudyApi {
    case list(range: Int,count:Int?)
    case getWebData(id:Int)
}

extension StudyApi: TargetType {
    var baseURL: URL {
        switch self {
        case .list,.getWebData:
            var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
            return (urlComponents?.url)!
        }
    }
    
    var urlPath: String {
        switch self {
        case .list:
            return "/api/msg/list"
        case .getWebData:
            return "/api/sysmsg/details"
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .list,.getWebData:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .list(let range,let count):
            var params = [String:Any]()
            params["range"] = range
            if let count = count {
                 params["count"] = count
            }
            return params
        case .getWebData(let id):
            return ["id":id]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .list,.getWebData:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        }
    }
}

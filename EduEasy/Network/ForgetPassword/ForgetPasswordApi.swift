//
//  ForgetPasswordApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/31.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let forgetProvider = MoyaProvider<ForgetPasswordApi>(manager: PGAlamofireManager.sharedManager)

enum ForgetPasswordApi {
    case forget(phone: String)
    case update(phone: String, password: String, verificationCode: String)
}

extension ForgetPasswordApi: TargetType {
    var baseURL: URL {
        var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
        switch self {
        case .forget, .update:
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
        }
        return (urlComponents?.url)!
    }
    
    var urlPath: String {
        switch self {
        case .forget:
            return "account/forgetpassword"
        case .update:
            return "account/updatepassword"
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .forget, .update:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .forget(let phone):
            return ["phone": phone]
        case let .update(phone, password, verificationCode):
            return ["phone": phone, "password": password, "verificationCode": verificationCode]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .forget, .update:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        }
    }
}

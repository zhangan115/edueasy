//
//  QuestionBankApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/27.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let questionBankProvider = MoyaProvider<QuestionBankApi>(manager: PGAlamofireManager.sharedManager)

enum QuestionBankApi {
    case paperList(type: ExamType, isYear: YearType, title: String?, pagerType: String?, pagerYear: String?, questionCatalogId: String?)
    case paperQuestion(id: Int)
    case favorite(paperQuestionId: Int, accountId: Int)
    case cancelFavorite(favoriteId: Int)
    case questionstype
    case upload(accountId: Int, data: String)
}

extension QuestionBankApi: TargetType {
    var baseURL: URL {
        switch self {
        case .paperList, .paperQuestion, .favorite, .questionstype, .cancelFavorite:
            var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
            return (urlComponents?.url)!
        case .upload:
            return Config.baseURL
        }
    }
    
    var urlPath: String {
        switch self {
        case .paperList:
            return "api/paper/objects"
        case .paperQuestion:
            return "api/paper/paperQuestion"
        case .favorite:
            return "api/usercollect/insert"
        case .cancelFavorite:
            return "api/usercollect/del"
        case .questionstype:
            return "api/questionType/list"
        case .upload:
            return "api/data/upload"
        }
    }
    
    var path: String {
        switch self {
        case .upload:
            return urlPath
        default: break
        }
        return ""
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .paperList(type, isYear, title, pagerType, pagerYear, questionCatalogId):
            var param: [String : Any] = ["type": type.rawValue, "isYear": isYear.rawValue]
            if let title = title {
                param["title"] = title
            }
            if let pagerType = pagerType {
                param["pagerType"] = pagerType
            }
            if let pagerYear = pagerYear {
                param["pagerYear"] = pagerYear
            }
            if let questionCatalogId = questionCatalogId {
                param["questionCatalogId"] = questionCatalogId
            }
            return param
        case .paperQuestion(let id):
            return ["id": id]
        case let .favorite(paperQuestionId, accountId):
            return ["paperQuestionId": paperQuestionId, "accountId": accountId]
        case .cancelFavorite(let favoriteId):
            return ["id": favoriteId, "token": kToken]
        case .questionstype:
            return nil
        case let .upload(accountId, data):
            return ["accountId": accountId, "data": data]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .paperList, .paperQuestion, .favorite, .questionstype, .cancelFavorite:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        case .upload:
            var params = parameters
            params!["token"] = kToken
            return .requestParameters(parameters: params!, encoding: parameterEncoding)
        }
    }
}


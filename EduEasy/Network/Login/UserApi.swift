//
//  PGUser.swift
//  Evpro
//
//  Created by gongyupeng on 2017/9/21.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import Moya

let userProvider = MoyaProvider<UserApi>(manager: PGAlamofireManager.sharedManager, plugins: [PGProvider.networkPlugin()])
let userProviderNoPlugin = MoyaProvider<UserApi>(manager: PGAlamofireManager.sharedManager)

enum UserApi {
    case login(username: String, password: String)
    case weiXinLogin(code:String)
}

extension UserApi: TargetType {
    var baseURL: URL {
        var urlComponents = URLComponents(string: Config.baseURL.absoluteString + Config.loginUrl)
        urlComponents?.queryItems = parameters?.toUrlQueryItems()
        return (urlComponents?.url)!
    }
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .weiXinLogin:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .login(username, password):
            return ["accountName": username, "pssword": password]
         case let .weiXinLogin(code):
            return ["code":code]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .login:
            return .requestPlain
        case .weiXinLogin:
            return .requestParameters(parameters: parameters!, encoding: URLEncoding.default)
        }
    }
}

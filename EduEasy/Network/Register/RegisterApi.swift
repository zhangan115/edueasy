//
//  RegisterApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/25.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let registerProvider = MoyaProvider<RegisterApi>(manager: PGAlamofireManager.sharedManager)

enum RegisterApi {
    case verifyPhoneCode(phone: String)
    case register(pssword: String, phone: String, verificationCode: String, inviteCode: String?
        ,realName:String,userCardId:String,suoxuezhuanye:String,type:String,openId: String?)
}

extension RegisterApi: TargetType {
    var baseURL: URL { return Config.baseURL }
    var path: String {
        switch self {
        case .verifyPhoneCode:
            return "account/sendCode"
        case .register:
            return "account/reg"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .verifyPhoneCode:
            return .get
        case .register:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .verifyPhoneCode(let phone):
            return ["phone": phone]
        case let .register(pssword, phone, verificationCode, inviteCode,realName,userCardId,suoxuezhuanye,type,openId):
            var parames = ["password": pssword,
                           "phone": phone,
                           "verificationCode": verificationCode,
                           "realName": realName,
                           "idcard": userCardId,
                           "suoxuezhuanye": suoxuezhuanye,
                           "type": type,
            ]
            if let openId = openId {
                parames["openId"] = openId
            }
            if let inviteCode = inviteCode {
                if inviteCode.count != 0 {
                    parames["inviteCode"] = inviteCode
                }
            }
            return parames
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .verifyPhoneCode:
            return .requestParameters(parameters: parameters!, encoding: URLEncoding.default)
        case .register :
            return .requestParameters(parameters: parameters!, encoding: parameterEncoding)
        }
    }
}

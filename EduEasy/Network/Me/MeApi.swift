//
//  MeApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/4/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let meProvider = MoyaProvider<MeApi>(manager: PGAlamofireManager.sharedManager)

enum MeApi {
    case favorite(accountId: Int, lastId: Int?)
    case errorTitleList(accountId: Int)
    case errorTitleDetail(examedPaperId: Int)
    case getRecordList(accountId: Int,bResult:String,lastId:Int?)
}

extension MeApi: TargetType {
    var baseURL: URL {
        switch self {
        case .favorite, .errorTitleList, .errorTitleDetail,.getRecordList:
            var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
            return (urlComponents?.url)!
        }
    }
    
    var urlPath: String {
        switch self {
        case .favorite:
            return "api/usercollect/list"
        case .errorTitleList:
            return "api/examed/examedQuestion"
        case .errorTitleDetail:
            return "api/examed/list"
        case .getRecordList:
            return "api/userExamedQuestion/list"
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .favorite, .errorTitleList, .errorTitleDetail,.getRecordList:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .favorite(accountId, lastId):
            var params: [String: Any] = ["accountId": accountId]
            if let lastId = lastId {
                params["lastId"] = lastId
            }
            return params
        case .errorTitleList(let accountId):
            return ["accountId": accountId]
        case .errorTitleDetail(let examedPaperId):
            return ["examedPaperId": examedPaperId]
        case let .getRecordList(accountId,bResult,lastId):
             var params: [String: Any] = ["accountId": accountId,"bResult": bResult]
             if let lastId = lastId {
                params["lastsId"] = lastId
             }
            return params
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .favorite, .errorTitleList, .errorTitleDetail,.getRecordList:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        }
    }
}

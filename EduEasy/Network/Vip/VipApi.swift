//
//  VipApi.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
import Moya

let vipProvider = MoyaProvider<VipApi>(manager: PGAlamofireManager.sharedManager)

enum VipApi {
    case orderString(carId: Int,payType:String)
    case cardList
    case paySuccessful(maccountId: Int, carId: Int)
    case payCallBack(outTradeNo:String!,trade_no:String?)
    
    case getSchoolList
    case paySchooMoney(schoolId:String!,majorTypeId:String!,tuitionId:String!,classGrade:String!,payType:String!,isnot:String!)
}

extension VipApi: TargetType {
    var baseURL: URL {
        var urlComponents = URLComponents(string: Config.baseURL.absoluteString + urlPath)
        switch self {
        case .orderString, .paySuccessful,.payCallBack,.paySchooMoney:
            urlComponents?.queryItems = parameters?.toUrlQueryItems()
        case .cardList,.getSchoolList: break
        }
        return (urlComponents?.url)!
    }
    
    var urlPath: String {
        switch self {
        case .orderString:
            return "/api/pay/cardOrTuition"
        case .cardList:
            return "/api/membercard/list"
        case .paySuccessful:
            return "/api/membercard/paySuccessful"
        case .payCallBack:
            return "/api/app/pay/back"
        case .getSchoolList:
            return "/api/tuition/list"
        case .paySchooMoney:
            return "/api/pay/cardOrTuition"
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        switch self {
        case .orderString, .cardList, .paySuccessful,.payCallBack,.getSchoolList,.paySchooMoney:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .orderString(let carId,let payType):
            return ["cardId": String(carId),"payType":payType,"payObjective":"0","appType":"ios"]
        case .cardList:
            return nil
        case let .paySuccessful(maccountId, carId):
            return ["maccountId": maccountId, "carId": carId]
        case let .payCallBack(outTradeNo,trade_no):
            var params = [String:Any]()
            params["outTradeNo"] =  outTradeNo
            if let trade_no = trade_no {
                params["trade_no"] =  trade_no
            }
            return params
        case .getSchoolList:
            return nil
        case .paySchooMoney(let schoolId,let majorTypeId,let tuitionId
            ,let classGrade,let payType,let isnot):
             var params = [String:Any]()
             params["schoolId"] = schoolId
             params["majorTypeId"] = majorTypeId
             params["tuitionId"] = tuitionId
             params["classGrade"] = classGrade
             params["payType"] = payType
             params["isnot"] = isnot
             params["appType"] = "ios"
             params["payObjective"] = "1"
            return params
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return "sampleData".data(using: .utf8)!
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .orderString, .cardList, .paySuccessful,.payCallBack,.getSchoolList,.paySchooMoney:
            return .requestParameters(parameters: ["token": kToken], encoding: parameterEncoding)
        }
    }
}

//
//  PGTabBarController.swift
//  Evpro
//
//  Created by piggybear on 2017/8/17.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

class PGTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = UIColor.white
        tabBar.isTranslucent = false
        self.delegate = self
        setup()
        
        let image = UIColor(hexString: "#CCCCCC")?.toImage()
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.shadowImage = image
    }
    
    func setup() {
        let homeController = HomeController()
        setupChildViewController(homeController, title: "首页", imageName: "icon_toolbar_home2", selectedImageName: "icon_toolbar_home")
        
        let questionBank = QuestionBankController()
        setupChildViewController(questionBank, title: "课程", imageName: "icon_toolbar_learn2", selectedImageName: "icon_toolbar_learn")
        
        let study = StudyPageController()
        setupChildViewController(study, title: "学习", imageName: "icon_toolbar_Item_Bank2", selectedImageName: "icon_toolbar_Item_Bank-")
        
        let me = MeController()
        setupChildViewController(me, title: "我的", imageName: "icon_toolbar_mine2", selectedImageName: "icon_toolbar_mine")
    }
    
    func setupChildViewController(_ viewController: UIViewController, title: String, imageName: String, selectedImageName: String) {
        let tabBarItem: UITabBarItem! = viewController.tabBarItem
        
        tabBarItem.title = title
        
        tabBarItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(hexString: "#262626")!], for: .normal)
        tabBarItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(hexString: "#2772FE")!], for: .selected)
        
        var image = UIImage(named: imageName)
        image = image?.withRenderingMode(.alwaysOriginal)
        tabBarItem.image = image
        
        var selectedImage = UIImage(named: selectedImageName)
        selectedImage = selectedImage?.withRenderingMode(.alwaysOriginal)
        tabBarItem.selectedImage = selectedImage
        
        let navigation = BaseNavigationController(rootViewController: viewController)
        self.addChildViewController(navigation)
    }
}

extension PGTabBarController :UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if isDidLogin {
            return true
        }
        let index = tabBarController.viewControllers?.index(of: viewController)
        if index == 1 || index == 3 {
            alertController()
            return false
        }
        return true
    }
}

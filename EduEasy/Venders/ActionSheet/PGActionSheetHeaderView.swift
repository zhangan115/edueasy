//
//  PGActionSheetHeaderView.swift
//  Evpro
//
//  Created by piggybear on 2017/11/15.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

typealias PGActionSheetButtonHandler = ()->()

class PGActionSheetHeaderView: UIView {

    var cancelButton: UIButton!
    var sureButton: UIButton!
    var cancelButtonHandler: PGActionSheetButtonHandler?
    var sureButtonHandler: PGActionSheetButtonHandler?

    override init(frame: CGRect) {
        super.init(frame: frame)
     
        setupSureButton()
        setupCancelButton()
        self.backgroundColor = UIColor(hexString: "#E7E0EF")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCancelButton() {
        cancelButton = UIButton()
        cancelButton.setTitle("取消", for: .normal)
        cancelButton.setTitleColor(UIColor.gray, for: .normal)
        cancelButton.setTitleColor(UIColor.darkGray, for: .highlighted)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(cancelButton)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint = NSLayoutConstraint(item: cancelButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 5)
        let centerY = NSLayoutConstraint(item: cancelButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0)
        self.addConstraints([leftConstraint, centerY])
        
        let widthConstraint = NSLayoutConstraint(item: cancelButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
        let heightConstraint = NSLayoutConstraint(item: cancelButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        cancelButton.addConstraints([widthConstraint, heightConstraint])
        cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
    }
    
    private func setupSureButton() {
        sureButton = UIButton()
        sureButton.setTitle("确定", for: .normal)
        sureButton.setTitleColor(UIColor.blue.withAlphaComponent(0.6), for: .normal)
        sureButton.setTitleColor(UIColor.blue.withAlphaComponent(0.8), for: .highlighted)
        sureButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(sureButton)
        sureButton.translatesAutoresizingMaskIntoConstraints = false
        let sureButtonRightConstraint = NSLayoutConstraint(item: sureButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: -5)
        let sureButtonCenterY = NSLayoutConstraint(item: sureButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0)
        self.addConstraints([sureButtonRightConstraint, sureButtonCenterY])
        let sureButtonWidthConstraint = NSLayoutConstraint(item: sureButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
        let sureButtonHeightConstraint = NSLayoutConstraint(item: sureButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        sureButton.addConstraints([sureButtonWidthConstraint, sureButtonHeightConstraint])
        sureButton.addTarget(self, action: #selector(sureButtonAction), for: .touchUpInside)
    }
    
    @objc private func sureButtonAction() {
        if sureButtonHandler != nil {
            self.sureButtonHandler!()
        }
    }
    
    @objc private func cancelButtonAction() {
        if cancelButtonHandler != nil {
            self.cancelButtonHandler!()
        }
    }
    
}

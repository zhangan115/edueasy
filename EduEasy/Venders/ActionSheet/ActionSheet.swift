//
//  ActionSheet.swift
//  ActionSheet
//
//  Created by piggybear on 2017/10/2.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import UIKit

public typealias handlerAction = (Int)->()
public class ActionSheet: UIViewController {
    public var handler: handlerAction?
    public weak var delegate: ActionSheetDelegate?
    
    public var textColor: UIColor?
    public var textFont: UIFont?
    
    public var cancelTextColor: UIColor?
    public var cancelTextFont: UIFont?
    
    public var actionSheetTitle: String?
    public var actionSheetTitleFont: UIFont?
    public var actionSheetTitleColor: UIColor?
    
    var selectedRow: Int = -1
    var selectedTime: Bool = false
    
    var otherStartTime: String!
    var otherEndTime: String!
    var headerView: PGActionSheetHeaderView!
    
    //MARK: - private property
    fileprivate let hasCancelButton: Bool
    fileprivate let screenWidth = UIScreen.main.bounds.size.width
    fileprivate let screenHeight = UIScreen.main.bounds.size.height
    fileprivate let buttonList: [String]!
    fileprivate var overlayView: UIView!
    fileprivate var tableView: UITableView!
    fileprivate let overlayBackgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.5)
    fileprivate let reuseIdentifier = "PGTableViewCell"
    
    //MARK: - system cycle
    required public init(cancelButton: Bool = false, buttonList:[String]!){
        self.buttonList = buttonList
        self.hasCancelButton = cancelButton
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = UIColor.clear
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalPresentationStyle = .custom
        setup()
        setHeaderView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        var bottom: CGFloat = 0
        if #available(iOS 11.0, *) {
            bottom = self.view.safeAreaInsets.bottom
        }
        
        var height: CGFloat = 0.0
        if self.buttonList.count > 5 {
            height = 5 * 44 - 20 - bottom
            tableView.showsVerticalScrollIndicator = true
            tableView.isScrollEnabled = true
        }else {
            height = CGFloat(buttonList.count) * 44
        }
        let frame = CGRect(x: 0, y: screenHeight - height - bottom, width: screenWidth, height: height + bottom)
        UIView.animate(withDuration: 0.2) {
            self.tableView.frame = frame
            self.overlayView.alpha = 1.0
        }
    }
    
    //MARK: - private method
    fileprivate func setup() {
        overlayView = UIView(frame: self.view.bounds)
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(overlayView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(overlayViewTapHandler))
        overlayView.addGestureRecognizer(tap)
        
        var frame = self.view.frame
        frame.origin.y = self.screenHeight
        tableView = UITableView(frame: frame, style: .plain)
        tableView.showsVerticalScrollIndicator = false
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PGTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView.tableFooterView = UIView()
        self.view.addSubview(tableView)
    }
    
    func setHeaderView() {
        let frame = CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: 44)
        headerView = PGActionSheetHeaderView(frame: frame)
        self.view.addSubview(headerView)
    }
    
    @objc fileprivate func overlayViewTapHandler() {
        var frame = self.tableView.frame
        frame.origin.y = self.view.bounds.size.height
        
        var headerViewFrame = self.headerView.frame
        headerViewFrame.origin.y = self.view.bounds.size.height
        UIView.animate(withDuration: 0.2, animations: {
            self.headerView.frame = headerViewFrame
            self.tableView.frame = frame
            self.overlayView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0)
        }) { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc fileprivate func deviceOrientationDidChange() {
        let height = self.tableView.bounds.size.height
        let width = self.view.bounds.size.width
        let y = self.view.bounds.size.height - height
        let frame = CGRect(x: 0, y: y, width: width, height: height)
        UIView.animate(withDuration: 0.2) {
            self.overlayView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2));
            self.tableView.frame = frame
            self.overlayView.frame = self.view.bounds
        }
    }
}

extension ActionSheet: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buttonList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PGTableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! PGTableViewCell
        cell.signlabel.text = buttonList[indexPath.row]
        return cell
    }
}

extension ActionSheet: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row >= buttonList.count {
            return
        }
        self.handler?(indexPath.row)
        self.delegate?.actionSheet!(self, clickedButtonAt: indexPath.row)
        overlayViewTapHandler()
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

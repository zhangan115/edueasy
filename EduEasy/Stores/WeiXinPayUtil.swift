//
//  WeiXinPayUtil.swift
//  WeiXinPayDemo
//
//  Created by sitech on 2018/12/21.
//  Copyright © 2018 zhangan. All rights reserved.
//

import Foundation
import SwiftyJSON
class WeiXinPayUtil {
    
    var weiXinApi:WXApi!
    
    static func toPay(model:WeiXinPayModel!) ->  PayReq {
        WXApi.registerApp(model.appid)
        let req = PayReq()
        req.partnerId = model.partnerid
        req.prepayId = model.prepayid
        req.package = model.package
        req.nonceStr = model.noncestr
        req.timeStamp = UInt32(model.timestamp)!
        req.sign = model.sign
        return req
    }
    
    class WeiXinBuild {
        
        private  var appId :String!
        private  var partnerId:String!
        private  var prepayId:String!
        private  var packageValue:String!
        private  var nonceStr:String!
        private  var timeStamp:String!
        private  var sign:String!
        
        func setAppId(_ appId:String) -> WeiXinBuild {
            self.appId = appId
            return self
        }
        
        func setPartnerId(_ partnerId :String) -> WeiXinBuild {
            self.partnerId = partnerId
            return self
        }
        
        func setPrepayId(_ prepayId :String) -> WeiXinBuild {
            self.prepayId = prepayId
            return self
        }
        
        func setPackageValue(_ packageValue :String) -> WeiXinBuild {
            self.packageValue = packageValue
            return self
        }
        
        func setNonceStr(_ nonceStr :String) -> WeiXinBuild {
            self.nonceStr = nonceStr
            return self
        }
        
        func setTimeStamp(_ timeStamp :String) -> WeiXinBuild {
            self.timeStamp = timeStamp
            return self
        }
        
        func setSign(_ sign :String) -> WeiXinBuild {
            self.sign = sign
            return self
        }
        
    }
}

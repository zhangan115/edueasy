//
//  PGMacros.swift
//  edetection
//
//  Created by piggybear on 30/04/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import Foundation
import UIKit

func PGColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
    return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
}

func Log<T>(_ message: T){
    #if DEBUG
        print(message)
    #endif
}

let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height

let kBackgroundColor: UIColor = UIColor(hexString: "#E7E7E7")!

var kToken: String {
     return UserModel.unarchiver()?.token ?? ""
}

var isDidLogin: Bool {
    return UserDefaults.standard.bool(forKey: kLogin)
}

func setIsLogin(_ isLogin: Bool) {
    UserDefaults.standard.set(isLogin, forKey: kLogin)
}

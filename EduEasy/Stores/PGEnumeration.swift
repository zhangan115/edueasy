//
//  PGEnumeration.swift
//  Evpro
//
//  Created by piggybear on 2017/11/2.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import UIKit

enum ExamType: Int {
    case collegeExam = 1    //成人高考
    case degreeEnglish      //学位英语
    case computerTraining   //计算机培训
}

enum YearType: Int {
    case simulate   //模拟考试
    case oldYear    //历年真题
}

enum PaperType: Int {
    case highUndergraduate = 1      //高中起点本科
    case highSpecialist             //高中起点专科
    case specialistUndergraduate    //专科起点本科
}

enum Result<T: Mappable> {
    case success(model: T)
    case failed(message: String)
    case ok
    case empty
    case error
}

enum TitleType: Int {
    case blank = 1          //填空题
    case singleChoice       //单项选择题
    case multipleChoice     //多项选择题
    case judgment           //判断题
    case simple             //简答题
    
    func toText() -> String {
        let list = ["填空题", "单选题", "多选题", "判断题", "简答题"]
        let index = rawValue - 1
        if index >= 0 && index < list.count {
            return list[index]
        }
        return ""
    }
}

enum AnswerType {
    case right
    case error //如果填空题有三个空，回答了两个，有一个没有回答也算错
    case empty  //未作答
}




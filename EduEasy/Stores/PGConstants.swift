//
//  PGConstants.swift
//  edetection
//
//  Created by piggybear on 02/05/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import Foundation
import UIKit

struct Config {
    static let baseURL: URL = URL(string: "http://39.106.210.43/FxExam/")!
    static let loginUrl: String = "account/login"
}

struct BundleConfig {
    static var appDisplayName: String {
        let infoDictionary = Bundle.main.infoDictionary
        let displayName: String = infoDictionary!["CFBundleDisplayName"] as! String
        return displayName
    }
    
    static var appVersion: String {
        let infoDictionary = Bundle.main.infoDictionary
        let version: String = infoDictionary!["CFBundleShortVersionString"] as! String
        return version
    }
}

struct WeiXin {
    static let appId : String = "wx1c0c07722cf3fe96"
}





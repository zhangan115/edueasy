//
//  BaseViewController.swift
//  edetection
//
//  Created by piggybear on 30/04/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class BaseViewController: UIViewController,UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = kBackgroundColor
        hiddenNaviBarLine()
    }
    
    func hiddenNaviBarLine() {
        if self.navBar?.subviews != nil {
            for item in (self.navBar?.subviews)! {
                if item.className == "_UIBarBackground" {
                    item.subviews.first?.isHidden = true
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if self == self.navigationController?.viewControllers.first {
            return false
        }
        return true
    }
}

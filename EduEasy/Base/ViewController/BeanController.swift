//
//  BeanController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class BeanController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        backButtonLogic()
    }

    func backButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(pop))
        button.setImage(UIImage(named: "icon_btn_arrow_l_r"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }
}

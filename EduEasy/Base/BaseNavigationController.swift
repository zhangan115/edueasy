//
//  BaseNavigationController.swift
//  edetection
//
//  Created by piggybear on 02/05/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import UIKit
import Foundation

class BaseNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置右划手势的代理为自己
//        self.interactivePopGestureRecognizer?.delegate = self
        
        let navi = UINavigationBar.appearance()
        navi.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navi.isTranslucent = false
        navi.barTintColor = UIColor(hexString: "#308AFF")
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
//    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
        
        //设置右划返回为true
//        self.interactivePopGestureRecognizer?.isEnabled = true
        
        //iOS 11中防止在push时tabBar上移
        if #available(iOS 11.0, *), self.tabBarController != nil {
            var frame: CGRect = (self.tabBarController?.tabBar.frame)!
            frame.origin.y = UIScreen.main.bounds.size.height - frame.size.height
            self.tabBarController?.tabBar.frame = frame
        }
    }
    
}

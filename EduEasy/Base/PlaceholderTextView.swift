//
//  PlaceholderTextView.swift
//  Evpro
//
//  Created by gongyupeng on 2017/9/19.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

struct Placeholder {
    var text: String = ""
}

@IBDesignable class PlaceholderTextView: UITextView {
    
    private lazy var placeholderLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(hexString: "#959595")
        self.addSubview(label)
        return label
    }()
    
    @IBInspectable public var placeholder: String? {
        set(value) {
            placeholderLabel.text = value
        }
        
        get {
            return placeholderLabel.text
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.frame = CGRect(x: 10, y: 0, w: self.frame.size.width - 20, h: 30)
    }
}


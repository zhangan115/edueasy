//
//  PGBaseTableView.swift
//  Evpro
//
//  Created by piggybear on 2017/11/17.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

class BaseTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        logic()
    }
    
    required init?(coder aDecoder: NSCoder) {
     super.init(coder: aDecoder)
        
        logic()
    }
    
    func logic() {
        tableFooterView = UIView()
        self.separatorInset = UIEdgeInsets.zero
    }
}


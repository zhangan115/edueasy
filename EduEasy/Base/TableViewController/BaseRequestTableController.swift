//
//  BaseRequestTableController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/4.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import MJRefresh

class BaseRequestTableController: BeanTableController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableViewHeader()
        setTableViewFooter()
    }
    
    func setTableViewHeader() {
        let refreshHeader = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_header = refreshHeader
        refreshHeader?.beginRefreshing()
    }
    
    func setTableViewFooter() {
        let footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(request))
        tableView.mj_footer = footer
        tableView.mj_footer.isHidden = true
    }
    
    @objc func request() {
        
    }

}

//
//  BaseTableController.swift
//  edetection
//
//  Created by piggybear on 16/05/2017.
//  Copyright © 2017 piggybear. All rights reserved.
//

import UIKit
import MJRefresh
import UIKit

class BaseTableController: UITableViewController,UIGestureRecognizerDelegate  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenNaviBarLine()
        
        self.view.backgroundColor = kBackgroundColor
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets.zero
        
        if #available(iOS 11.0, *) {
            UIScrollView.appearance().contentInsetAdjustmentBehavior = .never
        }else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }

    func hiddenNaviBarLine() {
        for item in (self.navBar?.subviews)! {
            if item.className == "_UIBarBackground" {
                item.subviews.first?.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if self == self.navigationController?.viewControllers.first {
            return false
        }
        return true
    }
    
    
}


//
//  NavigationBaseController.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit

class NavigationBaseTableController: BaseTableController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = UIColor.black
        self.navigationItem.titleView = label
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButtonLogic()
    }
    
    func setCustomTitle(_ title: String) {
        titleLabel.text = title
        let width = 15 * title.count
        titleLabel.frame = CGRect(x: 0, y: 0, w: CGFloat(width), h: 20)
    }
    
    func backButtonLogic() {
        let button = UIButton(x: 0, y: 0, w: 40, h: 40, target: self, action: #selector(pop))
        button.setImage(UIImage(named: "icon_btn_arrow_l_b"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func pop() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NavigationBaseTableController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBar?.barTintColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navBar?.barTintColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navBar?.barTintColor = UIColor(hexString: "#2772FE")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navBar?.barTintColor = UIColor(hexString: "#2772FE")
    }
}

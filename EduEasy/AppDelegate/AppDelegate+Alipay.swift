//
//  AppDelegate+Alipay.swift
//  EduEasy
//
//  Created by piggybear on 2018/5/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation
extension AppDelegate :WXApiDelegate{
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.host == "safepay" {
            AlipaySDK.defaultService().processOrder(withPaymentResult: url) { resultDic in
                PaymentUtils.ailpaySuccessfully(with: resultDic)
            }
            
            AlipaySDK.defaultService().processAuth_V2Result(url) { (resultDic) in
                print("1-resultDic = ", resultDic ?? [:])
                var authCode: String!
                let result: String = resultDic!["result"] as! String
                if result.count > 0 {
                    let resultArr = result.components(separatedBy: "&")
                    for subResult in resultArr {
                        if (subResult.length > 10 && subResult.hasPrefix("auth_code=")) {
                            let index = subResult.index(subResult.startIndex, offsetBy: 10)
                            authCode = subResult.substring(from: index)
                            break
                        }
                    }
                }
                print("授权结果 authCode = ", authCode)
            }
        } else {
            WXApi.handleOpen(url, delegate: self)
        }
        return true
    }
    
    func onResp(_ resp: BaseResp) {
        if let payResp = resp as? PayResp {
            if payResp.errCode == WXSuccess.rawValue {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "playWeiXinSuccessfully"), object: nil)
            }else{
                 NotificationCenter.default.post(name: Notification.Name(rawValue: "playWeiXinFail"), object: nil)
            }
        }else if let sendAuthResp = resp as? SendAuthResp {
            if sendAuthResp.errCode == WXSuccess.rawValue {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "WeiXinLogin"), object: sendAuthResp.code)
            }
        }
    }
    
    func onReq(_ req: BaseReq) {
        
    }
    

}

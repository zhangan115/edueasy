//
//  AppDelegate.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/3.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        UserDefaults.standard.set(false, forKey: "review")
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        print("documentPath = ", documentPath!)
        //iOS 11中防止在pop时UIScrollView偏移
        if #available(iOS 11.0, *) {
            UIScrollView.appearance().contentInsetAdjustmentBehavior = .never
        }
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        if isDidLogin {
            let controller = PGTabBarController()
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }else {
            let navi = BaseNavigationController(rootViewController: LoginController())
            self.window?.rootViewController = navi
            self.window?.makeKeyAndVisible()
//            loginRequest()
        }
        //微信支付
        WXApi.registerApp(WeiXin.appId)
        return true
    }

    func loginRequest() {
        userProviderNoPlugin
            .rxRequest(.login(username: "18700850000", password: "a1234567"))
            .toModel(type: UserModel.self)
            .subscribe(onSuccess: { (model) in
                UserModel.archiverUser(model)
                self.setRootViewController()
            }).disposed(by: disposeBag)
    }
    
    func setRootViewController() {
        let controller = PGTabBarController()
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }

}


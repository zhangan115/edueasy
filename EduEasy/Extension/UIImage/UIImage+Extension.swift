//
//  UIImage+Extension.swift
//  Evpro
//
//  Created by piggybear on 2017/12/15.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func compressTo(_ expectedSizeInKB: Int) -> Data? {
        let sizeInBytes = expectedSizeInKB * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = UIImageJPEGRepresentation(self, compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        return imgData
    }
}

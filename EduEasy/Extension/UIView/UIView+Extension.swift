//
//  UIView+Extension.swift
//  Evpro
//
//  Created by piggybear on 2017/11/2.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import UIKit

enum UIViewBorderType {
    case all //四边都有边界线
    case top //只有上面有
    case bottom
    case left
    case right
}

extension UIView {    
    /// 设置边缘线
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - height: 高度
    ///   - color: 线条颜色
    func setBorders(_ types: [UIViewBorderType], height: CGFloat, color: UIColor) {
        for item in types {
            switch item {
            case .all:
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = height
            case .top:
                let frame = CGRect(x: 0, y: 0, w: self.bounds.size.width, h: height)
                let layer = CALayer()
                layer.frame = frame
                layer.backgroundColor = color.cgColor
                self.layer.addSublayer(layer)
            case .bottom:
                let frame = CGRect(x: 0, y: self.bounds.size.height - height, w: self.bounds.size.width, h: height)
                let layer = CALayer()
                layer.frame = frame
                layer.backgroundColor = color.cgColor
                self.layer.addSublayer(layer)
            case .left:
                let frame = CGRect(x: 0, y: 0, w: height, h: self.bounds.size.height)
                let layer = CALayer()
                layer.frame = frame
                layer.backgroundColor = color.cgColor
                self.layer.addSublayer(layer)
            case .right:
                let frame = CGRect(x: self.bounds.size.width-height, y: 0, w: height, h: self.bounds.size.height)
                let layer = CALayer()
                layer.frame = frame
                layer.backgroundColor = color.cgColor
                self.layer.addSublayer(layer)
            }
        }
    }
    
    
    /// 设置边缘线
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - width: 宽度
    ///   - color: 线条颜色
    func setBorder(_ type: UIViewBorderType, frame: CGRect? = nil, height: CGFloat, color: UIColor) {
        var newFrame = self.frame
        switch type {
        case .all:
            self.layer.borderColor = color.cgColor
            self.layer.borderWidth = height
            return
        case .top:
            newFrame = CGRect(x: 0, y: 0, w: self.bounds.size.width, h: height)
        case .bottom:
            newFrame = CGRect(x: 0, y: self.bounds.size.height - height, w: self.bounds.size.width, h: height)
        case .left:
            newFrame = CGRect(x: 0, y: 0, w: height, h: self.bounds.size.height)
        case .right:
            newFrame = CGRect(x: self.bounds.size.width-height, y: 0, w: height, h: self.bounds.size.height)
        }
        if frame != nil {
            newFrame = frame!
        }
        let layer = CALayer()
        layer.frame = newFrame
        layer.backgroundColor = color.cgColor
        self.layer.addSublayer(layer)
    }
    
    /// 设置圆角
    ///
    /// - Parameters:
    ///   - rectCorner: 圆角类型
    ///   - radii: 圆角半径
    func setRoundingCorner(_ rectCorner: UIRectCorner, radii: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: radii, height: radii))
        var maskLayer: CAShapeLayer!
        if self.layer.mask != nil {
            maskLayer = self.layer.mask as! CAShapeLayer
        }else {
            maskLayer = CAShapeLayer()
            self.layer.mask = maskLayer
        }
        maskLayer.path = maskPath.cgPath
        maskLayer.frame = self.bounds
    }
}

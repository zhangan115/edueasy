//
//  UILabel+Extension.swift
//  EduEasy
//
//  Created by piggybear on 2018/3/19.
//  Copyright © 2018年 piggybear. All rights reserved.
//

import Foundation

extension UILabel {
    func set(text: String, lineSpace: CGFloat) {
        let mutableAttributedString = NSMutableAttributedString(string: text)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        mutableAttributedString.addAttributes([NSAttributedStringKey.paragraphStyle: style], range: NSRange(location: 0, length: text.length))
        self.attributedText = mutableAttributedString
    }
    
    var lineNumbers: CGFloat {
        let string: NSString = (self.text ?? "") as NSString
        // 获取单行时候的内容的size
        let singleSize = string.size(withAttributes: [NSAttributedStringKey.font: self.font])
        // 获取多行时候,文字的size
        let textSize = string.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat(MAXFLOAT)),
                            options: .usesLineFragmentOrigin,
                            attributes: [NSAttributedStringKey.font: self.font],
                            context: nil)
        // 返回计算的行数
        return ceil( textSize.height / singleSize.height)
    }
}

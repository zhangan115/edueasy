//
//  UIImageView+Extension.swift
//  Evpro
//
//  Created by piggybear on 2017/12/4.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    func loadNetworkImage(_ urlString: String, placeholder: String) {
        var newUrlString: String!
        if urlString.hasPrefix("http") {
            newUrlString = urlString
        }else {
            newUrlString = Config.baseURL.absoluteString + urlString
        }
        let image = UIImage(named: placeholder)
        let url = URL(string: newUrlString)
        if url == nil {
            return
        }
        self.sd_setImage(with: url!, placeholderImage: image)
    }
}

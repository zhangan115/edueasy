//
//  UIButton+Extension.swift
//  Evpro
//
//  Created by piggybear on 2017/10/24.
//  Copyright © 2017年 piggybear. All rights reserved.
//

import Foundation
import UIKit

enum ButtonImageEdgeInsetsStyle {
    case top        //图片在上
    case bottom     //图片在下
    case left       //图片在左
    case right      //图片在右
}

extension UIButton {
    
    /// 设置图片跟字的显示样式
    ///
    /// - Parameters:
    ///   - imagePosition: 样式类型
    ///   - titleSpace: 字与图片的间距
    func layout(imagePosition style: ButtonImageEdgeInsetsStyle, titleSpace space: CGFloat) {
        let imageWith: CGFloat! = self.imageView?.frame.size.width
        let imageHeight: CGFloat! = self.imageView?.frame.size.height
        var labelWidth: CGFloat = 0, labelHeight: CGFloat = 0
        labelWidth = (self.titleLabel?.intrinsicContentSize.width)!
        labelHeight = (self.titleLabel?.intrinsicContentSize.height)!
        
        var imageEdgeInsets: UIEdgeInsets = .zero
        var labelEdgeInsets: UIEdgeInsets = .zero
        switch style {
        case .top:
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight - space/2.0, 0, 0, -labelWidth)
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight - space/2.0, 0)
        case .left:
            imageEdgeInsets = UIEdgeInsetsMake(0, -space/2.0, 0, space/2.0)
            labelEdgeInsets = UIEdgeInsetsMake(0, space/2.0, 0, -space/2.0)
        case .bottom:
            imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight-space/2.0, -labelWidth)
            labelEdgeInsets = UIEdgeInsetsMake(-imageHeight-space/2.0, -imageWith, 0, 0)
        case .right:
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+space/2.0, 0, -labelWidth-space/2.0)
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith-space/2.0, 0, imageWith+space/2.0)
        }
        self.titleEdgeInsets = labelEdgeInsets
        self.imageEdgeInsets = imageEdgeInsets
    }
}


